<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    /*'facebook' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => 'http://localhost/tutorial-laravel/public/social/callback/facebook',
    ],*/

    'google' => [
        'client_id' => '89886253049-qi7vilcausvbnn5ntbdmth7kkuch5tj7.apps.googleusercontent.com',
        'client_secret' => 'AIc3HOTUUrpyXHgW5CxKo1N1',
        'redirect' => 'https://sunapiperu.com/oauth2/google',
    ],

    'github' => [
        'client_id' => 'db4f78cbc316f9bf0462',
        'client_secret' => '61d6fd09e0e4496a5c2f469384484fda6936505f',
        'redirect' => 'https://sunapiperu.com/oauth2/github',
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];
