<!DOCTYPE html>
<html class="no-js" lang="">
<head>

    <!-- Site Title -->
    <title>SunApi Per&uacute; :: Documentaci&oacute;n</title>

    <!-- Site Meta Info -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="SunApi Per&uacute; es la API de integraci&oacute;n que permitir&aacute; a sus aplicaciones realizar consultas de contribuyentes e informaci&oacute;n financiera del Per&uacute;.">
    <meta name="keywords"
          content="peru, per&uacute;, api, rest, cloud, sunat, sunapi, contribuyente, ruc, soles, monedas, cambio moneda, facturacion electronica">
    <meta name="author" content="sunapiperu.com">


    <!-- Essential CSS Files -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/simplelightbox.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- Color Styles | To Chage the color simply remove all the stylesheet form bellow without the color you want to keep -->
    <link rel="stylesheet" href="css/default-color.css">

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">

</head>
<body>
<section id="inicio"></section>
<?php /*<a class="btn btn-link" href="/#service" role="button"><span class="fa fa-arrow-circle-left"></span> Regresar</a>*/ ?>

<div class="container">

    <div class="row">
        <div class="col-md-8">
            <div class="page-header">
                <h1>SunApi Per&uacute;
                    <small>documentaci&oacute;n</small>
                </h1>
            </div>

            <p class="bg-primary"><i class="fa fa-warning"></i>
                Para ver ejemplos de consumo en varios lenguajes de programaci&oacute;n puede
                visitar nuestro repositorio en <a class="blank-link" TARGET="_blank" href="https://github.com/SunApiPeru/ConsumoAPI">GitHub</a></p>

            <section id="introduccion">
                <div class="page-header">
                    <h2>Introducci&oacute;n</h2>
                </div>

                <p><strong>SunApi Per&uacute;</strong> es una API de integraci&oacute;n que le permitir&aacute; consultar informaci&oacute;n de contribuyentes,
                    datos de pol&iacute;ticas monetarias y de tasas de cambio de Per&uacute;.
                    Las funcionalidades de esta API podr&aacute;n incorporarse a sus sistemas inform&aacute;ticos
                    como esencia del mismo o como un valor agregado, que le permitir&aacute; acceder en todo momento a datos relevantes
                    y actualizados a partir de informaci&oacute;n oficial. La presente gu&iacute;a tiene como objetivo documentar todas las funcionalidades de la API.</p>
            </section>

            <section id="publico">
                <div class="page-header">
                    <h2>P&uacute;blico</h2>
                </div>

                <p>Esta documentaci&oacute;n est&aacute; orientada a desarrolladores de aplicaciones (sitios web, m&oacute;viles u otras)
                    que requieran incorporar a sus sistemas consultas de datos de contribuyentes, pol&iacute;ticas monetarias
                    y tasas de cambio de Per&uacute;. Cada usuario de SunApi deber&aacute; tener asociado una <code>apikey</code>
                    (inf&oacute;rmate acerca de <a class="btn-link" href="#apikey">c&oacute;mo obtener una clave API</a>)
                    que le permitir&aacute; acceder a todas las funcionalidades.
                    La presente gu&iacute;a proporciona una introducci&oacute;n al uso de la API y material de referencia acerca de los par&aacute;metros
                    disponibles.
                </p>

            </section>

            <section id="apikey">
                <div class="page-header">
                    <h2>C&oacute;mo obtener una clave API</h2>
                </div>

                <p>La clave API o <code>apikey</code> es el c&oacute;digo &uacute;nico de usuario que le permitir&aacute; acceder a las funcionalidades
                    de SunApi. Para obtener una clave API debe iniciar sesi&oacute;n, mediante Google o GitHub, desde la
                    <a class="btn-link" href="https://sunapiperu.com">p&aacute;gina principal</a> de SunApi Per&uacute;.</p>

                <p>Una vez iniciada la sesi&oacute;n, puede ver su <code>apikey</code> en la secci&oacute;n de <b>Usuario</b> de la configuraci&oacute;n.
                    De igual forma esta <code>apikey</code> le ser&aacute; enviada por correo electr&oacute;nico.</p>
                    <?php /*Note que para comenzar a utilizar SunApi Per&uacute; debe elegir tambi&eacute;n un plan de los disponibles.*/ ?>

                <div align="center">
                    <img class="img-responsive" src="/img/apikeyuser.png"/>
                </div>

            </section>

            <section id="solicitudes">
                <div class="page-header">
                    <h2>Solicitudes</h2>
                </div>

                <p>La comunicaci&oacute;n con la API se realiza mediante solicitudes REST, empleando siempre
                    el m&eacute;todo <code>GET</code> a trav&eacute;s del protocolo seguro <code class="code-api-https-mini">https</code>.
                    Todas las respuestas a las solicitudes se obtienen en formato JSON.
                    Una solicitud debe respetar la siguiente estructura:</p>

                <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/<i>funcionalidad</i>?<i><b>parametros</b></i></span></pre>

                <p>donde <code><i><b>parametros</b></i></code> identifica a los par&aacute;metros de entrada que espera
                    el
                    servicio.
                    Algunos par&aacute;metros son obligatorios y otros opcionales. Como es norma en las direcciones URL,
                    los par&aacute;metros se separan con el car&aacute;cter <code>&</code>. Uno de los par&aacute;metros
                    obligatorios para todas las solicitudes es <code>apikey</code>, la clave privada
                    asignada a tu usuario (inf&oacute;rmate acerca de <a class="btn-link" href="#apikey">c&oacute;mo obtener
                        una clave API</a>).
                    Todas las solicitudes realizadas deben incluir esta informaci&oacute;n.
                </p>

                <div align="center">
                    <img class="img-responsive img-api" src="/img/consulta_api_web.png"/>
                </div>

                <p>A continuaci&oacute;n se describen todas las funcionalidades de la API y se muestran ejemplos de
                    posibles solicitudes:</p>

                <!---------------------------------------------------------------------------------------->

                <?php /*<div id="consultaContribuyentes">
                    <h3>Consultar contribuyentes</h3>

                    <p>Permite realizar la b&uacute;squeda de uno o m&aacute;s contribuyentes por su nombre o raz&oacute;n social.
                        Una solicitud para consultar contribuyentes debe respetar la siguiente estructura:</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/contribuyente?<i><b>parametros</b></i></span></pre>

                    <h4>Par&aacute;metros obligatorios</h4>

                    <p><code>nombre</code>: es la frase (parte del nombre o raz&oacute;n social) por la cual se realiza la b&uacute;squeda.</p>

                    <p><code>apikey</code>: es la clave privada asignada a tu usuario (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#apikey">c&oacute;mo
                            obtener una clave API</a>).</p>

                    <h4>Par&aacute;metros opcionales</h4>

                    <p><code>todos</code>: par&aacute;metro opcional que permite filtrar los contribuyentes por su
                        estado.
                        Acepta valor <code>true</code> para mostrar todos los contribuyentes (inhabilitados o no) o valor <code>false</code> para mostrar s&oacute;lo los contribuyentes
                        activos.
                        Si se ignora este par&aacute;metro por defecto se filtran solamente los contribuyentes activos.</p>

                    <h4>Ejemplo de solicitud consultar contribuyentes</h4>

                    <p>La siguiente solicitud devuelve el listado de todos los contribuyentes (activos e inhabilitados) cuyo nombre contiene la
                        frase
                        <code>bar restaurant</code>.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/contribuyente?<i>nombre=bar%20restaurant&todos=true&apikey=TU_API_KEY</i></span></pre>

                    <p>Al cambiar el par&aacute;metro <code>todos</code> se puede modificar la solicitud inicial
                        para que devuelva solamente los clientes activos (<code>false</code>).
                        De igual forma si se ignora este par&aacute;metro opcional, se filtran s&oacute;lo los
                        contribuyentes
                        activos.</p>

                    <h4>Respuesta del servidor</h4>
                    <p align="justify">
                        El servidor retornar&aacute; como respuesta un objeto HttpResponse que incluir&aacute;:
                    <ol>
                        <li>
                            Un objeto <code>JSON</code> con la lista de contribuyentes encontrados.
                        </li>
                        <li>
                            El <a class="btn-link" href="#codigoshttp">c&oacute;digo Http de estado de la solicitud</a>.
                        </li>
                    </ol>
                    <p>
                        El servidor retorna una estructura <code>JSON</code> como la siguiente:
                    </p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>[</samp></li><li><span class="pln">    </span><samp>{</samp></li><li><span class="pln">      </span><samp>"ruc"</samp><samp>:</samp><span class="code-json-att"> "20000000001"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"estado"</samp><samp>:</samp><span class="code-json-att"> "Activo"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"nombre"</samp><samp>:</samp><span class="code-json-att"> "Bar Restaurant Gente de Zona"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"departamento"</samp><samp>:</samp><span class="code-json-att"> "Lambayeque"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"provincia"</samp><samp>:</samp><span class="code-json-att"> "Chiclayo"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"distrito"</samp><samp>:</samp><span class="code-json-att"> "Jose Leonardo Ortiz"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"calle"</samp><samp>:</samp><span class="code-json-att"> "Los Pasos"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"numero"</samp><samp>:</samp><span class="code-json-att"> "504"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"ubigeo"</samp><samp>:</samp><span class="code-json-att"> "160101"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"mapa"</samp><samp>:</samp><span class="code-json-att"> "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Peru"</span></li><li class="L7"><span class="pln">    </span><samp>},</samp></li><li><span class="pln">    </span><samp>{</samp></li><li><span class="pln">      </span><samp>...</samp></li><li class="L7"><span class="pln">    </span><samp>},</samp></li><li class="L7"><span class="pln">    </span><samp>...</samp></li><li class="L7"><samp>]</samp></li></ol></pre>

                    <p>Si no existen resultados o se envi&oacute; alg&uacute;n par&aacute;metro incorrecto se devuelve un mensaje <code>JSON</code> informativo.</p>

                </div>*/ ?>

                <!---------------------------------------------------------------------------------------->

                <div id="consultaContribuyenteRUC">
                    <h3>Consultar contribuyente por RUC</h3>

                    <p>Permite realizar la b&uacute;squeda de un contribuyente seg&uacute;n su n&uacute;mero de RUC.
                        Una solicitud para consultar un contribuyente por RUC debe respetar la siguiente estructura:</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/contribuyente?<i><b>parametros</b></i></span></pre>

                    <h4>Par&aacute;metros obligatorios</h4>

                    <p><code>ruc</code>: es el n&uacute;mero de RUC por el que se realiza la b&uacute;squeda del
                        contribuyente.
                    </p>

                    <p><code>apikey</code>: es la clave privada asignada a tu usuario (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#apikey">c&oacute;mo
                            obtener una clave API</a>).</p>

                    <h4>Ejemplo de solicitud consultar contribuyente por RUC</h4>

                    <p>La siguiente solicitud devuelve el contribuyente cuyo n&uacute;mero de RUC es igual a
                        <code>20000000001</code>.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/contribuyente?<i>ruc=20000000001&apikey=TU_API_KEY</i></span></pre>

                    <h4>Respuesta del servidor</h4>
                    <p>
                        El servidor retornar&aacute; como respuesta un objeto HttpResponse que incluir&aacute;:
                    </p>
                    <ol>
                        <li>
                            Un objeto <code>JSON</code> con la informaci&oacute;n del contribuyente.
                        </li>
                        <li>
                            El <a class="btn-link" href="#codigoshttp">c&oacute;digo Http de estado de la solicitud</a>.
                        </li>
                    </ol>
                    <p>
                        El servidor retorna una estructura <code>JSON</code> como la siguiente:
                    </p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"ruc"</samp><samp>:</samp><span class="code-json-att"> "20000000001"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"estado"</samp><samp>:</samp><span class="code-json-att"> "Activo"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"nombre"</samp><samp>:</samp><span class="code-json-att"> "Bar Restaurant Gente de Zona"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"departamento"</samp><samp>:</samp><span class="code-json-att"> "Lambayeque"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"provincia"</samp><samp>:</samp><span class="code-json-att"> "Chiclayo"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"distrito"</samp><samp>:</samp><span class="code-json-att"> "Jose Leonardo Ortiz"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"via"</samp><samp>:</samp><span class="code-json-att"> "Av."</span><samp>,</samp></li><li><span class="pln">    </span><samp>"calle"</samp><samp>:</samp><span class="code-json-att"> "Los Pasos"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"numero"</samp><samp>:</samp><span class="code-json-att"> "504"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"ubigeo"</samp><samp>:</samp><span class="code-json-att"> "160101"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"mapa"</samp><samp>:</samp><span class="code-json-att"> "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Peru"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Si no existen resultados o se envi&oacute; alg&uacute;n par&aacute;metro incorrecto se devuelve un mensaje <code>JSON</code> informativo.</p>

                </div>

                <!---------------------------------------------------------------------------------------->

                <?php /*<div id="consultaContribuyentesBajas">
                    <h3>Consultar contribuyentes dados de baja</h3>

                    <p>Permite realizar la b&uacute;squeda de contribuyentes que fueron dados de baja definitiva o de oficio.
                        Una solicitud para consultar contribuyentes dados de baja debe respetar la siguiente estructura:</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/historial/contribuyente?<i><b>parametros</b></i></span></pre>

                    <h4>Par&aacute;metros obligatorios</h4>

                    <p><code>nombre</code>: es la frase (parte del nombre o raz&oacute;n social) por la cual se realiza la b&uacute;squeda.</p>

                    <p><code>apikey</code>: es la clave privada asignada a tu usuario (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#apikey">c&oacute;mo
                            obtener una clave API</a>).</p>

                    <h4>Ejemplo de solicitud consultar contribuyentes dados de baja</h4>

                    <p>La siguiente solicitud devuelve el listado de todos los contribuyentes dados de baja (definitiva o de oficio)
                        cuyo nombre contiene la frase
                        <code>bar restaurant</code>.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/historial/contribuyente?<i>nombre=bar%20restaurant&apikey=TU_API_KEY</i></span></pre>

                    <h4>Respuesta del servidor</h4>
                    <p>
                        El servidor retornar&aacute; como respuesta un objeto HttpResponse que incluir&aacute;:
                    </p>
                    <ol>
                        <li>
                            Un objeto <code>JSON</code> con la lista de contribuyentes encontrados.
                        </li>
                        <li>
                            El <a class="btn-link" href="#codigoshttp">c&oacute;digo Http de estado de la solicitud</a>.
                        </li>
                    </ol>
                    <p>
                        El servidor retorna una estructura <code>JSON</code> como la siguiente:
                    </p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>[</samp></li><li><span class="pln">    </span><samp>{</samp></li><li><span class="pln">      </span><samp>"ruc"</samp><samp>:</samp><span class="code-json-att"> "20000000001"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"estado"</samp><samp>:</samp><span class="code-json-att"> "Baja definitiva"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"nombre"</samp><samp>:</samp><span class="code-json-att"> "Bar Restaurant Gente de Zona"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"departamento"</samp><samp>:</samp><span class="code-json-att"> "Lambayeque"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"provincia"</samp><samp>:</samp><span class="code-json-att"> "Chiclayo"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"distrito"</samp><samp>:</samp><span class="code-json-att"> "Jose Leonardo Ortiz"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"calle"</samp><samp>:</samp><span class="code-json-att"> "Los Pasos"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"numero"</samp><samp>:</samp><span class="code-json-att"> "504"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"ubigeo"</samp><samp>:</samp><span class="code-json-att"> "160101"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"mapa"</samp><samp>:</samp><span class="code-json-att"> "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Peru"</span></li><li class="L7"><span class="pln">    </span><samp>},</samp></li><li><span class="pln">    </span><samp>{</samp></li><li><span class="pln">      </span><samp>...</samp></li><li class="L7"><span class="pln">    </span><samp>},</samp></li><li class="L7"><span class="pln">    </span><samp>...</samp></li><li class="L7"><samp>]</samp></li></ol></pre>

                    <p>Si no existen resultados o se envi&oacute; alg&uacute;n par&aacute;metro incorrecto se devuelve un mensaje <code>JSON</code> informativo.</p>

                </div>*/ ?>

                <!---------------------------------------------------------------------------------------->

                <div id="consultaContribuyenteRUCHistorico">
                    <h3>Consultar contribuyente dado de baja por RUC</h3>

                    <p>Permite realizar la b&uacute;squeda de un contribuyente dado de baja definitiva o de oficio,
                        seg&uacute;n su n&uacute;mero de RUC. Una solicitud para consultar un contribuyente
                        dado de baja por RUC debe respetar la siguiente estructura:</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/historial/contribuyente?<i><b>parametros</b></i></span></pre>

                    <h4>Par&aacute;metros obligatorios</h4>

                    <p><code>ruc</code>: es el n&uacute;mero de RUC por el que se realiza la b&uacute;squeda del
                        contribuyente.
                    </p>

                    <p><code>apikey</code>: es la clave privada asignada a tu usuario (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#apikey">c&oacute;mo
                            obtener una clave API</a>).</p>

                    <h4>Ejemplo de solicitud consultar contribuyente dado de baja por RUC</h4>

                    <p>La siguiente solicitud devuelve el contribuyente cuyo n&uacute;mero de RUC es igual a
                        <code>20000000001</code>.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/historial/contribuyente?<i>ruc=20000000001&apikey=TU_API_KEY</i></span></pre>

                    <h4>Respuesta del servidor</h4>
                    <p>
                        El servidor retornar&aacute; como respuesta un objeto HttpResponse que incluir&aacute;:
                    </p>
                    <ol>
                        <li>
                            Un objeto <code>JSON</code> con la informaci&oacute;n del contribuyente.
                        </li>
                        <li>
                            El <a class="btn-link" href="#codigoshttp">c&oacute;digo Http de estado de la solicitud</a>.
                        </li>
                    </ol>
                    <p>
                        El servidor retorna una estructura <code>JSON</code> como la siguiente:
                    </p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"ruc"</samp><samp>:</samp><span class="code-json-att"> "20000000001"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"estado"</samp><samp>:</samp><span class="code-json-att"> "Baja definitiva"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"nombre"</samp><samp>:</samp><span class="code-json-att"> "Bar Restaurant Gente de Zona"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"departamento"</samp><samp>:</samp><span class="code-json-att"> "Lambayeque"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"provincia"</samp><samp>:</samp><span class="code-json-att"> "Chiclayo"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"distrito"</samp><samp>:</samp><span class="code-json-att"> "Jose Leonardo Ortiz"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"via"</samp><samp>:</samp><span class="code-json-att"> "Av."</span><samp>,</samp></li><li><span class="pln">    </span><samp>"calle"</samp><samp>:</samp><span class="code-json-att"> "Los Pasos"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"numero"</samp><samp>:</samp><span class="code-json-att"> "504"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"ubigeo"</samp><samp>:</samp><span class="code-json-att"> "160101"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"mapa"</samp><samp>:</samp><span class="code-json-att"> "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Peru"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Si no existen resultados o se envi&oacute; alg&uacute;n par&aacute;metro incorrecto se devuelve un mensaje <code>JSON</code> informativo.</p>

                </div>

                <!---------------------------------------------------------------------------------------->

                <div id="validarRUC">
                    <h3>Validar n&uacute;mero de RUC</h3>

                    <p>Permite validar la estructura de un n&uacute;mero de RUC.
                        Una solicitud para validar un n&uacute;mero de RUC debe respetar la siguiente estructura:
                    </p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/validar_ruc?<i><b>parametros</b></i></span></pre>

                    <h4>Par&aacute;metros obligatorios</h4>

                    <p><code>ruc</code>: es el n&uacute;mero de RUC para validar.</p>

                    <p><code>apikey</code>: es la clave privada asignada a tu usuario (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#apikey">c&oacute;mo
                            obtener una clave API</a>).</p>

                    <h4>Ejemplo de solicitud para validar n&uacute;mero de RUC</h4>

                    <p>La siguiente solicitud devuelve si el n&uacute;mero de RUC: <code>20000000001</code> est&aacute; o
                        no
                        correcto.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/validar_ruc?<i>ruc=20000000001&apikey=TU_API_KEY</i></span></pre>

                    <h4>Respuesta del servidor</h4>
                    <p>
                        El servidor retornar&aacute; como respuesta un objeto HttpResponse que incluir&aacute;:
                    </p>
                    <ol>
                        <li>
                            Un objeto <code>JSON</code> con un campo <code>message</code>.
                        </li>
                        <li>
                            El <a class="btn-link" href="#codigoshttp">c&oacute;digo Http de estado de la solicitud</a>.
                        </li>
                    </ol>
                    <p>
                        El servidor retorna una estructura <code>JSON</code> como la siguiente:
                    </p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"valido"</samp><samp>:</samp><span class="code-json-bool"> true</span><samp>,</samp></li><li><span class="pln">    </span><samp>"mensaje"</samp><samp>:</samp><span class="code-json-att"> "Numero de RUC correcto"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Si el n&uacute;mero de RUC no presenta la estructura correcta el servidor emite la respuesta:</p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"valido"</samp><samp>:</samp><span class="code-json-bool"> false</span><samp>,</samp></li><li><span class="pln">    </span><samp>"mensaje"</samp><samp>:</samp><span class="code-json-att"> "Numero de RUC incorrecto"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Si se envi&oacute; alg&uacute;n par&aacute;metro incorrecto se devuelve un mensaje <code>JSON</code> informativo.</p>

                </div>

                <!---------------------------------------------------------------------------------------->

                <div id="tasaCambio">
                    <h3>Consultar tasa de cambio del Sol</h3>

                    <p>Permite consultar la tasa de cambio del Sol peruano respecto a una moneda extranjera.
                        Una solicitud para consultar la tasa de cambio monetaria del Sol debe respetar la siguiente estructura:
                    </p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/soles?<i><b>parametros</b></i></span></pre>

                    <h4>Par&aacute;metros obligatorios</h4>

                    <p><code>apikey</code>: es la clave privada asignada a tu usuario (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#apikey">c&oacute;mo
                            obtener una clave API</a>).</p>

                    <h4>Par&aacute;metros opcionales</h4>

                    <p><code>moneda</code>: tipo de moneda a la que se le aplica la tasa de cambio (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#monedas">los tipos de monedas admitidos</a>). Si se omite este par&aacute;metro se devuelve la tasa de cambio del Sol
                        respecto al D&oacute;lar (USD).</p>

                    <p><code>fecha</code>: par&aacute;metro para especificar la fecha en que se desea ver la tasa de
                        cambio. El
                        formato se debe
                        corresponder a <code>d-m-Y</code>, ej: <code>28-04-2014</code>. Si no existen valores para la
                        fecha
                        indicada se retorna el valor inmediato inferior
                        a la fecha se&ntilde;alada.</p>

                    <h4>Ejemplo de solicitud consultar tasa de cambio del Sol</h4>

                    <p>La siguiente solicitud devuelve la tasa de cambio del Sol respecto al Euro (<code>eur</code>) en
                        la fecha
                        <code>04-05-2016</code>.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/soles?<i>moneda=eur&fecha=04-05-2016&apikey=YOUR_API_KEY</i></span></pre>

                    <p>Si se obvia el par&aacute;metro <code>moneda</code> se obtiene la tasa de cambio del Sol respecto
                        al D&oacute;lar
                        (USD).
                        De igual forma si se ignora el par&aacute;metro <code>fecha</code> se obtiene la tasa de cambio
                        actual.
                    </p>

                    <h4>Respuesta del servidor</h4>
                    <p>
                        El servidor retornar&aacute; como respuesta un objeto HttpResponse que incluir&aacute;:
                    </p>
                    <ol>
                        <li>
                            Un objeto <code>JSON</code> con la informaci&oacute;n sobre la tasa de cambio.
                        </li>
                        <li>
                            El <a class="btn-link" href="#codigoshttp">c&oacute;digo Http de estado de la solicitud</a>.
                        </li>
                    </ol>
                    <p>
                        El servidor retorna una estructura <code>JSON</code> como la siguiente:
                    </p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"fecha_publicacion"</samp><samp>:</samp><span class="code-json-att"> "03-05-2016"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"valor_venta"</samp><samp>:</samp><span class="code-json-bool"> 3.935</span><samp>,</samp></li><li><span class="pln">    </span><samp>"valor_compra"</samp><samp>:</samp><span class="code-json-bool"> 3.623</span><samp>,</samp></li><li><span class="pln">    </span><samp>"moneda"</samp><samp>:</samp><span class="code-json-att"> "Euro (EUR)"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Si no existen resultados o se envi&oacute; alg&uacute;n par&aacute;metro incorrecto se devuelve un mensaje <code>JSON</code> informativo.</p>

                </div>

                <!---------------------------------------------------------------------------------------->

                <div id="calculadora">

                    <h3>Calculadora monetaria</h3>

                    <p>Permite convertir valores de una moneda a otra moneda seg&uacute;n la tasa de cambio vigente en la fecha se&ntilde;alada.
                        Una solicitud para calcular un monto monetario debe respetar la siguiente estructura:</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/calculadora?<i><b>parametros</b></i></span></pre>

                    <h4>Par&aacute;metros obligatorios:</h4>

                    <p><code>valor</code>: monto monetario al que se aplicar&aacute; el cambio.</p>

                    <p><code>apikey</code>: es la clave privada asignada a tu usuario (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#apikey">c&oacute;mo obtener una clave API</a>).</p>

                    <h4>Par&aacute;metros Opcionales:</h4>

                    <p><code>de</code>: tipo de moneda del monto monetario al que se aplicar&aacute; el cambio
                        (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#monedas">los tipos de monedas admitidos</a>).
                        Si se omite este par&aacute;metro se aplicar&aacute; el tipo de moneda Sol (<code>PEN</code>) por defecto.</p>

                    <p><code>a</code>: tipo de moneda del valor que se obtendr&aacute; como resultado (inf&oacute;rmate acerca de <a
                                class="btn-link" href="#monedas">los tipos de monedas admitidos</a>). Si se omite este par&aacute;metro
                        se aplicar&aacute; el tipo de moneda D&oacute;lar (<code>USD</code>) por defecto.</p>

                    <p><code>decimal</code>: indica la cantidad de valores decimales a redondear despu&eacute;s del separador (coma).
                        Si se omite este par&aacute;metro se aplicar&aacute; un valor decimal equivalente a 2, o sea que el valor
                        decimal ser&aacute; redondeado a dos cifras por defecto.</p>

                    <p><code>fecha</code>: par&aacute;metro para especificar la fecha de la tasa de cambio en que se
                        desea hacer
                        el
                        c&aacute;lculo. El formato se debe corresponder a <code>d-m-Y</code>, ej:
                        <code>28-04-2016</code>.
                        Si no existen valores para la fecha indicada se utiliza el valor inmediato inferior
                        a la fecha se&ntilde;alada.</p>

                    <h4>Ejemplo de solicitud para c&aacute;lculo monetario</h4>

                    <p>La siguiente solicitud devuelve el c&aacute;lculo de <code>500.55</code> Soles (<code>pen</code>) a
                        Euros
                        (<code>eur</code>) aplicando la tasa de cambio
                        del Sol respecto al Euro en la fecha <code>28-04-2016</code> y redondeado a <code>3</code> cifras decimales.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/calculadora?<i>valor=500.55&de=pen&a=eur&fecha=28-04-2016&decimal=3&apikey=YOUR_API_KEY</i></span></pre>

                    <h4>Respuesta del servidor</h4>
                    <p>
                        El servidor retornar&aacute; como respuesta un objeto HttpResponse que incluir&aacute;:
                    </p>
                    <ol>
                        <li>
                            Un objeto <code>JSON</code> con la informaci&oacute;n sobre la conversi&oacute;n monetaria realizada.
                        </li>
                        <li>
                            El <a class="btn-link" href="#codigoshttp">c&oacute;digo Http de estado de la solicitud</a>.
                        </li>
                    </ol>
                    <p>
                        El servidor retorna una estructura <code>JSON</code> como la siguiente:
                    </p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"valor_inicial"</samp><samp>:</samp><span class="code-json-bool"> 500.55</span><samp>,</samp></li><li><span class="pln">    </span><samp>"moneda_inicial"</samp><samp>:</samp><span class="code-json-att"> "PEN"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"valor_final"</samp><samp>:</samp><span class="code-json-bool"> 129.912</span><samp>,</samp></li><li><span class="pln">    </span><samp>"moneda_final"</samp><samp>:</samp><span class="code-json-att"> "EUR"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"tasa_venta"</samp><samp>:</samp><span class="code-json-bool"> 3.853</span><samp>,</samp></li><li><span class="pln">    </span><samp>"fecha_tasa"</samp><samp>:</samp><span class="code-json-att"> "27-04-2016"</span><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Si no existen resultados o se envi&oacute; alg&uacute;n par&aacute;metro incorrecto se devuelve un mensaje <code>JSON</code> informativo.</p>

                    <p>Si se omiten los par&aacute;metros <code>de</code> <code>a</code> y <code>fecha</code>, se
                        realiza el
                        c&aacute;lculo de Soles a D&oacute;lares aplicando la tasa de cambio actual.
                        Si se especifican monedas distintas al Sol se calcula el resultado
                        de acuerdo a la tasa de compra/venta del Sol peruano.
                        Por ejemplo, para calcular un valor de <code>500 usd</code> a <code>eur</code> con la tasa
                        actual de
                        compra/venta del Sol, se debe enviar una solicitud como la siguiente:</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/calculadora?<i>valor=500&de=usd&a=eur&apikey=YOUR_API_KEY</i></span></pre>

                    <h4>Respuesta del servidor</h4>
                    <p>
                        El servidor retornar&aacute; como respuesta un objeto HttpResponse que incluir&aacute;:
                    </p>
                    <ol>
                        <li>
                            Un objeto <code>JSON</code> con la informaci&oacute;n sobre la conversi&oacute;n monetaria realizada.
                        </li>
                        <li>
                            El <a class="btn-link" href="#codigoshttp">c&oacute;digo Http de estado de la solicitud</a>.
                        </li>
                    </ol>
                    <p>
                        La estructura del objeto <code>JSON</code> devuelto por el servidor es:
                    </p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"valor_inicial"</samp><samp>:</samp><span class="code-json-bool"> 500</span><samp>,</samp></li><li><span class="pln">    </span><samp>"moneda_inicial"</samp><samp>:</samp><span class="code-json-att"> "USD"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"valor_final"</samp><samp>:</samp><span class="code-json-bool"> 422.53</span><samp>,</samp></li><li><span class="pln">    </span><samp>"moneda_final"</samp><samp>:</samp><span class="code-json-att"> "EUR"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"tasa_compra_soles_usd"</samp><samp>:</samp><span class="code-json-bool"> 3.322</span><samp>,</samp></li><li><span class="pln">    </span><samp>"fecha_tasa_usd"</samp><samp>:</samp><span class="code-json-att"> "03-05-2016"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"tasa_venta_soles_eur"</samp><samp>:</samp><span class="code-json-bool"> 3.935</span><samp>,</samp></li><li><span class="pln">    </span><samp>"fecha_tasa_eur"</samp><samp>:</samp><span class="code-json-att"> "03-05-2016"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>
                        Si se envi&oacute; alg&uacute;n par&aacute;metro incorrecto se devuelve un mensaje <code>JSON</code> informativo.</p>
                    </p>

                </div>

                <!---------------------------------------------------------------------------------------->

                <?php /*<div id="consultaEstadoPlan">
                    <div>
                        <h3>Consultar estado de mi plan</h3>
                        <p align="justify">
                            Permite consultar el estado del plan adquirido. Entre la informaci&oacute;n a consultar se puede obtener: la cantidad
                            de peticiones realizadas, la cantidad de peticiones disponibles y la fecha de renovaci&oacute;n del plan.
                            Una solicitud para consultar el estado de mi plan debe respetar la siguiente estructura:
                        </p>
                        <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/plan?<i><b>parametros</b></i></span></pre>
                    </div>
                    <div>
                        <h4>Par&aacute;metros obligatorios</h4>
                        <p><code>apikey</code>: es la clave privada asignada a tu usuario (inf&oacute;rmate acerca de <a
                                    class="btn-link" href="#apikey">c&oacute;mo
                                obtener una clave API</a>).</p>
                    </div>
                    <div>
                        <h4>Ejemplo de solicitud consultar estado de mi plan</h4>
                        <p>
                            La siguiente solicitud devuelve el estado del plan adquirido por un usuario con la apikey: <code>muep87bxs17pd6by40xe</code>.
                        </p>
                        <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api/plan?<i>apikey=muep87bxs17pd6by40xe</i></span></pre>
                    </div>
                    <div>
                        <h4>Respuesta del servidor</h4>
                        <p>
                            El servidor retornar&aacute; como respuesta un objeto HttpResponse que incluir&aacute;:
                        </p>
                        <ol>
                            <li>
                                Un objeto <code>JSON</code> con la informaci&oacute;n sobre el estado del plan.
                            </li>
                            <li>
                                El <a class="btn-link" href="#codigoshttp">c&oacute;digo Http de estado de la solicitud</a>.
                            </li>
                        </ol>
                        <p>
                            El servidor retorna una estructura <code>JSON</code> como la siguiente:
                        </p>

                        <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"plan"</samp><samp>:</samp><span class="code-json-att"> "Premium"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"peticiones"</samp><samp>:</samp><span class="code-json-bool"> 3401</span><samp>,</samp></li><li><span class="pln">    </span><samp>"disponibles"</samp><samp>:</samp><span class="code-json-bool"> 1599</span><samp>,</samp></li><li><span class="pln">    </span><samp>"renueva"</samp><samp>:</samp><span class="code-json-att"> "03-05-2016"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                        <p>Si no existen resultados o se envi&oacute; alg&uacute;n par&aacute;metro incorrecto se devuelve un mensaje <code>JSON</code> informativo.</p>

                    </div>
                </div>*/ ?>

                <!---------------------------------------------------------------------------------------->

            </section>

            <section id="unidades">
                <div class="page-header">
                    <h2>Sistemas de unidades y c&oacute;digos HTTP</h2>
                </div>

                <p>Las solicitudes y respuestas en Sunapi Per&uacute; manejan una serie de unidades como monedas, fechas, valores decimales o valores
                    boleanos, los cuales deben respetar cierta estructura. Tambi&eacute;n cada solicitud obtendr&aacute; como respuesta un c&oacute;digo
                    HTTP de estado de la solicitud junto al objeto <code>JSON</code>. A continuaci&oacute;n se describen estas unidades y c&oacute;digos HTTP.</p>

                 <div id="monedas">
                     <h3>Tipos de monedas</h3>
                     <p>Los tipos de monedas soportadas por Sunapi Per&uacute; son las siguientes:</p>
                     <p><code>PEN</code>: abreviatura para el tipo de moneda Sol.</p>
                     <p><code>USD</code>: abreviatura para el tipo de moneda D&oacute;lar de Norte Am&eacute;rica.</p>
                     <p><code>EUR</code>: abreviatura para el tipo de moneda Euro.</p>
                     <p><code>CAD</code>: abreviatura para el tipo de moneda D&oacute;lar canadiense</p>
                     <p><code>GBP</code>: abreviatura para el tipo de moneda Libra esterlina.</p>
                     <p><code>SEK</code>: abreviatura para el tipo de moneda Corona sueca.</p>
                     <p><code>CHF</code>: abreviatura para el tipo de moneda Franco suizo.</p>
                     <p><code>JPY</code>: abreviatura para el tipo de moneda Yen japon&eacute;s.</p>
                 </div>

                <div id="otrasmedidas">
                    <h3>Otras medidas</h3>
                    <p>Otras unidades de medida y formatos se detallan a continuaci&oacute;n:</p>
                    <p><code>Fecha</code>: todas las solicitudes que empleen un par&aacute;metro de tipo
                        fecha deben respetar la estrucutra <code>d-m-Y</code> (d&iacute;a-mes-a&ntilde;o), ej: <code>28-04-2014</code>.</p>
                    <p><code>Boleano</code>: todas las solicitudes que empleen un par&aacute;metro de tipo Boleano deben tener
                    como posibles valores <code>true</code> o <code>false</code>.</p>
                    <p><code>Valores decimales</code>: todas las solicitudes y respuestas que empreen un valor decimal deben
                        tener como separador decimal el simbolo <code>.</code> ej: <code>10.53</code>.</p>

                </div>

                <div id="codigoshttp">

                    <h3>C&oacute;digos Http de estados</h3>

                    <p>Los siguientes c&oacute;digos de estado son los que se emplean en Sunapi para indicar el estado de una
                    solicitud:</p>
                    <p><code>200</code>: respuesta exitosa.</p>
                    <p><code>206</code>: respuesta exitosa pero sin contenido (contenido parcial).</p>
                    <p><code>400</code>: solicitud inv&aacute;lida.</p>
                    <p><code>403</code>: acceso prohibido. </p>
                    <p><code>412</code>: precondici&oacute;n fallida. </p>

                    <p class="bg-primary"><i class="fa fa-warning"></i>
                        Puede suceder que se generen otros estados a partir de errores en la red
                        o por el empleo de servidores proxy. Para ver un listado completo de los c&oacute;digos de solicitudes REST puede
                        acceder a este <a class="blank-link" TARGET="_blank" href="http://www.restapitutorial.com/httpstatuscodes.html">v&iacute;nculo</a> (ingl&eacute;s).</p>

                </div>
            </section>

            <br><br>

        </div>

        <div class="col-md-4">
            <nav class="col-xs-3 bs-docs-sidebar">
                <ul id="sidebar" class="nav nav-stacked fixed">
                    <li>
                        <a href="#inicio">Inicio</a>
                    </li>
                    <li>
                        <a href="#introduccion">Introducci&oacute;n</a>
                    </li>
                    <li>
                        <a href="#publico">P&uacute;blico</a>
                    </li>
                    <li>
                        <a href="#apikey">C&oacute;mo obtener una clave API</a>
                    </li>
                    <li>
                        <a href="#solicitudes">Solicitudes</a>
                            <?php /*<li><a class="sub-item" href="#consultaContribuyentes">Consultar contribuyentes</a></li>*/ ?>
                            <li><a class="sub-item" href="#consultaContribuyenteRUC">Consultar contribuyente por RUC</a></li>
                            <?php /*<li><a class="sub-item" href="#consultaContribuyentesBajas">Consultar contribuyentes dados de baja</a></li>*/ ?>
                            <li><a class="sub-item" href="#consultaContribuyenteRUCHistorico">Consultar contribuyente dado de baja por RUC</a></li>
                            <li><a class="sub-item" href="#validarRUC">Validar n&uacute;mero de RUC</a></li>
                            <li><a class="sub-item" href="#tasaCambio">Consultar tasa de cambio del Sol</a></li>
                            <li><a class="sub-item" href="#calculadora">Calculadora monetaria</a></li>
                    <?php /*<li><a class="sub-item" href="#consultaEstadoPlan">Consultar estado de mi plan</a></li>*/ ?>
                    </li>

                    <li>
                        <a href="#unidades">Sistemas de unidades y c&oacute;digos</a>
                        <li><a class="sub-item" href="#monedas">Tipos de monedas</a></li>
                        <li><a class="sub-item" href="#otrasmedidas">Otras medidas</a></li>
                        <li><a class="sub-item" href="#codigoshttp">C&oacute;digos Http de estados</a></li>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

</div>

<script>
    $('body').scrollspy({
        target: '.bs-docs-sidebar',
        offset: 40
    });
</script>

</body>
</html>