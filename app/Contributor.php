<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Contributor extends Eloquent{

    protected $connection = 'mongodb';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'contributors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ruc', 'name', 'state', 'geo', 'depart', 'prov', 'dist', 'strT', 'str', 'numb'];

    protected $maps = ['state' => 'estado', 'name' => 'nombre', 'depart' => 'departamento', 'prov' => 'provincia', 'dist' => 'distrito', 'strT' => 'via', 'str' => 'calle', 'numb' => 'numero', 'ubigeo' => 'geo'];

    protected $hidden = ['state', '_id', 'name', 'geo', 'depart', 'prov', 'dist', 'strT', 'str', 'numb', 'updated_at'];

    protected $appends = ['estado', 'nombre', 'departamento', 'provincia', 'distrito', 'via', 'calle', 'numero', 'ubigeo', 'mapa'];

    public function getEstadoAttribute()
    {
        switch ($this->state) {
            case 1:
                return "Activo";
            case 0:
                return "Baja definitiva";
            case 2:
                return "Baja de oficio";
            case 3:
                return "Suspension temporal";
            case 4:
                return "Baja provisional por Oficio";
            case 5:
                return "Baja provisional";
            case 6:
                return "Baja multiples inscripciones";
            case 7:
                return "Num. interno Identif";
            case 8:
                return "Inhabilitado-Vent.un";
            case 9:
                return "Anulacion error SUNAT";
            case 10:
                return "Anulacion provicional por acto ilicito";
            case 11:
                return "Anulacion por acto ilicito";
            case 12:
                return "Otros obligados";
            default:
                return "Desconocido";
        }
    }

    public function getNombreAttribute()
    {
        return $this->name;
    }

    public function getDepartamentoAttribute()
    {
        return $this->depart;
    }

    public function getProvinciaAttribute()
    {
        return $this->prov;
    }

    public function getDistritoAttribute()
    {
        return $this->dist;
    }

    public function getNumeroAttribute()
    {
        return $this->numb;
    }

    public function getViaAttribute()
    {
        return $this->strT;
    }

    public function getCalleAttribute()
    {
        return $this->str;
    }
    
    public function getUbigeoAttribute()
    {
        return $this->geo;
    }

    public function getMapaAttribute(){
        return 'https://www.google.com/maps/place/' . $this->depart . ',' . $this->prov . ','. $this->dist . ',' . $this->str . ',+Peru';
    }
}