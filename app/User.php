<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'picture', 'password', 'api_key'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relacion embed con plan
     *
     */
    public function plan()
    {
        return $this->embedsOne('App\UserPlan');
    }

    /**
     * Relacion embed con pay
     *
     */
    public function pay()
    {
        return $this->embedsOne('App\UserPay');
    }

    /**
     * Relacion One-to-Many con historial de pagos
     *
     */
    public function payments()
    {
        return $this->embedsMany('App\UserPayHistory');
    }

    public function availableQueries(){
        return $this->plan->plan_query_count - $this->plan->current_query_count;
    }

    public function getPlanLabelStyle(){
        //Si la cantidad dispobible es menor que la 3 parte es warning
        //Si la cantidad disponible es menor que la 4 parte es peligro
        //Caso contrario es ok
        $division3 = $this->plan->plan_query_count / 3;
        $division4 = $this->plan->plan_query_count / 4;

        if($this->availableQueries() < $division4){
            return "label-ultra-danger";
        } elseif($this->availableQueries() < $division3){
            return "label-warning";
        } else {
            return "label-success";
        }
    }
}
