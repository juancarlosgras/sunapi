<?php

namespace App\Library;

use Illuminate\Support\Facades\Mail;

class EmailManagement
{
    public static function sendmail($to_name, $to_email, $message) {
        $data = array(
            'name' => $to_name,
            'email' => $to_email,
            'apikey' => $message
        );
        $to = $to_name;
        $name = $to_email;
        Mail::send('emails.ewelcome', $data, function ($m) use ($to, $name) {
            $m->from('no-reply@sunapiperu.com', 'SunApi Per�');
            $m->to($to, $name)->subject('�Te damos la bienvenida a SunApi Per�!');
        });
    }
}