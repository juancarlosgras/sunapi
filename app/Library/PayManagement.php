<?php

namespace App\Library;

require_once 'payusdk/PayU.php';
use App\CloudPlan;
use App\User;
use App\UserPay;
use App\UserPayHistory;
use App\UserPlan;
use DateTime;
use Environment;
use Illuminate\Support\Facades\Mail;
use PayUParameters;
use PayUTokens;
use PayU;
use PayUCountries;
use PayUPayments;

class PayManagement
{
    /**
     * Funcionalidad para comprar un plan
     */
    public static function buyPlan($plan_code, $user, $user_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_dni, $cc_number,
                            $cc_exp_date, $cc_sec_code, $cc_type)
    {
        //1. Buscar el plan de referencia
        //2. Establecer parametros del pago
        //3. Realizar pago
        //4. Buscar el usuario y registrar datos del pago
        //5. Registrar historial del pago
        //6. Asignar el plan
        //7. Guardar los datos del usuario
        //8. Retornar respuesta

        //1. Buscar el plan de referencia
        $plan = CloudPlan::where('plan_code', '=', $plan_code)->first();
        $value = $plan->plan_cost;
        $current_date = new \DateTime('now');
        $reference = "sunapi_peru_" . $plan->plan_code . "_" . $plan->plan_cost . "_" . $user_id . "_" . $current_date->format('Y-m-d-H-i-s');

        //2. Establecer parametros del pago
        Environment::setPaymentsCustomUrl(env('URL_PAY_PAYMENTS'));
        Environment::setReportsCustomUrl(env('URL_PAY_REPORTS'));
        Environment::setSubscriptionsCustomUrl(env('URL_PAY_SUBSCRIPTIONS'));
        PayU::$merchantId = env('PAY_MERCHANT_ID');
        PayU::$apiKey = env('PAY_API_KEY');
        PayU::$apiLogin = env('PAY_API_LOGIN');

        $parameters = array(
            // -- Datos generales --
            PayUParameters::ACCOUNT_ID => env('PAY_ACCOUNT_ID'),
            PayUParameters::REFERENCE_CODE => $reference,
            PayUParameters::DESCRIPTION => "Pago plan " . $plan->plan_code,

            // -- Valores --
            PayUParameters::VALUE => $value,
            PayUParameters::CURRENCY => "USD",

            // -- Comprador
            PayUParameters::BUYER_NAME => $buyer_name,
            PayUParameters::BUYER_EMAIL => $buyer_email,
            PayUParameters::BUYER_CONTACT_PHONE => $buyer_phone,
            PayUParameters::BUYER_DNI => $buyer_dni,
            PayUParameters::BUYER_STREET => "-", //"Avenida de la poesia",
            PayUParameters::BUYER_STREET_2 => "-", //"160",
            PayUParameters::BUYER_CITY => "-", //"Cuzco",
            PayUParameters::BUYER_STATE => "-", //"CU",
            PayUParameters::BUYER_COUNTRY => "PE",
            PayUParameters::BUYER_POSTAL_CODE => "000000",
            PayUParameters::BUYER_PHONE => $buyer_phone,

            // -- pagador --
            PayUParameters::PAYER_NAME => $buyer_name,
            PayUParameters::PAYER_EMAIL => $buyer_email,
            PayUParameters::PAYER_CONTACT_PHONE => $buyer_phone,
            PayUParameters::PAYER_DNI => $buyer_dni,
            PayUParameters::PAYER_BIRTHDATE => '', //'1980-06-22',
            PayUParameters::PAYER_STREET => "-", //"av abancay",
            PayUParameters::PAYER_STREET_2 => "-", //"cra 4",
            PayUParameters::PAYER_CITY => "-", //"Iquitos",
            PayUParameters::PAYER_STATE => "-", //"LO",
            PayUParameters::PAYER_COUNTRY => "PE",
            PayUParameters::PAYER_POSTAL_CODE => "00000",
            PayUParameters::PAYER_PHONE => $buyer_phone,

            // -- Datos de la tarjeta de cr�dito --
            PayUParameters::CREDIT_CARD_NUMBER => $cc_number,
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $cc_exp_date,
            PayUParameters::CREDIT_CARD_SECURITY_CODE => $cc_sec_code,
            PayUParameters::PAYMENT_METHOD => $cc_type, //VISA||MASTERCARD
            PayUParameters::INSTALLMENTS_NUMBER => "1", //Ingrese aqu� el n�mero de cuotas.
            PayUParameters::COUNTRY => PayUCountries::PE,

            // -- Session id del device --
            PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
            PayUParameters::IP_ADDRESS => "127.0.0.1",
            PayUParameters::PAYER_COOKIE => "pt1t38347bs6jc9ruv2ecpv7o2",
            PayUParameters::USER_AGENT => "Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
        );

        //3. Realizar pago
        $responseType = "error";
        $response = PayUPayments::doAuthorizationAndCapture($parameters);

        //4. Buscar el usuario y registrar datos del pago
        if ($user == NULL) {
            $user = User::find($user_id);
        }

        $userPay = new UserPay();
        $userPay->buyer_name = $buyer_name;
        $userPay->buyer_email = $buyer_email;
        $userPay->buyer_phone = $buyer_phone;
        $userPay->buyer_dni = $buyer_dni;
        $userPay->cc_number = Util::encrypt($cc_number, $cc_type);
        $userPay->cc_exp_date = $cc_exp_date;
        $userPay->cc_sec_code = Util::encrypt($cc_sec_code, $cc_type);
        $userPay->cc_type = $cc_type;
        $userPay->pay_token = "0";
        $userPay->verified = false;

        //5. Registrar historial del pago
        $historyPay = new UserPayHistory();
        $historyPay->reference_code = $reference;
        $historyPay->pay_value = $value;
        if ($response) {
            $historyPay->response_code = $response->transactionResponse->responseCode;
            $historyPay->response_code2 = $response->code;
            $historyPay->pay_state = $response->transactionResponse->state;
            $historyPay->order_id = $response->transactionResponse->orderId;
            $historyPay->transaction_id = $response->transactionResponse->transactionId;
            if ($response->transactionResponse->state == "PENDING") {
                $historyPay->pending_reason = $response->transactionResponse->pendingReason;
                $responseType = "warn";
            }
            elseif ($response->transactionResponse->state == "APPROVED") {
                //6. Asignar el plan
                $userPlan = new UserPlan();
                $userPlan->plan_id = $plan->id;
                $userPlan->plan_name = $plan->plan_name;
                $userPlan->current_query_count = 0;
                $userPlan->plan_query_count = $plan->query_count;
                $today = new DateTime();
                $date = strtotime("+1 month", strtotime($today->format("Y-m-d")));
                $payDate = new DateTime(date("d-m-Y", $date));
                $userPlan->pay_date = $payDate->format('d/m/Y');
                $userPlan->paid_days = 0;
                $userPlan->payed = true;
                $user->plan()->save($userPlan);
                $responseType = "ok";
                $userPay->verified = true;
            }
        } else {
            $historyPay->pay_state = "ERROR_PENDING";
            $responseType = "warn";
        }

        //7. Guardar los datos del usuario
        $user->pay()->save($userPay);
        $user->payments()->save($historyPay);
        $user->save();

        //8. Retornar respuesta
        switch ($responseType) {
            case "warn":
                //Se env�a un email al usuario notificando que se est� validando el pago
                $data_email = array(
                    'name' => $user->name,
                    'plan' => $plan->plan_name
                );
                $to = $user->email;
                $name = $user->name;
                Mail::send('emails.verifypay', $data_email, function ($m) use ($to, $name) {
                    $m->from('no-reply@sunapiperu.com', 'SunApi Per�');
                    $m->to($to, $name)->subject('Estamos validando tu pago');
                });
                return response()->json(
                    array(
                        'responseType' => $responseType,
                        'userEmail' => $user->email
                    ));
            case "error":
                return response()->json(
                    array(
                        'responseType' => $responseType,
                        'userEmail' => $user->email
                    ));
            default: //OK
                //Se env�a un email al usuario notificando que ha adquirido el plan
                $data_email = array(
                    'name' => $user->name,
                    'plan' => $plan->plan_name
                );
                $to = $user->email;
                $name = $user->name;
                Mail::send('emails.truepay', $data_email, function ($m) use ($to, $name, $plan) {
                    $m->from('no-reply@sunapiperu.com', 'SunApi Per�');
                    $m->to($to, $name)->subject('Has adquirido el plan '.$plan->plan_name);
                });
                return response()->json(
                    array(
                        'responseType' => $responseType,
                        'planLabelStyle' => $user->getPlanLabelStyle(),
                        'availableQueries' => $user->availableQueries(),
                        'planQueryCount' => $user->plan->plan_query_count,
                        'planName' => $plan->plan_name,
                        'payDate' => $user->plan->pay_date
                    ));
        }
    }

    /**
     * Funcionalidad para simular el pago
     */
    public static function buyPlanTest($plan_code, $user, $user_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_dni, $cc_number,
                                   $cc_exp_date, $cc_sec_code, $cc_type)
    {
        //1. Buscar el plan de referencia
        $plan = CloudPlan::where('plan_code', '=', $plan_code)->first();
        $value = $plan->plan_cost;
        $current_date = new \DateTime('now');
        $reference = "sunapi_peru_" . $plan->plan_code . "_" . $plan->plan_cost . "_" . $user_id . "_" . $current_date->format('Y-m-d-H-i-s');

        //2. Buscar usuario
        if ($user == NULL) {
            $user = User::find($user_id);
        }

        //2. Asignar datos de comprador
        $userPay = new UserPay();
        $userPay->buyer_name = $buyer_name;
        $userPay->buyer_email = $buyer_email;
        $userPay->buyer_phone = $buyer_phone;
        $userPay->buyer_dni = $buyer_dni;
        $userPay->cc_number = Util::encrypt($cc_number, $cc_type);
        $userPay->cc_exp_date = $cc_exp_date;
        $userPay->cc_sec_code = Util::encrypt($cc_sec_code, $cc_type);
        $userPay->cc_type = $cc_type;
        $userPay->pay_token = "0";
        $userPay->verified = false;

        //3. Registrar historial del pago
        $historyPay = new UserPayHistory();
        $historyPay->reference_code = $reference;
        $historyPay->pay_value = $value;
        $historyPay->response_code = "55325";
        $historyPay->response_code2 = "55325";
        $historyPay->pay_state = "APPROVED";
        $historyPay->order_id = "883848398334534";
        $historyPay->transaction_id = "8834573846";

        //4. Asignar el plan
        $userPlan = new UserPlan();
        $userPlan->plan_id = $plan->id;
        $userPlan->plan_name = $plan->plan_name;
        $userPlan->current_query_count = 0;
        $userPlan->plan_query_count = $plan->query_count;
        $today = new DateTime();
        $date = strtotime("+1 month", strtotime($today->format("Y-m-d")));
        $payDate = new DateTime(date("d-m-Y", $date));
        $userPlan->pay_date = $payDate->format('d/m/Y');
        $userPlan->paid_days = 0;
        $userPlan->payed = true;
        $user->plan()->save($userPlan);
        $userPay->verified = true;

        //5. Guardar los datos del usuario
        $user->pay()->save($userPay);
        $user->payments()->save($historyPay);
        $user->save();

        return response()->json(
            array(
                'responseType' => "ok",
                'planLabelStyle' => $user->getPlanLabelStyle(),
                'availableQueries' => $user->availableQueries(),
                'planQueryCount' => $user->plan->plan_query_count,
                'planName' => $plan->plan_name,
                'payDate' => $user->plan->pay_date
            ));
    }

    /**
     * Funcionalidad para actualizar la informaci�n del pago
     */
    public static function updatePayInfo($user, $buyer_name, $buyer_email, $buyer_phone, $buyer_dni, $cc_number, $cc_type,
                                  $cc_exp_date, $cc_sec_code){
        $userPay = new UserPay();
        $userPay->buyer_name = $buyer_name;
        $userPay->buyer_email = $buyer_email;
        $userPay->buyer_phone = $buyer_phone;
        $userPay->buyer_dni = $buyer_dni;
        $userPay->cc_number = Util::encrypt($cc_number, $cc_type);
        $userPay->cc_exp_date = $cc_exp_date;
        $userPay->cc_sec_code = Util::encrypt($cc_sec_code, $cc_type);
        $userPay->cc_type = $cc_type;
        $userPay->pay_token = "0";
        $userPay->verified = false;
        $user->pay()->save($userPay);
    }

    /**
     * Funcionalidad para pagar un plan directamente sin token
     */
    public static function payPlan($plan_id, $user, $buyer_name, $buyer_email, $buyer_phone, $buyer_dni, $cc_number,
                                   $cc_exp_date, $cc_sec_code, $cc_type)
    {
        //1. Buscar el plan de referencia
        //2. Establecer parametros del pago
        //3. Realizar pago
        //4. Registrar historial del pago
        //5. Guardar los datos del usuario
        //6. Retornar respuesta

        //1. Buscar el plan de referencia
        $plan = CloudPlan::where('id', '=', $plan_id)->first();
        $value = $plan->plan_cost;
        $current_date = new \DateTime('now');
        $reference = "sunapi_peru_" . $plan->plan_code . "_" . $plan->plan_cost . "_" . $user->id . "_" . $current_date->format('Y-m-d-H-i-s');

        //2. Establecer parametros del pago
        Environment::setPaymentsCustomUrl(env('URL_PAY_PAYMENTS'));
        Environment::setReportsCustomUrl(env('URL_PAY_REPORTS'));
        Environment::setSubscriptionsCustomUrl(env('URL_PAY_SUBSCRIPTIONS'));
        PayU::$merchantId = env('PAY_MERCHANT_ID');
        PayU::$apiKey = env('PAY_API_KEY');
        PayU::$apiLogin = env('PAY_API_LOGIN');

        $parameters = array(
            // -- Datos generales --
            PayUParameters::ACCOUNT_ID => env('PAY_ACCOUNT_ID'),
            PayUParameters::REFERENCE_CODE => $reference,
            PayUParameters::DESCRIPTION => "Pago plan " . $plan->plan_code,

            // -- Valores --
            PayUParameters::VALUE => $value,
            PayUParameters::CURRENCY => "USD",

            // -- Comprador
            PayUParameters::BUYER_NAME => $buyer_name,
            PayUParameters::BUYER_EMAIL => $buyer_email,
            PayUParameters::BUYER_CONTACT_PHONE => $buyer_phone,
            PayUParameters::BUYER_DNI => $buyer_dni,
            PayUParameters::BUYER_STREET => "-", //"Avenida de la poesia",
            PayUParameters::BUYER_STREET_2 => "-", //"160",
            PayUParameters::BUYER_CITY => "-", //"Cuzco",
            PayUParameters::BUYER_STATE => "-", //"CU",
            PayUParameters::BUYER_COUNTRY => "PE",
            PayUParameters::BUYER_POSTAL_CODE => "000000",
            PayUParameters::BUYER_PHONE => $buyer_phone,

            // -- pagador --
            PayUParameters::PAYER_NAME => $buyer_name,
            PayUParameters::PAYER_EMAIL => $buyer_email,
            PayUParameters::PAYER_CONTACT_PHONE => $buyer_phone,
            PayUParameters::PAYER_DNI => $buyer_dni,
            PayUParameters::PAYER_BIRTHDATE => '', //'1980-06-22',
            PayUParameters::PAYER_STREET => "-", //"av abancay",
            PayUParameters::PAYER_STREET_2 => "-", //"cra 4",
            PayUParameters::PAYER_CITY => "-", //"Iquitos",
            PayUParameters::PAYER_STATE => "-", //"LO",
            PayUParameters::PAYER_COUNTRY => "PE",
            PayUParameters::PAYER_POSTAL_CODE => "00000",
            PayUParameters::PAYER_PHONE => $buyer_phone,

            // -- Datos de la tarjeta de cr�dito --
            PayUParameters::CREDIT_CARD_NUMBER => $cc_number,
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $cc_exp_date,
            PayUParameters::CREDIT_CARD_SECURITY_CODE => $cc_sec_code,
            PayUParameters::PAYMENT_METHOD => $cc_type, //VISA||MASTERCARD
            PayUParameters::INSTALLMENTS_NUMBER => "1", //Ingrese aqu� el n�mero de cuotas.
            PayUParameters::COUNTRY => PayUCountries::PE,

            // -- Session id del device --
            PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
            PayUParameters::IP_ADDRESS => "127.0.0.1",
            PayUParameters::PAYER_COOKIE => "pt1t38347bs6jc9ruv2ecpv7o2",
            PayUParameters::USER_AGENT => "Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
        );

        //3. Realizar pago
        $responseType = "error";
        $response = PayUPayments::doAuthorizationAndCapture($parameters);

        //4. Registrar historial del pago
        $historyPay = new UserPayHistory();
        $historyPay->reference_code = $reference;
        $historyPay->pay_value = $value;
        if ($response) {
            $historyPay->response_code = $response->transactionResponse->responseCode;
            $historyPay->response_code2 = $response->code;
            $historyPay->pay_state = $response->transactionResponse->state;
            $historyPay->order_id = $response->transactionResponse->orderId;
            $historyPay->transaction_id = $response->transactionResponse->transactionId;
            if ($response->transactionResponse->state == "PENDING") {
                $historyPay->pending_reason = $response->transactionResponse->pendingReason;
                $responseType = "warn";
            } elseif ($response->transactionResponse->state == "APPROVED") {
                $responseType = "ok";
                $user->pay->verified = true;
                $user->pay->save();
            }
        } else {
            $historyPay->pay_state = "ERROR_PENDING";
            $responseType = "warn";
        }

        //5. Guardar los datos del usuario
        $user->payments()->save($historyPay);
        $user->save();

        //6. Retornar respuesta
        switch ($responseType) {
            case "warn":
                //Se env�a un email al usuario notificando que se est� validando el pago
                return response()->json(
                    array(
                        'responseType' => $responseType,
                        'userEmail' => $user->email
                    ));
            case "error":
                return response()->json(
                    array(
                        'responseType' => $responseType,
                        'userEmail' => $user->email
                    ));
            default: //OK
                return response()->json(
                    array(
                        'responseType' => $responseType,
                        'userEmail' => $user->email
                    ));
        }
    }

    /**
     * Funcionalidad para generar un token de pago. Retorna el token generado para una tarjeta
     */
    public static function createPayToken($payer_name, $payer_dni, $credit_number, $expiration_date, $credit_type)
    {
        //1. Establecer parametros del pago
        Environment::setPaymentsCustomUrl(env('URL_PAY_PAYMENTS'));
        Environment::setReportsCustomUrl(env('URL_PAY_REPORTS'));
        Environment::setSubscriptionsCustomUrl(env('URL_PAY_SUBSCRIPTIONS'));
        PayU::$merchantId = env('PAY_MERCHANT_ID');
        PayU::$apiKey = env('PAY_API_KEY');
        PayU::$apiLogin = env('PAY_API_LOGIN');

        //2. Establecer parametros del token
        $parameters = array(
            PayUParameters::PAYER_NAME => $payer_name,
            PayUParameters::PAYER_ID => $payer_dni,
            PayUParameters::PAYER_DNI => $payer_dni,
            PayUParameters::CREDIT_CARD_NUMBER => $credit_number,
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $expiration_date,
            PayUParameters::PAYMENT_METHOD => $credit_type
        );

        //3. Crear token
        $response = PayUTokens::create($parameters);
        if ($response) {
            return $response->creditCardToken->creditCardTokenId;
        }

        return "-1";
    }

    /**
     * Funcionalidad para cobrar un token de pago.
     */
    public static function chargePayToken($plan_id, $user, $buyer_name, $buyer_email, $buyer_phone, $buyer_dni, $token,
                                          $cc_sec_code, $cc_type)
    {
        //1. Buscar el plan de referencia
        //2. Establecer parametros del pago
        //3. Realizar pago
        //4. Registrar historial del pago
        //5. Guardar los datos del usuario
        //6. Retornar respuesta

        //1. Buscar el plan de referencia
        $plan = CloudPlan::where('id', '=', $plan_id)->first();
        $value = $plan->plan_cost;
        $current_date = new \DateTime('now');
        $reference = "sunapi_peru_" . $plan->plan_code . "_" . $plan->plan_cost . "_" . $user->id . "_" . $current_date->format('Y-m-d-H-i-s');

        //2. Establecer parametros del pago
        Environment::setPaymentsCustomUrl(env('URL_PAY_PAYMENTS'));
        Environment::setReportsCustomUrl(env('URL_PAY_REPORTS'));
        Environment::setSubscriptionsCustomUrl(env('URL_PAY_SUBSCRIPTIONS'));
        PayU::$merchantId = env('PAY_MERCHANT_ID');
        PayU::$apiKey = env('PAY_API_KEY');
        PayU::$apiLogin = env('PAY_API_LOGIN');

        $parameters = array(
            PayUParameters::ACCOUNT_ID => env('PAY_ACCOUNT_ID'),
            PayUParameters::REFERENCE_CODE => $reference,
            PayUParameters::DESCRIPTION => "Pago plan " . $plan->plan_code,

            // -- Valores --
            PayUParameters::VALUE => $value,
            PayUParameters::CURRENCY => "USD",

            // -- Comprador
            PayUParameters::BUYER_NAME => $buyer_name,
            PayUParameters::BUYER_EMAIL => $buyer_email,
            PayUParameters::BUYER_CONTACT_PHONE => $buyer_phone,
            PayUParameters::BUYER_DNI => $buyer_dni,
            PayUParameters::BUYER_STREET => "-", //"Avenida de la poesia",
            PayUParameters::BUYER_STREET_2 => "-", //"160",
            PayUParameters::BUYER_CITY => "-", //"Cuzco",
            PayUParameters::BUYER_STATE => "-", //"CU",
            PayUParameters::BUYER_COUNTRY => "PE",
            PayUParameters::BUYER_POSTAL_CODE => "000000",
            PayUParameters::BUYER_PHONE => $buyer_phone,

            // -- pagador --
            PayUParameters::PAYER_NAME => $buyer_name,
            PayUParameters::PAYER_EMAIL => $buyer_email,
            PayUParameters::PAYER_CONTACT_PHONE => $buyer_phone,
            PayUParameters::PAYER_DNI => $buyer_dni,
            PayUParameters::PAYER_BIRTHDATE => '', //'1980-06-22',

            //Ingrese aqu� la direcci�n del pagador.
            PayUParameters::PAYER_STREET => "-", //"av abancay",
            PayUParameters::PAYER_STREET_2 => "-", //"cra 4",
            PayUParameters::PAYER_CITY => "-", //"Iquitos",
            PayUParameters::PAYER_STATE => "-", //"LO",
            PayUParameters::PAYER_COUNTRY => "PE",
            PayUParameters::PAYER_POSTAL_CODE => "00000",
            PayUParameters::PAYER_PHONE => $buyer_phone,

            // DATOS DEL TOKEN
            PayUParameters::TOKEN_ID => $token,
            PayUParameters::CREDIT_CARD_SECURITY_CODE => $cc_sec_code,
            PayUParameters::PAYMENT_METHOD => $cc_type, //VISA||MASTERCARD
            PayUParameters::INSTALLMENTS_NUMBER => "1", //Ingrese aqu� el n�mero de cuotas.
            PayUParameters::COUNTRY => PayUCountries::PE,

            PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
            PayUParameters::IP_ADDRESS => "127.0.0.1",
            PayUParameters::PAYER_COOKIE => "pt1t38347bs6jc9ruv2ecpv7o2",
            PayUParameters::USER_AGENT => "Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
        );

        //3. Realizar pago
        $responseType = "error";
        $response = PayUPayments::doAuthorizationAndCapture($parameters);

        //4. Registrar historial del pago
        $user->pay->verified = false;
        $historyPay = new UserPayHistory();
        $historyPay->reference_code = $reference;
        $historyPay->pay_value = $value;
        if ($response) {
            $historyPay->response_code = $response->transactionResponse->responseCode;
            $historyPay->response_code2 = $response->code;
            $historyPay->pay_state = $response->transactionResponse->state;
            $historyPay->order_id = $response->transactionResponse->orderId;
            $historyPay->transaction_id = $response->transactionResponse->transactionId;
            if ($response->transactionResponse->state == "PENDING") {
                $historyPay->pending_reason = $response->transactionResponse->pendingReason;
                $responseType = "warn";
            } elseif ($response->transactionResponse->state == "APPROVED") {
                $responseType = "ok";
                $user->pay->verified = true;
            }
        } else {
            $historyPay->pay_state = "ERROR_PENDING";
            $responseType = "warn";
        }

        //5. Guardar los datos del usuario
        $user->pay->save();
        $user->payments()->save($historyPay);
        $user->save();

        //6. Retornar respuesta
        switch ($responseType) {
            case "warn":
                //Se env�a un email al usuario notificando que se est� validando el pago
                return response()->json(
                    array(
                        'responseType' => $responseType,
                        'userEmail' => $user->email
                    ));
            case "error":
                return response()->json(
                    array(
                        'responseType' => $responseType,
                        'userEmail' => $user->email
                    ));
            default: //OK
                return response()->json(
                    array(
                        'responseType' => $responseType,
                        'userEmail' => $user->email
                    ));
        }
    }

    /**
     * Funcionalidad para consultar un token de pago.
     */
    public static function consultToken($token)
    {
        Environment::setPaymentsCustomUrl(env('URL_PAY_PAYMENTS'));
        Environment::setReportsCustomUrl(env('URL_PAY_REPORTS'));
        Environment::setSubscriptionsCustomUrl(env('URL_PAY_SUBSCRIPTIONS'));
        PayU::$merchantId = env('PAY_MERCHANT_ID');
        //PayU::$isTest = true;
        PayU::$apiKey = env('PAY_API_KEY');
        PayU::$apiLogin = env('PAY_API_LOGIN');

        //Parametros filtros para b�squeda
        $parameters = array(
            //PayUParameters::PAYER_ID => "10"
            PayUParameters::TOKEN_ID => $token,
            //PayUParameters::START_DATE=> "2010-01-01T12:00:00",
            //PayUParameters::END_DATE=> "2015-01-01T12:00:00"
        );

        $response=PayUTokens::find($parameters);

        if($response) {
            $credit_cards = $response->creditCardTokenList;
            foreach ($credit_cards as $credit_card) {
                $credit_card->creditCardTokenId;
                $credit_card->maskedNumber;
                $credit_card->payerId;
                $credit_card->identificationNumber;
                $credit_card->paymentMethod;
            }
        }

        return $response;
    }

    /**
     * Funcionalidad para eliminar un token de pago.
     */
    public static function deleteToke($token, $user_id)
    {
        Environment::setPaymentsCustomUrl(env('URL_PAY_PAYMENTS'));
        Environment::setReportsCustomUrl(env('URL_PAY_REPORTS'));
        Environment::setSubscriptionsCustomUrl(env('URL_PAY_SUBSCRIPTIONS'));
        PayU::$merchantId = env('PAY_MERCHANT_ID');
        PayU::$apiKey = env('PAY_API_KEY');
        PayU::$apiLogin = env('PAY_API_LOGIN');

        $parameters = array(
            PayUParameters::PAYER_ID => $user_id,
            PayUParameters::TOKEN_ID => $token
        );
        return PayUTokens::remove($parameters);
    }
}