<?php
/**
 * Created by IntelliJ IDEA.
 * User: des08-user
 * Date: 10/03/2016
 * Time: 11:58
 */

namespace App\Http\Controllers;

use DateTime;
use Symfony\Component\HttpFoundation\Request;

class ApiQAController extends Controller
{

    //**********************************************************
    //***** Funcionalidades API Pruebas ************************
    //**********************************************************

    /**
     * Busca un contribuyente por su RUC o nombre
     *
    */
    public function searchContributor(Request $request)
    {
        //Validar condiciones de negocio
        $apikey = $request->input('apikey');
        if ($apikey == null)
            return response()->json(array("mensaje" => "Debe ingresar un atributo 'apikey' con el valor correspondiente."), 403);

        if ($apikey != 'sunapi')
            return response()->json(array("mensaje" => "El valor del atributo 'apikey' debe ser 'sunapi' para la API de pruebas."), 403);

        //Consultar información
        $ruc = $request->input('ruc');
        if ($ruc != null) {
            return response()->json(array(
                "ruc" => $ruc,
                "estado" => "Activo",
                "nombre" => "Bar Restaurant Gente de Zona",
                "departamento" => "Lambayeque",
                "provincia" => "Chiclayo",
                "distrito" => "Jose Leonardo Ortiz",
                "calle" => "Los Pasos",
                "numero" => "504",
                "ubigeo" => "160101",
                "mapa" => "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"
                ));
        }
        else {
            return response()->json(array("mensaje" => "Debe ingresar un atributo 'ruc' para la busqueda."), 400);
        }
        //Deshabilitdo hasta mejorar rendimiento
        /*else {
            $name = $request->input('nombre');
            if ($name != null) {
                $all = $request->input('todos');
                if ($all != null && strtolower($all) == 'true') {
                    $customers = response()->json(array(array(
                        "ruc" => "20000000001",
                        "estado" => "Activo",
                        "nombre" => "Bar Restaurant Gente de Zona",
                        "departamento" => "Lambayeque",
                        "provincia" => "Chiclayo",
                        "distrito" => "Jose Leonardo Ortiz",
                        "calle" => "Los Pasos",
                        "numero" => "504",
                        "ubigeo" => "160101",
                        "mapa" => "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"
                    ),
                        array(
                            "ruc" => "20000000005",
                            "estado" => "Suspension temporal",
                            "nombre" => "Hostal Bar Restaurant la Gozadera",
                            "departamento" => "Ancash",
                            "provincia" => "Caraz",
                            "distrito" => "Jose Leonardo Ortiz",
                            "calle" => "Nueva Victoria",
                            "numero" => "113",
                            "ubigeo" => "021201",
                            "mapa" => "https://www.google.com/maps/place/Ancash,Huaylas,Caraz,NUEVA VICTORIA,+Per%C3%BA"
                        )));
                } else {
                    $customers = response()->json(array(array(
                        "ruc" => "20000000001",
                        "estado" => "Activo",
                        "nombre" => "Bar Restaurant Gente de Zona",
                        "departamento" => "Lambayeque",
                        "provincia" => "Chiclayo",
                        "distrito" => "Jose Leonardo Ortiz",
                        "calle" => "Los Pasos",
                        "numero" => "504",
                        "ubigeo" => "160101",
                        "mapa" => "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"
                    ),
                        array(
                            "ruc" => "20000000006",
                            "estado" => "Activo",
                            "nombre" => "Bar Restaurant Trova",
                            "departamento" => "Lambayeque",
                            "provincia" => "Chiclayo",
                            "distrito" => "Jose Leonardo Ortiz",
                            "calle" => "Los Pasos",
                            "numero" => "504",
                            "ubigeo" => "160101",
                            "mapa" => "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"
                        )));
                }
                return $customers;
            } else {
                return response()->json(array("mensaje" => "Debe ingresar un atributo de busqueda como 'ruc' o 'nombre'."), 400);
            }
        }*/
    }

    /**
     * Busca un contribuyente en el registro Histórico (bajas definitivas o de oficio) por su RUC o nombre
     *
     */
    public function searchHistoricalContributor(Request $request)
    {
        //Validar condiciones de negocio
        $apikey = $request->input('apikey');
        if ($apikey == null)
            return response()->json(array("mensaje" => "Debe ingresar un atributo 'apikey' con el valor correspondiente."), 403);

        if ($apikey != 'sunapi')
            return response()->json(array("mensaje" => "El valor del atributo 'apikey' debe ser 'sunapi' para la API de pruebas."), 403);

        //Consultar información
        $ruc = $request->input('ruc');
        if ($ruc != null) {
            return response()->json(array(
                "ruc" => $ruc,
                "estado" => "Baja de oficio",
                "nombre" => "Bar Restaurant Gente de Zona",
                "departamento" => "Lambayeque",
                "provincia" => "Chiclayo",
                "distrito" => "Jose Leonardo Ortiz",
                "calle" => "Los Pasos",
                "numero" => "504",
                "ubigeo" => "160101",
                "mapa" => "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"
            ));
        }
        else {
            return response()->json(array("mensaje" => "Debe ingresar un atributo 'ruc' para la busqueda."), 400);
        }
        //Deshabilitdo hasta mejorar rendimiento
        /*else {
            $name = $request->input('nombre');
            if ($name != null) {
                return response()->json(array(array(
                    "ruc" => "20000000001",
                    "estado" => "Baja de oficio",
                    "nombre" => "Bar Restaurant Gente de Zona",
                    "departamento" => "Lambayeque",
                    "provincia" => "Chiclayo",
                    "distrito" => "Jose Leonardo Ortiz",
                    "calle" => "Los Pasos",
                    "numero" => "504",
                    "ubigeo" => "160101",
                    "mapa" => "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"
                ),
                    array(
                        "ruc" => "20000000005",
                        "estado" => "Baja definitiva",
                        "nombre" => "Hostal Bar Restaurant la Gozadera",
                        "departamento" => "Ancash",
                        "provincia" => "Caraz",
                        "distrito" => "Jose Leonardo Ortiz",
                        "calle" => "Nueva Victoria",
                        "numero" => "113",
                        "ubigeo" => "021201",
                        "mapa" => "https://www.google.com/maps/place/Ancash,Huaylas,Caraz,NUEVA VICTORIA,+Per%C3%BA"
                    )));
            } else {
                return response()->json(array("mensaje" => "Debe ingresar un atributo de busqueda como 'ruc' o 'nombre'."), 400);
            }
        }*/
    }

    /**
     * Valida la estructura de un número de RUC
     *
     */
    public function validateRUC(Request $request)
    {
        //Validar condiciones de negocio
        $apikey = $request->input('apikey');
        if ($apikey == null)
            return response()->json(array("mensaje" => "Debe ingresar un atributo 'apikey' con el valor correspondiente."), 403);

        if ($apikey != 'sunapi')
            return response()->json(array("mensaje" => "El valor del atributo 'apikey' debe ser 'sunapi' para la API de pruebas."), 403);

        //Consultar información
        $ruc = $request->input('ruc');
        if ($ruc == null) {
            return response()->json(array("mensaje" => "Debe ingresar un parametro 'ruc'"), 400);
        }

        return response()->json(array(
            "valido" => true,
            "mensaje" => "Numero de RUC correcto"
        ));
    }

    /**
     * Obtiene valores de cambio de monedas
     *
     */
    public function getCurrencyChange(Request $request)
    {
        //Validar condiciones de negocio
        $apikey = $request->input('apikey');
        if ($apikey == null)
            return response()->json(array("mensaje" => "Debe ingresar un atributo 'apikey' con el valor correspondiente."), 403);

        if ($apikey != 'sunapi')
            return response()->json(array("mensaje" => "El valor del atributo 'apikey' debe ser 'sunapi' para la API de pruebas."), 403);

        //Consultar información
        $fecha_publicacion = $request->input('fecha');
        $moneda = $request->input('moneda');

        if($moneda === null){
            $moneda = 'USD';
        }
        else{
            $moneda = strtoupper($moneda);
            $available_currencies = array("USD", "EUR", "CAD", "GBP", "SEK", "CHF", "JPY");
            if (!in_array($moneda, $available_currencies)) {
                return response()->json(array("mensaje" => "Tipo de moneda '" . $moneda . "' no valida. Ingrese un tipo de moneda de las disponibles:  USD, EUR, CAD, GBP, SEK, CHF, JPY"), 400);
            }
        }

        if($fecha_publicacion === null){
            $fecha_publicacion = new DateTime();
        }
        else if(!$this->validateDate($fecha_publicacion)){
            return response()->json(array("mensaje" => "Fecha invalida. Ingrese una fecha con un formato valido: d-m-Y, ej: 23-05-2015"), 400);
        }
        else{
            $fecha_publicacion = new DateTime($fecha_publicacion);
        }

        return response()->json(array(
            "fecha_publicacion" => $fecha_publicacion->format('d-m-Y'),
            "valor_venta" => 3.935,
            "valor_compra" => 3.623,
            "moneda" => "Euro (EUR)"
        ));
    }

    private function validateDate($date, $format = 'd-m-Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Calcula un monto según el cambio de monedas
     *
     */
    public function calc(Request $request){
        //Validar condiciones de negocio
        $apikey = $request->input('apikey');
        if ($apikey == null)
            return response()->json(array("mensaje" => "Debe ingresar un atributo 'apikey' con el valor correspondiente."), 403);

        if ($apikey != 'sunapi')
            return response()->json(array("mensaje" => "El valor del atributo 'apikey' debe ser 'sunapi' para la API de pruebas."), 403);

        //Consultar información
        $de = $request->input('de');
        $a = $request->input('a');
        $valor = $request->input('valor');
        $fecha_publicacion = $request->input('fecha');
        $round = $request->input('decimal');

        //Validación de valor a convertir
        if ($valor == null) {
            return response()->json(array("mensaje" => "Debe ingresar un parametro 'valor'"), 400);
        } else if(!is_numeric($valor)){
            return response()->json(array("mensaje" => "El parametro 'valor' debe contener un valor numerico"), 400);
        }

        //Validación de moneda de procedencia
        if($de === null){
            $de = 'PEN';
        }
        else{
            $de = strtoupper($de);
            $available_currencies = array("PEN", "USD", "EUR", "CAD", "GBP", "SEK", "CHF", "JPY");
            if (!in_array($de, $available_currencies)) {
                return response()->json(array("mensaje" => "Tipo de moneda parametro 'de'='" . $de . "' no valida. Ingrese un tipo de moneda de las disponibles:  PEN, USD, EUR, CAD, GBP, SEK, CHF, JPY"), 400);
            }
        }

        //Validación de moneda de destino
        if($a === null){
            $a = 'USD';
        }
        else{
            $a = strtoupper($a);
            $available_currencies = array("PEN", "USD", "EUR", "CAD", "GBP", "SEK", "CHF", "JPY");
            if (!in_array($a, $available_currencies)) {
                return response()->json(array("mensaje" => "Tipo de moneda parametro 'a'='" . $a . "' no valida. Ingrese un tipo de moneda de las disponibles: PEN, USD, EUR, CAD, GBP, SEK, CHF, JPY"), 400);
            }
        }

        //Validación de fecha de tasa de cambio
        if($fecha_publicacion === null){
            $fecha_publicacion = new DateTime();
        }
        else if(!$this->validateDate($fecha_publicacion)){
            return response()->json(array("mensaje" => "Fecha invalida. Ingrese una fecha con un formato valido: d-m-Y, ej: 23-05-2015"), 400);
        }
        else{
            $fecha_publicacion = new DateTime($fecha_publicacion);
        }

        if($round === null){
            $round = 2;
        }

        //Calculo
        if($de == 'PEN'){
            return response()->json(array(
                "valor_inicial" => floatval($valor),
                "moneda_inicial" => $de,
                "valor_final" => round(129.91234534345435, $round),
                "moneda_final" => $a,
                "tasa_venta" => 3.853,
                "fecha_tasa" => $fecha_publicacion->format('d-m-Y')));
        } elseif($a == 'PEN'){
            return response()->json(array(
                "valor_inicial" => floatval($valor),
                "moneda_inicial" => $de,
                "valor_final" => round(129.91234534345435, $round),
                "moneda_final" => $a,
                "tasa_compra" => 3.853,
                "fecha_tasa" => $fecha_publicacion->format('d-m-Y')));
        } else {
            return response()->json(array(
                "valor_inicial" => floatval($valor),
                "moneda_inicial" => $de,
                "valor_final" => round(129.91234534345435, $round),
                "moneda_final" => $a,
                "tasa_compra_soles_" . strtolower($de) => 3.853,
                "fecha_tasa_" . strtolower($de) => $fecha_publicacion->format('d-m-Y'),
                "tasa_venta_soles_" . strtolower($a) => 3.853,
                "fecha_tasa_" . strtolower($a) => $fecha_publicacion->format('d-m-Y')));
        }
    }

    /**
     * Obtiene los valores del plan del usuario
     *
     */
    public function plan(Request $request){
        //Validar condiciones de negocio
        $apikey = $request->input('apikey');
        if ($apikey == null)
            return response()->json(array("mensaje" => "Debe ingresar un atributo 'apikey' con el valor correspondiente."), 403);

        if ($apikey != 'sunapi')
            return response()->json(array("mensaje" => "El valor del atributo 'apikey' debe ser 'sunapi' para la API de pruebas."), 403);

        $fecha = new DateTime();
        return response()->json(array(
            "plan" => "Premium",
            "peticiones" => 3401,
            "disponibles" => 1599,
            "renueva" => $fecha->format('d-m-Y')));
    }

}