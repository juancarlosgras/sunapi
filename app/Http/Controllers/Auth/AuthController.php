<?php

namespace App\Http\Controllers\Auth;

use App\CloudPlan;
use App\DeletedToken;
use App\Library\PayManagement;
use App\User;
use App\UserPlan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Validator;
use Socialite;
use Auth;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use DateTime;

class AuthController extends Controller
{
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    //protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     */
    public function __construct()
    {
        //$this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'picture' => $data['picture'],
            'api_key' => $data['api_key'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function redirectToProvider(Request $request, $auth){
        //Para agregar promoción
        $promo = $request->input('p');
        if ($promo != null){
            if($promo == 'hxkbpe31dfkp'){
                $request->session()->put('promo', $promo);
            }
        }
        return Socialite::driver($auth)->redirect();
    }

    public function handleProviderCallback(Request $request, $auth){
        $user = Socialite::driver($auth)->user();
        $userDB = User::where('email', '=', $user->email)->first();
        if(is_null($userDB)){
            //Se crea el usuario
            $apikey = $this->generateApiKey(40);
            $data = ['name' => $user->name, 'email' => $user->email, 'picture' => $user->avatar, 'password' => $user->token, 'api_key' => $apikey];
            $userDB = $this->create($data);

            //Se asigna el plan gratis
            //----------TEMPORAL-----------------------
            $plan = CloudPlan::where('plan_code', '=', 'FRE01')->first();
            $userPlan = new UserPlan();
            $userPlan->plan_id = $plan->id;
            $userPlan->plan_name = $plan->plan_name;
            $userPlan->current_query_count = 0;
            $userPlan->plan_query_count = $plan->query_count;
            $today = new DateTime();
            $date = strtotime("+1 month", strtotime($today->format("Y-m-d")));
            $payDate = new DateTime(date("d-m-Y", $date));
            $userPlan->pay_date = $payDate->format('d/m/Y');
            $userPlan->paid_days = 0;
            $userPlan->payed = false;
            $userDB->plan()->save($userPlan);
            $userDB->save();

            //----------TEMPORAL-----------------------

            //Email data
            $template_email = 'emails.ewelcome';
            $data_email = array(
                'name' => $user->name,
                'email' => $user->email,
                'apikey' => $apikey
            );

            //Si aplica la promoción, para cuando se ponga una promoción.
            /*$promo = $request->session()->get('promo');
            if($promo == 'hxkbpe31dfkp'){
                $endDate = $this->addPromoPlan($userDB, 'BAS01');
                $template_email = 'emails.ewelcome_promo';
                $data_email += ['promodate' => $endDate];
            }*/

            //Send welcome email
            $to = $user->email;
            $name = $user->name;
            Mail::send($template_email, $data_email, function ($m) use ($to, $name) {
                $m->from('no-reply@sunapiperu.com', 'SunApi Perú');
                $m->to($to, $name)->subject('¡Te damos la bienvenida a SunApi Perú!');
            });
        }
        else{
            if($userDB->name != $user->name || $userDB->picture != $user->avatar){
                $userDB->name = $user->name;
                $userDB->picture = $user->avatar;
                $userDB->save();
            }
        }

        Auth::login($userDB);
        $request->session()->put('user_id', $userDB->id);
        return redirect('/');
    }

    private function addPromoPlan($user, $planCode){
        $plan = CloudPlan::where('plan_code', '=', $planCode)->first();
        $userPlan = new UserPlan();
        $userPlan->plan_id = $plan->id;
        $userPlan->plan_name = $plan->plan_name;
        $userPlan->current_query_count = 0;
        $userPlan->plan_query_count = $plan->query_count;
        $today = new DateTime();
        $date = strtotime("+1 month", strtotime($today->format("Y-m-d")));
        $payDate = new DateTime(date("d-m-Y", $date));
        $userPlan->pay_date = $payDate->format('d/m/Y');
        $userPlan->paid_days = 0;
        $userPlan->payed = false;
        $userPlan->promo = true;
        $user->plan()->save($userPlan);
        $user->save();
        return $payDate->format('d/m/Y');
    }

    /**
     * Funcinalidad para simular el autenticación
     */
    public function autenticarse(Request $request){
        $userDB = User::where('email', '=', 'juancarlosgras@gmail.com')->first();
        Auth::login($userDB);
        $request->session()->put('user_id', $userDB->id);
        //Send welcome email
        /*$data = array(
            'name' => $userDB->name,
            'email' => $userDB->email,
            'apikey' => $userDB->api_key
        );
        $to = $userDB->email;
        $name = $userDB->name;
        Mail::send('emails.ewelcome', $data, function ($m) use ($to, $name) {
            $m->from('no-reply@sunapiperu.com', 'SunApi Perú');
            $m->to($to, $name)->subject('¡Te damos la bienvenida a SunApi Perú!');
        });*/
        return redirect('/');
    }

    /**
     * Asignar plan gratis
     */
    public function setFreePlan(Request $request){
        $user = $request->user();
        if ($user == NULL) {
            $user = User::find($request->session()->get('user_id'));
        }

        $plan = CloudPlan::where('plan_code', '=', $request->input('codigo'))->first();
        $userPlan = new UserPlan();
        $userPlan->plan_id = $plan->id;
        $userPlan->plan_name = $plan->plan_name;
        $userPlan->current_query_count = 0;
        $userPlan->plan_query_count = $plan->query_count;
        $today = new DateTime();
        $date = strtotime("+1 month", strtotime($today->format("Y-m-d")));
        $payDate = new DateTime(date("d-m-Y", $date));
        $userPlan->pay_date = $payDate->format('d/m/Y');
        $userPlan->paid_days = 0;
        $userPlan->payed = false;
        $user->plan()->save($userPlan);
        $user->save();

        return response()->json(
            array(
                'planLabelStyle' => $user->getPlanLabelStyle(),
                'availableQueries' => $user->availableQueries(),
                'planQueryCount' => $user->plan->plan_query_count,
                'planName' => $plan->plan_name,
                'payDate' => $user->plan->pay_date
                ));
    }

    /**
     * Comprar un plan
     */
    public function comprarPlan(Request $request){
        return PayManagement::buyPlan($request->input('plan'), $request->user(), $request->session()->get('user_id'),
            $request->input('nombre'), $request->input('correo'), $request->input('telefono'), $request->input('dni'),
            $request->input('numero'), $request->input('fecha'), $request->input('cvc'), $request->input('tipo'));
    }

    /**
     * Actualizar información de pago
     */
    public function actualizarPago(Request $request){
        $user = $request->user();
        if ($user == NULL) {
            $user = User::find($request->session()->get('user_id'));
        }

        PayManagement::updatePayInfo($user, $request->input('nombre'), $request->input('correo'),
            $request->input('telefono'), $request->input('dni'), $request->input('numero'), $request->input('tipo'),
            $request->input('fecha'), $request->input('cvc'));

        return response()->json(array('updated' => true));
    }

    /**
     * Terminar un plan
     */
    public function terminarPlan(Request $request){
        $user = $request->user();
        if ($user == NULL) {
            $user = User::find($request->session()->get('user_id'));
        }

        //No se puede eliminar un plan gratis
        if($user->plan->plan_name == 'Gratis')
            return response()->json(array('removed' => false));

        //Si tiene información de pago se elimina el token
        if($user->pay != null){
            if($user->pay->pay_token != "0") { //Si ya se ha generado el token, le pongo pendiente de eliminación.
                $delete_token = new DeletedToken();
                $delete_token->token_id = $user->pay->pay_token;
                $delete_token->buyer_dni = $user->pay->buyer_dni;
                $delete_token->deleted = false;
                $delete_token->save();
            }
            //Elimino el pago
            $user->pay->delete();
        }
        $user->plan()->delete();
        $user->save();
        return response()->json(array('removed' => true));
    }

    public function logout(Request $request){
        $request->session()->clear();
        Auth::logout();
        return redirect('/');
    }

    /**
     * Genera un código de Api Key
     *
     */
    private function generateApiKey($longitud)
    {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern) - 1;
        for ($i = 0; $i < $longitud; $i++)
            $key .= $pattern{mt_rand(0, $max)};

        return $key;
    }
}
