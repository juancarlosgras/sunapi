<?php
namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use PDF;

class InvoiceController extends Controller
{
    //En el objeto documento:
    //* incluir campo sistema [indica el sistema desde donde fue enviado el documento. ej: webapp]
    //* separar collecticón de xml, comprimir el documento.



    /**
     * Genera un documento UBL a partir de un JSON con auto-completamiento
     *
     */
    public function generateAutoUBLInvoice($invoiceJSON){
        $query = new Queries();
        $receptor = $query->searchContributorByRUC($invoiceJSON->receptor->ruc);


        $ublInvoice = '<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>';
        $ublInvoice .= '<Invoice xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2"';

        //Campos adicionales
        if(isset($invoiceJSON->campos_adicionales)) {
            $ublInvoice .= 'xmlns:extadd="urn:sunapi:extension" xmlns:cacadd="urn:sunapi:aggregates" xmlns:cbcadd="urn:sunapi:basics">'; //Close Invoice tag
            $ublInvoice .= '<ext:UBLExtensions><ext:UBLExtension><ext:ExtensionContent><extadd:ExtensionContent><cacadd:ExtraParameters>';
            foreach($invoiceJSON->campos_adicionales as $key => $val) {
                $ublInvoice .= '<cbcadd:extra name="<![CDATA[' . $key . ']]>"><![CDATA[' . $val . ']]></cbcadd:extra>';
            }
            $ublInvoice .= '</cacadd:ExtraParameters></extadd:ExtensionContent></ext:ExtensionContent></ext:UBLExtension>';
        }
        else {
            $ublInvoice .= '>'; //Close Invoice tag
            $ublInvoice .= '<ext:UBLExtensions>';
        }

        //Valores Items
        //De aqui es que se optienen los valores totales
        $itemsInvoice = '';
        $total_gravado = 0;
        $total_inafecto = 0;
        $total_exonerado = 0;
        $sub_total = 0;
        $total_descuentos = 0; //Suma de decuentos por Items y descuento global
        $total_igv = 0;
        $total_isc = 0;
        $total_otros_impuestos = 0;
        $valor_a_pagar = 0;
        $itemNumber = 1;
        foreach ($invoiceJSON->lineas as $linea) {
            $itemsInvoice .= '<cac:InvoiceLine><cbc:ID>' . $itemNumber . '</cbc:ID>';
            $itemsInvoice .= '<cbc:InvoicedQuantity unitCode="';
            $itemsInvoice .= isset($linea->unidad) ? $linea->unidad : 'NUI';
            $count = isset($linea->cantidad) ? $linea->cantidad : 1;
            $itemsInvoice .= '">' . $count . '</cbc:InvoicedQuantity>';



            $itemsInvoice .= '</cac:InvoiceLine>';
            $itemNumber++;
        }

        $ublInvoice .= '</ext:UBLExtensions>';
        $ublInvoice .= '<cbc:UBLVersionID>2.0</cbc:UBLVersionID><cbc:CustomizationID>1.0</cbc:CustomizationID>';
        $ublInvoice .= '<cbc:ID>' . $invoiceJSON->serie . '-' . $invoiceJSON->numeral . '</cbc:ID>';
        $ublInvoice .= '<cbc:IssueDate>' . $invoiceJSON->fecha_emision . '</cbc:IssueDate>';
        //$ublInvoice .= '<cac:Signature><cbc:ID>S' . $numeral . '</cbc:ID><cac:SignatoryParty><cac:PartyIdentification><cbc:ID>' . $rucEmisor . '</cbc:ID></cac:PartyIdentification><cac:PartyName><cbc:Name><![CDATA[' . $nombreEmisor . ']]></cbc:Name></cac:PartyName></cac:SignatoryParty><cac:DigitalSignatureAttachment><cac:ExternalReference><cbc:URI>#S' . $numeral . '</cbc:URI></cac:ExternalReference></cac:DigitalSignatureAttachment></cac:Signature>';
        //$ublInvoice .= '<cac:AccountingSupplierParty><cbc:CustomerAssignedAccountID>' . $rucEmisor . '</cbc:CustomerAssignedAccountID><cbc:AdditionalAccountID>6</cbc:AdditionalAccountID><cac:Party><cac:PartyName><cbc:Name><![CDATA[' . $nombreEmisor . ']]></cbc:Name></cac:PartyName><cac:PostalAddress><cbc:StreetName><![CDATA[' . $direccionEmisor . ']]></cbc:StreetName><cac:Country><cbc:IdentificationCode>PE</cbc:IdentificationCode></cac:Country></cac:PostalAddress><cac:PartyLegalEntity><cbc:RegistrationName><![CDATA[' . $nombreEmisor . ']]></cbc:RegistrationName></cac:PartyLegalEntity></cac:Party></cac:AccountingSupplierParty>';

        //Valores opcionales
        if (isset($invoiceJSON->valor_descuento_global)) {
            $ublInvoice .= '<cbc:IssueDiscount>' . $invoiceJSON->valor_descuento_global . '</cbc:IssueDiscount>';
        }

        //Subvalores
        $ublInvoice .= '<cbc:Emisor>' . $invoiceJSON->receptor->correo . '</cbc:Emisor>';
        $ublInvoice .= '<cbc:Emisor>' . $receptor->name . '</cbc:Emisor>';
        //Items
        $ublInvoice .= $itemsInvoice;
        $ublInvoice .= '</Invoice>';
        return $ublInvoice;
    }

    /**
     * Genera un documento UBL a partir de un JSON fijo
     *
     */
    public function generateFixedUBLInvoice($invoiceJSON){
        $invoice = json_decode($invoiceJSON);
        return $invoice->result;
    }

    /**
     * Genera un documento PDF de factura
     *
     */
    public function generateInvoicePDF(){
        $data = array(
            'name' => 'Amapola',
            'email' => 'amapola@grass.com',
            'apikey' => '374uy38735y834534'
        );
        $pdf = PDF::loadView('pdf.invoice', $data);
        return $pdf->download('invoice.pdf');


        /*$pdf = PDF::loadHTML(DocumentoController::getRide($documento_id));
        return $pdf->stream();*/
    }

    /**
     * Envía un documento UBL a partir de un JSON
     *
     */
    public function sendUBLInvoice($invoiceJSON){
        //Se determina si se autogeneran valores o son fijos

        //Compresss
        $ublInvoice = $this->generateAutoUBLInvoice($invoiceJSON);
        $bzstr = gzcompress($ublInvoice, 9);
        $response = '<Original>' . strlen($ublInvoice) . '</Original>';
        $response .= '<Compress>' . strlen($bzstr) . '</Compress>';
        $response .= '<cbc:UnCompress>' . strlen(gzuncompress($bzstr)) . '</cbc:UnCompress>';
        return $response;
    }
}