<?php
namespace App\Http\Controllers\Core;

use App\Currency;
use App\Contributor;
use App\Http\Controllers\Controller;
use App\User;
use DateTime;
use Exception;

class Queries extends Controller
{
    /**
     * Valida el usuario, el plan asignado y la cantidad de consultas disponibles
     *
     */
    public function validateUserAPI($apikey){
        if ($apikey == null)
            throw new Exception("Debe ingresar un atributo 'apikey' con el valor correspondiente.", 403);
        $user = User::where('api_key', '=', $apikey)->first();
        if ($user == null)
            throw new Exception("Api Key incorrecta", 403);
        else if($user->plan == null)
            throw new Exception("No tiene ningún plan activo, debe adquirir un plan para proceder.", 403);
        else if($user->plan->current_query_count == $user->plan->plan_query_count)
            throw new Exception("Ha sobrepasado la cantidad de consultas disponibles para su plan de " . $user->plan->plan_query_count . " consultas.", 403);
        return $user;
    }

    /**
     * Valida el usuario y el plan asignado
     *
     */
    public function validateUserPlan($apikey){
        if ($apikey == null)
            throw new Exception("Debe ingresar un atributo 'apikey' con el valor correspondiente.", 403);
        $user = User::where('api_key', '=', $apikey)->first();
        if ($user == null)
            throw new Exception("Api Key incorrecta", 403);
        else if($user->plan == null)
            throw new Exception("No tiene ningún plan activo, debe adquirir un plan para proceder.", 403);
        return $user;
    }

    public function validateCurrencyPlan($user, $moneda)
    {
        /*if($user->plan->plan_name == 'Gratis' && ($moneda != null && strtoupper($moneda) != 'USD'))
            throw new Exception("El plan 'Gratis' solamente admite el tipo de moneda USD para obtener la tasa de cambio, debe adquirir otro plan para proceder con su consulta.", 403);*/
    }

    public function searchContributorByRUC($ruc)
    {
        if ($ruc == null)
            throw new Exception("Debe ingresar un atributo 'ruc' para la busqueda.", 400);
        if (!$this->validateRUCNumber($ruc))
            throw new Exception("Numero de RUC incorrecto", 412);
        $customer = Contributor::where('ruc', $ruc)->first();
        if ($customer == null)
            throw new Exception("No existe el contribuyente con RUC '" . $ruc . "'", 206);
        //Deshabilitdo hasta mejorar rendimiento
        /*else {
            $name = $request->input('nombre');
            if ($name != null) {
                $all = $request->input('todos');
                if ($all != null && strtolower($all) == 'true') {
                    $customers = Contributor::whereRaw(array('$text' => array('$search' => "\"" . $name . "\"")))->get();
                } else {
                    $customers = Contributor::whereRaw(array('$text' => array('$search' => "\"" . $name . "\"")))->where('state', '=', 1)->get();
                }
                if ($customers->first() == null) {
                    return response()->json(array("mensaje" => "No existen registros para el parametro de busqueda '" . $name . "'"), 206);
                } else {
                    return $customers;
                }
            } else {
                return response()->json(array("mensaje" => "Debe ingresar un atributo de busqueda como 'ruc' o 'nombre'."), 400);
            }
        }*/
        return $customer;
    }

    public function searchHistoricalContributorByRUC($ruc)
    {
        if ($ruc == null)
            throw new Exception("Debe ingresar un atributo 'ruc' para la busqueda.", 400);
        if (!$this->validateRUCNumber($ruc))
            throw new Exception("Numero de RUC incorrecto", 412);
        $customer = Contributor::where('ruc', $ruc)->where(function($query)
        {
            $query->where('state', 0)
            ->orWhere('state', 2);
        })->first();

        if ($customer == null)
            throw new Exception("No existe el contribuyente con RUC '" . $ruc . "' en históricos", 206);
        //Deshabilitdo hasta mejorar rendimiento
        /*else {
            $name = $request->input('nombre');
            if ($name != null) {
                $all = $request->input('todos');
                if ($all != null && strtolower($all) == 'true') {
                    $customers = Customer_Old::whereRaw(array('$text' => array('$search' => "\"" . $name . "\"")))->get();
                } else {
                    $customers = Customer_Old::whereRaw(array('$text' => array('$search' => "\"" . $name . "\"")))->where('state', '=', 1)->get();
                }
                if ($customers->first() == null) {
                    return response()->json(array("mensaje" => "No existen registros para el parametro de busqueda '" . $name . "'"), 206);
                } else {
                    return $customers;
                }
            } else {
                return response()->json(array("mensaje" => "Debe ingresar un atributo de busqueda como 'ruc' o 'nombre'."), 400);
            }
        }*/
        return $customer;
    }

    public function validateRUCNumber($ruc)
    {
        if ($ruc == null)
            throw new Exception("Debe ingresar un parametro 'ruc'", 400);

        if(!is_numeric($ruc) || strlen($ruc) != 11){
            return false;
        }

        $dig01 = substr($ruc, 0, 1) * 5;
        $dig02 = substr($ruc, 1, 1) * 4;
        $dig03 = substr($ruc, 2, 1) * 3;
        $dig04 = substr($ruc, 3, 1) * 2;
        $dig05 = substr($ruc, 4, 1) * 7;
        $dig06 = substr($ruc, 5, 1) * 6;
        $dig07 = substr($ruc, 6, 1) * 5;
        $dig08 = substr($ruc, 7, 1) * 4;
        $dig09 = substr($ruc, 8, 1) * 3;
        $dig10 = substr($ruc, 9, 1) * 2;
        $dig11 = substr($ruc, 10, 1);

        $suma = (int)$dig01 + (int)$dig02 + (int)$dig03 + (int)$dig04 + (int)$dig05 + (int)$dig06 + (int)$dig07 + (int)$dig08 + (int)$dig09 + (int)$dig10;
        $residuo = $suma % 11;
        $resta = 11 - $residuo;

        if($resta == 10)
            $digChk = 0;
        else if ($resta == 11) {
            $digChk = 1;
        } else {
            $digChk = $resta;
        }

        if($dig11 == $digChk) {
            return true;
        }
        else{
            return false;
        }
    }

    public function getCurrencyChange($moneda, $fecha_publicacion)
    {
        if($moneda === null){
            $moneda = 'USD';
        }
        else{
            $moneda = strtoupper($moneda);
            $available_currencies = array("USD", "EUR", "CAD", "GBP", "SEK", "CHF", "JPY");
            if (!in_array($moneda, $available_currencies)) {
                throw new Exception("Tipo de moneda '" . $moneda . "' no valida. Ingrese un tipo de moneda de las disponibles:  USD, EUR, CAD, GBP, SEK, CHF, JPY", 400);
            }
        }

        if($fecha_publicacion === null){
            $fecha_publicacion = new DateTime();
        }
        else if(!$this->validateDate($fecha_publicacion)){
            throw new Exception("Fecha invalida. Ingrese una fecha con un formato valido: d-m-Y, ej: 23-05-2015", 400);
        }
        else{
            $fecha_publicacion = new DateTime($fecha_publicacion);
        }

        $value = $this->getCurrencyChangeInt($fecha_publicacion, $moneda);
        if ($value == null) { //En caso que no existan resultados
            throw new Exception("No se encontraron registros para la fecha seleccionada", 206);
        }

        return $value;
    }

    public function calcCurrnencyChange($valor, $round, $fecha_publicacion, $de, $a)
    {
        if ($valor == null) {
            throw new Exception("Debe ingresar un parametro 'valor'", 400);
        } else if(!is_numeric($valor)){
            throw new Exception("El parametro 'valor' debe contener un valor numerico", 400);
        }

        if($round === null){
            $round = 2;
        } elseif(!is_numeric($round)){
            throw new Exception("El parametro 'decimal' debe contener un valor numerico", 400);
        }

        //Validación de moneda de procedencia
        if($de === null){
            $de = 'PEN';
        }
        else{
            $de = strtoupper($de);
            $available_currencies = array("PEN", "USD", "EUR", "CAD", "GBP", "SEK", "CHF", "JPY");
            if (!in_array($de, $available_currencies)) {
                throw new Exception("Tipo de moneda parametro 'de'='" . $de . "' no valida. Ingrese un tipo de moneda de las disponibles:  PEN, USD, EUR, CAD, GBP, SEK, CHF, JPY", 400);
            }
        }

        //Validación de moneda de destino
        if($a === null){
            $a = 'USD';
        }
        else{
            $a = strtoupper($a);
            $available_currencies = array("PEN", "USD", "EUR", "CAD", "GBP", "SEK", "CHF", "JPY");
            if (!in_array($a, $available_currencies)) {
                throw new Exception("Tipo de moneda parametro 'a'='" . $a . "' no valida. Ingrese un tipo de moneda de las disponibles: PEN, USD, EUR, CAD, GBP, SEK, CHF, JPY", 400);
            }
        }

        //Validación de fecha de tasa de cambio
        if($fecha_publicacion === null){
            $fecha_publicacion = new DateTime();
        }
        else if(!$this->validateDate($fecha_publicacion)){
            throw new Exception("Fecha invalida. Ingrese una fecha con un formato valido: d-m-Y, ej: 23-05-2015", 400);
        }
        else{
            $fecha_publicacion = new DateTime($fecha_publicacion);
        }

        //Calculo
        if($de == 'PEN'){
            if($a != 'PEN') {
                $a_value = $this->getCurrencyChangeInt($fecha_publicacion, $a);
                if ($a_value == null) { //En caso que no existan resultados
                    throw new Exception("No se encontraron registros para la fecha seleccionada", 206);
                }
                $venta = $a_value->valor_venta;
                $calc = $valor / $venta;
                $calc = round($calc, $round);
                $fecha = $a_value->fecha_publicacion;
            } else {
                $calc = $valor;
                $venta = "";
                $fecha = "";
            }
            return response()->json(array(
                "valor_inicial" => floatval($valor),
                "moneda_inicial" => $de,
                "valor_final" => $calc,
                "moneda_final" => $a,
                "tasa_venta" => $venta,
                "fecha_tasa" => $fecha));
        } elseif($a == 'PEN'){
            $de_value = $this->getCurrencyChangeInt($fecha_publicacion, $de);
            if ($de_value == null) { //En caso que no existan resultados
                throw new Exception("No se encontraron registros para la fecha seleccionada", 206);
            }
            $calc = $valor * $de_value->valor_compra;
            $calc = round($calc, $round);
            return response()->json(array(
                "valor_inicial" => floatval($valor),
                "moneda_inicial" => $de,
                "valor_final" => $calc,
                "moneda_final" => $a,
                "tasa_compra" => $de_value->valor_compra,
                "fecha_tasa" => $de_value->fecha_publicacion));
        } else {
            $de_value = $this->getCurrencyChangeInt($fecha_publicacion, $de);
            if ($de_value == null) { //En caso que no existan resultados
                throw new Exception("No se encontraron registros para la fecha seleccionada", 206);
            }
            $a_value = $this->getCurrencyChangeInt($fecha_publicacion, $a);
            if ($a_value == null) { //En caso que no existan resultados
                throw new Exception("No se encontraron registros para la fecha seleccionada", 206);
            }
            $calc = ($valor * $de_value->valor_compra)/$a_value->valor_venta;
            $calc = round($calc, $round);
            return response()->json(array(
                "valor_inicial" => floatval($valor),
                "moneda_inicial" => $de,
                "valor_final" => $calc,
                "moneda_final" => $a,
                "tasa_compra_soles_" . strtolower($de_value->currency) => $de_value->valor_compra,
                "fecha_tasa_" . strtolower($de_value->currency) => $de_value->fecha_publicacion,
                "tasa_venta_soles_" . strtolower($a_value->currency) => $a_value->valor_venta,
                "fecha_tasa_" . strtolower($a_value->currency) => $a_value->fecha_publicacion));
        }
    }

    private function validateDate($date, $format = 'd-m-Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    private function getCurrencyChangeInt($fecha_publicacion, $moneda)
    {
        return Currency::where('issue_date', '<=', $fecha_publicacion)->where('currency', '=', $moneda)->orderBy('issue_date', 'desc')->take(1)->first();
    }
}