<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Core\InvoiceController;
use App\Http\Controllers\Core\Queries;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * Busca un contribuyente por su RUC
     *
    */
    public function searchContributor(Request $request)
    {
        try{
            $query = new Queries();
            $user = $query->validateUserAPI($request->input('apikey'));
            $customer = $query->searchContributorByRUC($request->input('ruc'));
            $user->plan->increment('current_query_count');
            return $customer;
        }
        catch(Exception $ex){
            return response()->json(array("mensaje" => $ex->getMessage()), $ex->getCode());
        }
    }

    /**
     * Busca un contribuyente en el registro Histórico (bajas definitivas o de oficio) por su RUC
     *
     */
    public function searchHistoricalContributor(Request $request)
    {
        try{
            $query = new Queries();
            $user = $query->validateUserAPI($request->input('apikey'));
            $customer = $query->searchHistoricalContributorByRUC($request->input('ruc'));
            $user->plan->increment('current_query_count');
            return $customer;
        }
        catch(Exception $ex){
            return response()->json(array("mensaje" => $ex->getMessage()), $ex->getCode());
        }
    }

    /**
     * Valida la estructura de un número de RUC
     *
     */
    public function validateRUC(Request $request)
    {
        try {
            $query = new Queries();
            $query->validateUserPlan($request->input('apikey'));
            if ($query->validateRUCNumber($request->input('ruc')))
                return response()->json(array("valido" => true, "mensaje" => "Numero de RUC correcto"));
            else
                return response()->json(array("valido" => false, "mensaje" => "Numero de RUC incorrecto"));
        } catch (Exception $ex) {
            return response()->json(array("mensaje" => $ex->getMessage()), $ex->getCode());
        }
    }

    /**
     * Obtiene valores de cambio de monedas
     *
     */
    public function getCurrencyChange(Request $request)
    {
        try{
            $query = new Queries();
            $user = $query->validateUserPlan($request->input('apikey'));
            $moneda = $request->input('moneda');
            //$query->validateCurrencyPlan($user, $moneda);
            $user->plan->increment('current_query_count');
            return $query->getCurrencyChange($moneda, $request->input('fecha'));
        }
        catch(Exception $ex){
            return response()->json(array("mensaje" => $ex->getMessage()), $ex->getCode());
        }
    }

    /**
     * Calcula un monto según el cambio de monedas
     *
     */
    public function calc(Request $request){
        try{
            $query = new Queries();
            $user = $query->validateUserPlan($request->input('apikey'));
            $de = $request->input('de');
            $a = $request->input('a');
            //$query->validateCurrencyPlan($user, $de);
            //$query->validateCurrencyPlan($user, $a);
            $user->plan->increment('current_query_count');
            return $query->calcCurrnencyChange($request->input('valor'), $request->input('decimal'), $request->input('fecha'), $de, $a);
        }
        catch(Exception $ex){
            return response()->json(array("mensaje" => $ex->getMessage()), $ex->getCode());
        }
    }

    /**
     * Obtiene los valores del plan del usuario
     *
     */
    public function plan(Request $request)
    {
        try {
            $query = new Queries();
            $user = $query->validateUserPlan($request->input('apikey'));
            return response()->json(array("plan" => $user->plan->plan_name, "peticiones" => $user->plan->current_query_count,
                "disponibles" => $user->availableQueries(), "renueva" => $user->plan->pay_date));
        } catch (Exception $ex) {
            return response()->json(array("mensaje" => $ex->getMessage()), $ex->getCode());
        }
    }

    //===============INVOICE============================

    /**
     * Carga y valida un documento json para tributos
     *
     */
    private function loadJsonDocument($document){
        $json = json_decode($document);
        //Valida estrucutura de json
        if($json === null){
            $badRequestMsm = "Debe enviar un documento json válido: [" . json_last_error_msg();
            switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    $badRequestMsm .= ' - No errors]';
                    break;
                case JSON_ERROR_DEPTH:
                    $badRequestMsm .= ' - Maximum stack depth exceeded]';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $badRequestMsm .= ' - Underflow or the modes mismatch]';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $badRequestMsm .= ' - Unexpected control character found]';
                    break;
                case JSON_ERROR_SYNTAX:
                    $badRequestMsm .= ' - Syntax error, malformed JSON]';
                    break;
                case JSON_ERROR_UTF8:
                    $badRequestMsm .= ' - Malformed UTF-8 characters, possibly incorrectly encoded]';
                    break;
                default:
                    $badRequestMsm .= ' - Unknown error]';
                    break;
            }
            throw new Exception($badRequestMsm, 400);
        }
        //Valida que exista el campo "tipo"
        if($json->tipo === null){
            throw new Exception("Debe especificar un campo 'tipo' que indique el tipo de documento.", 400);
        }
        //Valida los tipos de documentos aceptados
        $documentsTypes = array("01", "07", "08");
        if(!in_array($json->tipo, $documentsTypes)){
            throw new Exception("El campo 'tipo' solo acepta los valores: " . implode(", ", $documentsTypes), 400);
        }
        return $json;
    }

    /**
     * Obtiene una vista previa de la factura UBL a generar
     *
     */
    public function getUBLInvoicePreview(Request $request)
    {
        try {
            $json = $this->loadJsonDocument($request->getContent());
            $ctrl = new InvoiceController();
            switch($json->tipo){
                case "01":
                    $ublDocument = $ctrl->generateAutoUBLInvoice($json);
                    break;
                case "07":
                    $ublDocument = $ctrl->generateAutoUBLInvoice($json);
                    break;
                default:
                    $ublDocument = $ctrl->generateAutoUBLInvoice($json);
                    break;
            }
            return response($ublDocument, 200)->header('Content-Type', 'text/xml');
        } catch (Exception $ex) {
            return response($this->builXmlError($ex->getCode(), $ex->getMessage() . $ex->getTraceAsString()), 400)->header('Content-Type', 'text/xml');
        }
    }

    /**
     * Obtiene una vista previa de la factura en PDF a generar
     *
     */
    public function getPDFInvoicePreview(Request $request)
    {
        //Usar seguridad JWT (definir si se pone opcional)
        $ctrl = new InvoiceController();
        return $ctrl->generateInvoicePDF();
    }

    private function builXmlError($code, $message){
        $xmlError = '<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>';
        $xmlError .= '<Error><code>' . $code . '</code><message>' . $message . '</message></Error>';
        return $xmlError;
    }
}