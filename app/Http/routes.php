<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/documentacion', function () {
    return view('apidoc');
});

Route::get('/pruebas', function () {
    return view('apitest');
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () { return view('welcome'); });
    Route::get('/hxkbpe31dfkp', function () { return view('promo.promo'); });
    Route::get('auth/{auth}', 'Auth\AuthController@redirectToProvider');
    Route::get('oauth2/{auth}', 'Auth\AuthController@handleProviderCallback');
    Route::get('au', 'Auth\AuthController@autenticarse'); //---------------------------------------borrar
    Route::post('asignarplan', 'Auth\AuthController@setFreePlan');
    Route::get('logout', 'Auth\AuthController@logout');
    Route::post('comprarplan', 'Auth\AuthController@comprarPlan');
    Route::post('actualizarpago', 'Auth\AuthController@actualizarPago');
    Route::post('teminarplan', 'Auth\AuthController@terminarPlan');
});

//API
Route::get('api/contribuyente', 'ApiController@searchContributor');
Route::get('api/historial/contribuyente', 'ApiController@searchHistoricalContributor');
Route::get('api/validar_ruc', 'ApiController@validateRUC');
Route::get('api/soles', 'ApiController@getCurrencyChange');
Route::get('api/calculadora', 'ApiController@calc');
Route::get('api/plan', 'ApiController@plan');
//Documentos
Route::post('api/preliminar/ubl', 'ApiController@getUBLInvoicePreview');
Route::post('api/preliminar/pdf', 'ApiController@getPDFInvoicePreview');
Route::post('api/doc/enviar', 'ApiController@getInvoicePreview');

//API QA
Route::get('api_qa/contribuyente', 'ApiQAController@searchContributor');
Route::get('api_qa/historial/contribuyente', 'ApiQAController@searchHistoricalContributor');
Route::get('api_qa/validar_ruc', 'ApiQAController@validateRUC');
Route::get('api_qa/soles', 'ApiQAController@getCurrencyChange');
Route::get('api_qa/calculadora', 'ApiQAController@calc');
Route::get('api_qa/plan', 'ApiQAController@plan');
