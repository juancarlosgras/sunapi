<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DeletedToken extends Eloquent{

    protected $connection = 'mongodb';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'deleted_tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['token_id', 'buyer_dni', 'deleted'];

}