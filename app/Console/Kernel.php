<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\Tokens',
        'App\Console\Commands\ChargePayTokens',
        'App\Console\Commands\UpdateCurrencyChange',
        'App\Console\Commands\UpdateContributorDB',
        'App\Console\Commands\UpdateFreeQueries'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('currency_change')->dailyAt('19:00');
        $schedule->command('free_update')->dailyAt('01:00');
        //$schedule->command('tokens')->dailyAt('08:00');
        //$schedule->command('charge')->dailyAt('19:00');
    }
}
