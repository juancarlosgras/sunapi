<?php namespace App\Console\Commands;

use App\User;
use DateTime;
use Illuminate\Console\Command;

class UpdateFreeQueries extends Command {
    
    /**
    * The console command name.
    *
    * @var string
    */
    protected $name = 'free_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para actualizar numero de consultas gratis.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Actualizando planes...");
        $today = new DateTime();
        $todayUsers = User::where('plan.pay_date', '=', $today->format('d/m/Y'))->get(); //Buscar los usuarios que tengan que renovar el plan en la fecha actual
        foreach ($todayUsers as $user) {
            $this->resetPlan($user, $today);
            $this->info("Reset plan to: ".$user->email);
        }
    }

    private function resetPlan($user, $today){
        $user->plan->current_query_count = 0;
        $date = strtotime("+1 month", strtotime($today->format("Y-m-d")));
        $payDate = new DateTime(date("d-m-Y", $date));
        $user->plan->pay_date = $payDate->format('d/m/Y');
        $user->plan->increment('paid_days');
        $user->plan->save();
    }

}