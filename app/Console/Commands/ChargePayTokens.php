<?php namespace App\Console\Commands;

use App\Library\PayManagement;
use App\Library\Util;
use App\User;
use DateTime;
use Illuminate\Console\Command;

class ChargePayTokens extends Command {
    
    /**
    * The console command name.
    *
    * @var string
    */
    protected $name = 'charge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para cobrar tokens de pagos.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Hacer cobros pendientes para activar planes



        //Cobrar y/o renovar los planes
        $today = new DateTime();
        $todayUsers = User::where('plan.pay_date', '=', $today->format('d/m/Y'))->get(); //Buscar los usuarios que tengan que renovar el plan en la fecha actual

        foreach ($todayUsers as $user) {
            if ($user->plan->payed == true) {
                if ($user->pay) {
                    if ($user->pay->verified == true) {
                        //Cobrar token
                        $response = PayManagement::chargePayToken($user->plan->plan_id, $user, $user->pay->buyer_name,
                            $user->pay->buyer_email, $user->pay->buyer_phone, $user->pay->buyer_dni,
                            $user->pay->pay_token, Util::decrypt($user->pay->cc_sec_code, $user->pay->cc_type), $user->pay->cc_type);

                        $token_json = json_decode($response->getContent());
                        if ($token_json->responseType == 'ok') {
                            //Renovar plan pagado con token
                            $this->resetPlan($user, $today);
                            $this->info("Reset plan to: ".$user->email);
                        }
                    } else {
                        //Cobrar directo
                        $response = PayManagement::payPlan($user->plan->plan_id, $user, $user->pay->buyer_name,
                            $user->pay->buyer_email, $user->pay->buyer_phone, $user->pay->buyer_dni,
                            Util::decrypt($user->pay->cc_number, $user->pay->cc_type), $user->pay->cc_exp_date,
                            Util::decrypt($user->pay->cc_sec_code, $user->pay->cc_type), $user->pay->cc_type);

                        $token_json = json_decode($response->getContent());
                        if ($token_json->responseType == 'ok') {
                            //Renovar plan pagado sin token
                            $this->resetPlan($user, $today);
                            $this->info("Reset plan to: ".$user->email);
                        }
                    }
                }
            } else {
                //Renovar plan gratis
                $this->resetPlan($user, $today);
                $this->info("Reset plan to: ".$user->email);
            }
        }
    }

    private function resetPlan($user, $today){
        $user->plan->current_query_count = 0;
        $date = strtotime("+1 month", strtotime($today->format("Y-m-d")));
        $payDate = new DateTime(date("d-m-Y", $date));
        $user->plan->pay_date = $payDate->format('d/m/Y');
        $user->plan->increment('paid_days');
        $user->plan->save();
    }

}