<?php namespace App\Console\Commands;

use App\Currency;
use Carbon\Carbon;
use DateTime;
use DOMDocument;
use Illuminate\Console\Command;

class UpdateCurrencyChange extends Command {
    
    /**
    * The console command name.
    *
    * @var string
    */
    protected $name = 'currency_change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comandos para la actualizacion de la tasa de cambio de soles.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function getCurrencyByCode($code)
    {
        switch ($code) {
            case "02":
                return "USD";
            case "66":
                return "EUR";
            case "11":
                return "CAD";
            case "34":
                return "GBP";
            case "55":
                return "SEK";
            case "57":
                return "CHF";
            case "38":
                return "JPY";
            default:
                return $code;
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Boca-arriba abollao");
        $current_date = new DateTime(); //'07-04-2016'
        $current_date_time = $current_date;
        $current_date = $current_date->format('d/m/Y');
        $html_date = ' ' . $current_date . ' ';
        $this->info("Current date: " . $current_date);

        //USD, //EUR, //CAD //GBP, //CHF, //JPY, //SEK
        $currency_types = array('02', '66', '11', '34', '57', '38', '55');

        foreach($currency_types as $type){
            $purchase_value = null;
            $sale_value = null;
            $data = file_get_contents('http://www.sbs.gob.pe/app/stats/seriesH-tipo_cambio_moneda_excel.asp?fecha1=' . $current_date . '&fecha2=' . $current_date . '&moneda=' . $type);
            $dom = new domDocument;
            libxml_use_internal_errors(true);
            $dom->loadHTML($data);
            libxml_use_internal_errors(false);
            $dom->preserveWhiteSpace = false;
            $tables = $dom->getElementsByTagName('table');
            $rows = $tables->item(0)->getElementsByTagName('tr');

            $get_values = false;
            $read_count = 0;

            foreach ($rows as $row) {
                $cols = $row->getElementsByTagName('td');
                foreach ($cols as $col) {
                    if($col->nodeValue == $html_date){
                        $get_values = true;
                    }

                    if($get_values == true && $col->nodeValue != $html_date && $read_count <= 2){
                        if($read_count == 1){
                            $purchase_value = $col->nodeValue;
                        }
                        else if($read_count == 2){
                            $sale_value = $col->nodeValue;
                            break;
                        }
                        $read_count++;
                    }
                }
            }

            if($purchase_value !== null && $sale_value !== null){
                if($purchase_value == 0){
                    $change = Currency::where('issue_date', '<=', new DateTime())->where('currency', '=', $this->getCurrencyByCode($type))->orderBy('issue_date', 'desc')->take(1)->first();
                    $purchase_value = $change->purchase_value;
                }

                if($sale_value == 0){
                    $change = Currency::where('issue_date', '<=', new DateTime())->where('currency', '=', $this->getCurrencyByCode($type))->orderBy('issue_date', 'desc')->take(1)->first();
                    $sale_value = $change->sale_value;
                }

                Currency::create([
                    'purchase_value' => (float)$purchase_value,
                    'sale_value' => (float)$sale_value,
                    'issue_date' => $current_date_time,
                    'currency' => $this->getCurrencyByCode($type)
                ]);
            }
        }
    }
}