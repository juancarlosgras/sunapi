<?php namespace App\Console\Commands;

use App\DeletedToken;
use App\Library\PayManagement;
use App\Library\Util;
use App\User;
use Illuminate\Console\Command;

class Tokens extends Command {
    
    /**
    * The console command name.
    *
    * @var string
    */
    protected $name = 'tokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para la gestión de tokens de pagos.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //1. Eliminar tokens de bajas
        $this->info("Eliminado tokens de pagos...");
        $tokens = DeletedToken::where('deleted', "=", false)->get();
        foreach ($tokens as $token){
            $this->info("Eliminando token: ".$token->token_id);
            PayManagement::deleteToke($token->token_id, $token->buyer_dni);
            $token->deleted = true;
            $token->save();
        }

        //2. Creando tokens de pagos
        $this->info("Creando tokens de pagos......");
        $untokenUsers = User::where('plan.payed', '=', true)->where('pay.verified', '=', true)->where('pay.pay_token', "=", "0")->get();
        foreach ($untokenUsers as $user){
            $cc_number = Util::decrypt($user->pay->cc_number, $user->pay->cc_type);
            $token = PayManagement::createPayToken($user->pay->buyer_name, $user->pay->buyer_dni, $cc_number, $user->pay->cc_exp_date, $user->pay->cc_type);
            $this->info("Token: ".$token);
            if($token != "-1"){
                //Se guarda el token y se elimina la información del pago del cliente
                $user->pay->cc_number = null;
                $user->pay->cc_exp_date = null;
                $user->pay->pay_token = $token;
                $user->pay->save();
            }
        }
    }
}