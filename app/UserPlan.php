<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserPlan extends Eloquent{

    protected $connection = 'mongodb';
    protected $dateFormat = 'U';
    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'plan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['plan_id', 'plan_name', 'current_query_count', 'plan_query_count', 'pay_date', 'paid_days', 'payed', 'promo'];

}