<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserPayHistory extends Eloquent{

    protected $connection = 'mongodb';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'pay_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['reference_code', 'pay_value', 'response_code', 'response_code2', 'pay_state', 'pending_reason', 'order_id', 'transaction_id'];

}