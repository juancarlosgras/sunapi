<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CloudPlan extends Eloquent{

    protected $connection = 'mongodb';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'cloud_plans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['_id', 'plan_code', 'plan_name', 'plan_cost', 'query_count'];

}