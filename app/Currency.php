<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Currency extends Eloquent{

    protected $connection = 'mongodb';

    protected $dateFormat = 'U';
    public $timestamps = false;
    protected $dates = ['issue_date'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'currency';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['issue_date', 'sale_value', 'purchase_value', 'currency'];

    protected $maps = ['issue_date' => 'fecha_publicacion', 'sale_value' => 'valor_venta', 'purchase_value' => 'valor_compra', 'currency' => 'moneda'];

    protected $hidden = ['_id', 'issue_date', 'sale_value', 'purchase_value', 'currency'];

    protected $appends = ['fecha_publicacion', 'valor_venta', 'valor_compra', 'moneda'];

    public function getFechaPublicacionAttribute()
    {
        return $this->issue_date->format('d-m-Y');
    }

    public function getValorVentaAttribute()
    {
        return $this->sale_value;
    }

    public function getValorCompraAttribute()
    {
        return $this->purchase_value;
    }

    public function getMonedaAttribute()
    {
        switch ($this->currency) {
            case "USD":
                return "Dolar Americano (USD)";
            case "EUR":
                return "Euro (EUR)";
            case "CAD":
                return "Dolar Canadiense (CAD)";
            case "GBP":
                return "Libra Esterlina (GBP)";
            case "SEK":
                return "Corona Sueca (SEK)";
            case "CHF":
                return "Franco Suizo (CHF)";
            case "JPY":
                return "Yen Japones (JPY)";
            case "PEN":
                return "Nuevos Soles (PEN)";
            default:
                return $this->currency;
        }
    }
}