<?php namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserPay extends Eloquent{

    protected $connection = 'mongodb';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'pay';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['buyer_name', 'buyer_email', 'buyer_phone', 'buyer_dni', 'cc_number', 'cc_exp_date',
        'cc_sec_code', 'cc_type', 'pay_token', 'verified'];
}