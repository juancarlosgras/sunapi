<!DOCTYPE html>
<html class="no-js" lang="">
<head>

    <!-- Site Title -->
    <title>SunApi Per&uacute; :: Pruebas</title>

    <!-- Site Meta Info -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="SunApi Per&uacute; es la API de integraci&oacute;n que permitir&aacute; a sus aplicaciones realizar consultas de contribuyentes e informaci&oacute;n financiera del Per&uacute;.">
    <meta name="keywords"
          content="peru, per&uacute;, api, rest, cloud, sunat, sunapi, contribuyente, ruc, soles, monedas, cambio moneda, facturacion electronica">
    <meta name="author" content="sunapiperu.com">


    <!-- Essential CSS Files -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/simplelightbox.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- Color Styles | To Chage the color simply remove all the stylesheet form bellow without the color you want to keep -->
    <link rel="stylesheet" href="css/default-color.css">

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- Hotjar Tracking Code for https://sunapiperu.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:457032,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>
<body>
<section id="inicio"></section>
{{--<a class="btn btn-link" href="/#service" role="button"><span class="fa fa-arrow-circle-left"></span> Regresar</a>--}}

<div class="container">

    <div class="row">
        <div class="col-md-8">
            <div class="page-header">
                <h1>SunApi Per&uacute;
                    <small>pruebas</small>
                </h1>
            </div>

            <p class="bg-primary"><i class="fa fa-warning"></i>
                Para ver la documentaci&oacute;n de la API oficial puede entrar a este <a class="blank-link" TARGET="_blank" href="/documentacion">v&iacute;nculo</a> o
                para ver ejemplos de consumo en varios lenguajes de programaci&oacute;n puede
                visitar nuestro repositorio en <a class="blank-link" TARGET="_blank" href="https://github.com/SunApiPeru/ConsumoAPI">GitHub</a></p>

            <section id="introduccion">
                <div class="page-header">
                    <h2>Introducci&oacute;n</h2>
                </div>

                <p><strong>SunApi Per&uacute;</strong> cuenta con una API alternativa para facilitar las pruebas antes de comenzar en producci&oacute;n.
                    Esta API de pruebas le permitir&aacute; enviar solicitudes y recibir respuestas en todas las funcionalidades mediante un conjunto de casos de pruebas
                    detallados en esta gu&iacute;a.
                </p>

                <p>La comunicaci&oacute;n con la API se realiza mediante solicitudes REST, empleando siempre
                    el m&eacute;todo <code>GET</code> a trav&eacute;s del protocolo seguro <code class="code-api-https-mini">https</code>.
                    Todas las respuestas a las solicitudes se obtienen en formato JSON.
                    Una solicitud debe respetar la siguiente estructura:</p>

                <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api_qa/<i>funcionalidad</i>?<i><b>parametros</b></i></span></pre>

                <p>donde <code><i><b>parametros</b></i></code> identifica a los par&aacute;metros de entrada que espera
                    el
                    servicio.
                    Algunos par&aacute;metros son obligatorios y otros opcionales. Como es norma en las direcciones URL,
                    los par&aacute;metros se separan con el car&aacute;cter <code>&</code>. Uno de los par&aacute;metros
                    obligatorios para todas las solicitudes es <code>apikey</code>, la cual tendr&aacute; el valor de <code>sunapi</code>
                    para todas las solicitudes en la API de pruebas.
                    Todas las solicitudes realizadas deben incluir esta informaci&oacute;n.
                </p>

                <p>A continuaci&oacute;n se describen los casos de pruebas para las funcionalidades de la API:</p>

                {{--<div id="consultaContribuyentes">
                    <h3>Caso de prueba: Consultar contribuyentes</h3>

                    <p>Permite realizar la b&uacute;squeda de uno o m&aacute;s contribuyentes por su nombre o raz&oacute;n social.
                        La siguiente solicitud devuelve el listado de los contribuyentes (activos) cuyo nombre contiene la
                        frase <code>bar restaurant</code>.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api_qa/contribuyente?<i><b>apikey</b>=sunapi&<b>nombre</b>=bar%20restaurant</i></span></pre>

                    <p>El servidor deber&aacute; retornar una estructura <code>JSON</code> como la siguiente:</p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>[</samp></li><li><span class="pln">    </span><samp>{</samp></li><li><span class="pln">      </span><samp>"ruc"</samp><samp>:</samp><span class="code-json-att"> "20000000001"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"estado"</samp><samp>:</samp><span class="code-json-att"> "Activo"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"nombre"</samp><samp>:</samp><span class="code-json-att"> "Bar Restaurant Gente de Zona"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"departamento"</samp><samp>:</samp><span class="code-json-att"> "Lambayeque"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"provincia"</samp><samp>:</samp><span class="code-json-att"> "Chiclayo"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"distrito"</samp><samp>:</samp><span class="code-json-att"> "Jose Leonardo Ortiz"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"calle"</samp><samp>:</samp><span class="code-json-att"> "Los Pasos"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"numero"</samp><samp>:</samp><span class="code-json-att"> "504"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"ubigeo"</samp><samp>:</samp><span class="code-json-att"> "160101"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"mapa"</samp><samp>:</samp><span class="code-json-att"> "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"</span></li><li class="L7"><span class="pln">    </span><samp>},</samp></li><li><span class="pln">    </span><samp>{</samp></li><li><span class="pln">      </span><samp>...</samp></li><li class="L7"><span class="pln">    </span><samp>},</samp></li><li class="L7"><span class="pln">    </span><samp>...</samp></li><li class="L7"><samp>]</samp></li></ol></pre>

                    <p>Puede optener m&aacute;s informaci&oacute;n sobre esta funcionalidad <a class="btn-link" TARGET="_blank" href="/documentacion#consultaContribuyentes">aqu&iacute;</a>.</p>
                </div>--}}

                <!---------------------------------------------------------------------------------------->

                <div id="consultaContribuyenteRUC">
                    <h3>Caso de prueba: Consultar contribuyente por RUC</h3>

                    <p>Permite realizar la b&uacute;squeda de un contribuyente seg&uacute;n su n&uacute;mero de RUC.
                        La siguiente solicitud devuelve el contribuyente cuyo n&uacute;mero de RUC es igual a
                        <code>20000000001</code>.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api_qa/contribuyente?<i><b>apikey</b>=sunapi&<b>ruc</b>=20000000001</i></span></pre>

                    <p>El servidor deber&aacute; retornar una estructura <code>JSON</code> como la siguiente:</p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"ruc"</samp><samp>:</samp><span class="code-json-att"> "20000000001"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"estado"</samp><samp>:</samp><span class="code-json-att"> "Activo"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"nombre"</samp><samp>:</samp><span class="code-json-att"> "Bar Restaurant Gente de Zona"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"departamento"</samp><samp>:</samp><span class="code-json-att"> "Lambayeque"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"provincia"</samp><samp>:</samp><span class="code-json-att"> "Chiclayo"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"distrito"</samp><samp>:</samp><span class="code-json-att"> "Jose Leonardo Ortiz"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"via"</samp><samp>:</samp><span class="code-json-att"> "Av."</span><samp>,</samp></li><li><span class="pln">    </span><samp>"calle"</samp><samp>:</samp><span class="code-json-att"> "Los Pasos"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"numero"</samp><samp>:</samp><span class="code-json-att"> "504"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"ubigeo"</samp><samp>:</samp><span class="code-json-att"> "160101"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"mapa"</samp><samp>:</samp><span class="code-json-att"> "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Puede optener m&aacute;s informaci&oacute;n sobre esta funcionalidad <a class="btn-link" TARGET="_blank" href="/documentacion#consultaContribuyenteRUC">aqu&iacute;</a>.</p>

                </div>

                <!---------------------------------------------------------------------------------------->

                {{--<div id="consultaContribuyentesBajas">
                    <h3>Caso de prueba: Consultar contribuyentes dados de baja</h3>

                    <p>Permite realizar la b&uacute;squeda de contribuyentes que fueron dados de baja definitiva o de oficio.
                        La siguiente solicitud devuelve el listado de todos los contribuyentes dados de baja (definitiva o de oficio)
                        cuyo nombre contiene la frase
                        <code>bar restaurant</code>.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api_qa/historial/contribuyente?<i><b>apikey</b>=sunapi&<b>nombre</b>=bar%20restaurant</i></span></pre>

                    <p>El servidor deber&aacute; retornar una estructura <code>JSON</code> como la siguiente:</p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>[</samp></li><li><span class="pln">    </span><samp>{</samp></li><li><span class="pln">      </span><samp>"ruc"</samp><samp>:</samp><span class="code-json-att"> "20000000001"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"estado"</samp><samp>:</samp><span class="code-json-att"> "Baja definitiva"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"nombre"</samp><samp>:</samp><span class="code-json-att"> "Bar Restaurant Gente de Zona"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"departamento"</samp><samp>:</samp><span class="code-json-att"> "Lambayeque"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"provincia"</samp><samp>:</samp><span class="code-json-att"> "Chiclayo"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"distrito"</samp><samp>:</samp><span class="code-json-att"> "Jose Leonardo Ortiz"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"calle"</samp><samp>:</samp><span class="code-json-att"> "Los Pasos"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"numero"</samp><samp>:</samp><span class="code-json-att"> "504"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"ubigeo"</samp><samp>:</samp><span class="code-json-att"> "160101"</span><samp>,</samp></li><li><span class="pln">      </span><samp>"mapa"</samp><samp>:</samp><span class="code-json-att"> "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"</span></li><li class="L7"><span class="pln">    </span><samp>},</samp></li><li><span class="pln">    </span><samp>{</samp></li><li><span class="pln">      </span><samp>...</samp></li><li class="L7"><span class="pln">    </span><samp>},</samp></li><li class="L7"><span class="pln">    </span><samp>...</samp></li><li class="L7"><samp>]</samp></li></ol></pre>

                    <p>Puede optener m&aacute;s informaci&oacute;n sobre esta funcionalidad <a class="btn-link" TARGET="_blank" href="/documentacion#consultaContribuyentesBajas">aqu&iacute;</a>.</p>

                </div>--}}

                <!---------------------------------------------------------------------------------------->

                <div id="consultaContribuyenteRUCHistorico">
                    <h3>Caso de prueba: Consultar contribuyente dado de baja por RUC</h3>

                    <p>Permite realizar la b&uacute;squeda de un contribuyente dado de baja definitiva o de oficio,
                        seg&uacute;n su n&uacute;mero de RUC. La siguiente solicitud devuelve el contribuyente cuyo n&uacute;mero de RUC es igual a
                        <code>20000000001</code>.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api_qa/historial/contribuyente?<i><b>apikey</b>=sunapi&<b>ruc</b>=20000000001</i></span></pre>

                    <p>El servidor deber&aacute; retornar una estructura <code>JSON</code> como la siguiente:</p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"ruc"</samp><samp>:</samp><span class="code-json-att"> "20000000001"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"estado"</samp><samp>:</samp><span class="code-json-att"> "Baja definitiva"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"nombre"</samp><samp>:</samp><span class="code-json-att"> "Bar Restaurant Gente de Zona"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"departamento"</samp><samp>:</samp><span class="code-json-att"> "Lambayeque"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"provincia"</samp><samp>:</samp><span class="code-json-att"> "Chiclayo"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"distrito"</samp><samp>:</samp><span class="code-json-att"> "Jose Leonardo Ortiz"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"via"</samp><samp>:</samp><span class="code-json-att"> "Av."</span><samp>,</samp></li><li><span class="pln">    </span><samp>"calle"</samp><samp>:</samp><span class="code-json-att"> "Los Pasos"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"numero"</samp><samp>:</samp><span class="code-json-att"> "504"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"ubigeo"</samp><samp>:</samp><span class="code-json-att"> "160101"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"mapa"</samp><samp>:</samp><span class="code-json-att"> "https://www.google.com/maps/place/Lambayeque,Chiclayo,Jose Leonardo Ortiz,Los Pasos,+Per%C3%BA"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Puede optener m&aacute;s informaci&oacute;n sobre esta funcionalidad <a class="btn-link" TARGET="_blank" href="/documentacion#consultaContribuyenteRUCHistorico">aqu&iacute;</a>.</p>
                </div>

                <!---------------------------------------------------------------------------------------->

                <div id="validarRUC">
                    <h3>Caso de prueba: Validar n&uacute;mero de RUC</h3>

                    <p>Permite validar la estrutura de un n&uacute;mero de RUC.
                        La siguiente solicitud devuelve si el n&uacute;mero de RUC: <code>20000000001</code> est&aacute; o no correcto.
                    </p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api_qa/validar_ruc?<i><b>apikey</b>=sunapi&<b>ruc</b>=20000000001</i></span></pre>

                    <p>El servidor deber&aacute; retornar una estructura <code>JSON</code> como la siguiente:</p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"valido"</samp><samp>:</samp><span class="code-json-bool"> true</span><samp>,</samp></li><li><span class="pln">    </span><samp>"mensaje"</samp><samp>:</samp><span class="code-json-att"> "Numero de RUC correcto"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Puede optener m&aacute;s informaci&oacute;n sobre esta funcionalidad <a class="btn-link" TARGET="_blank" href="/documentacion#validarRUC">aqu&iacute;</a>.</p>

                </div>

                <!---------------------------------------------------------------------------------------->

                <div id="tasaCambio">
                    <h3>Caso de prueba: Consultar tasa de cambio del Sol</h3>

                    <p>Permite consultar la tasa de cambio del Sol peruano respecto a una moneda extranjera.
                        La siguiente solicitud devuelve la tasa de cambio del Sol respecto al Euro (<code>eur</code>) en
                        la fecha <code>04-05-2016</code>.
                    </p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api_qa/soles?<i><b>apikey</b>=sunapi&<b>moneda</b>=eur&<b>fecha</b>=04-05-2016</i></span></pre>

                    <p>El servidor deber&aacute; retornar una estructura <code>JSON</code> como la siguiente:</p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"fecha_publicacion"</samp><samp>:</samp><span class="code-json-att"> "03-05-2016"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"valor_venta"</samp><samp>:</samp><span class="code-json-bool"> 3.935</span><samp>,</samp></li><li><span class="pln">    </span><samp>"valor_compra"</samp><samp>:</samp><span class="code-json-bool"> 3.623</span><samp>,</samp></li><li><span class="pln">    </span><samp>"moneda"</samp><samp>:</samp><span class="code-json-att"> "Euro (EUR)"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Puede optener m&aacute;s informaci&oacute;n sobre esta funcionalidad <a class="btn-link" TARGET="_blank" href="/documentacion#tasaCambio">aqu&iacute;</a>.</p>

                </div>

                <!---------------------------------------------------------------------------------------->

                <div id="calculadora">
                    <h3>Caso de prueba: Calculadora monetaria</h3>

                    <p>Permite convertir valores de una moneda a otra moneda seg&uacute;n la tasa de cambio vigente en la fecha se&ntilde;alada.
                        La siguiente solicitud devuelve el c&aacute;lculo de <code>500.55</code> Soles (<code>pen</code>) a
                        Euros (<code>eur</code>) aplicando la tasa de cambio del Sol respecto al Euro en la fecha <code>28-04-2016</code>
                        y redondeado a <code>3</code> cifras decimales.</p>

                    <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api_qa/calculadora?<i><b>apikey</b>=sunapi&<b>valor</b>=500.55&<b>de</b>=pen&<b>a</b>=eur&<b>fecha</b>=28-04-2016&<b>decimal</b>=3</i></span></pre>

                    <p>El servidor deber&aacute; retornar una estructura <code>JSON</code> como la siguiente:</p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"valor_inicial"</samp><samp>:</samp><span class="code-json-bool"> 500.55</span><samp>,</samp></li><li><span class="pln">    </span><samp>"moneda_inicial"</samp><samp>:</samp><span class="code-json-att"> "PEN"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"valor_final"</samp><samp>:</samp><span class="code-json-bool"> 129.912</span><samp>,</samp></li><li><span class="pln">    </span><samp>"moneda_final"</samp><samp>:</samp><span class="code-json-att"> "EUR"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"tasa_venta"</samp><samp>:</samp><span class="code-json-bool"> 3.853</span><samp>,</samp></li><li><span class="pln">    </span><samp>"fecha_tasa"</samp><samp>:</samp><span class="code-json-att"> "27-04-2016"</span><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Puede optener m&aacute;s informaci&oacute;n sobre esta funcionalidad <a class="btn-link" TARGET="_blank" href="/documentacion#calculadora">aqu&iacute;</a>.</p>

                </div>

                <!---------------------------------------------------------------------------------------->

                {{--<div id="consultaEstadoPlan">
                    <div>
                        <h3>Caso de prueba: Consultar estado de mi plan</h3>
                        <p align="justify">
                            Permite consultar el estado del plan adquirido. Esta funcionalidad retornar&aacute;: el nombre del plan aquirido,
                            la cantidad de peticiones realizadas, la cantidad de peticiones disponibles y la fecha de renovaci&oacute;n del plan.
                            La siguiente solicitud devuelve el estado del plan adquirido por un usuario con la apikey: <code>sunapi</code>
                        </p>
                        <pre class="prettyprint linenums"><span class="code-api"><span class="code-api-https">https:</span>//sunapiperu.com/api_qa/plan?<i><b>apikey</b>=sunapi</i></span></pre>
                    </div>

                    <p>El servidor deber&aacute; retornar una estructura <code>JSON</code> como la siguiente:</p>

                    <pre class="prettyprint linenums"><ol class="linenums"><li><samp>{</samp></li><li><span class="pln">    </span><samp>"plan"</samp><samp>:</samp><span class="code-json-att"> "Premium"</span><samp>,</samp></li><li><span class="pln">    </span><samp>"peticiones"</samp><samp>:</samp><span class="code-json-bool"> 3401</span><samp>,</samp></li><li><span class="pln">    </span><samp>"disponibles"</samp><samp>:</samp><span class="code-json-bool"> 1599</span><samp>,</samp></li><li><span class="pln">    </span><samp>"renueva"</samp><samp>:</samp><span class="code-json-att"> "03-05-2016"</span></li><li class="L7"><samp>}</samp></li></ol></pre>

                    <p>Puede optener m&aacute;s informaci&oacute;n sobre esta funcionalidad <a class="btn-link" TARGET="_blank" href="/documentacion#consultaEstadoPlan">aqu&iacute;</a>.</p>

                </div>--}}

                <!---------------------------------------------------------------------------------------->

            </section>

            <br><br>

        </div>

        <div class="col-md-4">
            <nav class="col-xs-3 bs-docs-sidebar">
                <ul id="sidebar" class="nav nav-stacked fixed">
                    <li><a href="#inicio">Inicio</a></li>
                    <li><a href="#introduccion">Introducci&oacute;n</a></li>
                    {{--<li><a href="#consultaContribuyentes">Caso de prueba: Consultar contribuyentes</a></li>--}}
                    <li><a href="#consultaContribuyenteRUC">Caso de prueba: Consultar contribuyente por RUC</a></li>
                    {{--<li><a href="#consultaContribuyentesBajas">Caso de prueba: Consultar contribuyentes dados de baja</a></li>--}}
                    <li><a href="#consultaContribuyenteRUCHistorico">Caso de prueba: Consultar contribuyente dado de baja por RUC</a></li>
                    <li><a href="#validarRUC">Caso de prueba: Validar n&uacute;mero de RUC</a></li>
                    <li><a href="#tasaCambio">Caso de prueba: Consultar tasa de cambio del Sol</a></li>
                    <li><a href="#calculadora">Caso de prueba: Calculadora monetaria</a></li>
                    {{--<li><a href="#consultaEstadoPlan">Caso de prueba: Consultar estado de mi plan</a></li>--}}
                </ul>
            </nav>
        </div>
    </div>

</div>

<script>
    $('body').scrollspy({
        target: '.bs-docs-sidebar',
        offset: 40
    });
</script>

</body>
</html>