<!DOCTYPE html>
<html class="no-js" lang="">
<head>

    <!-- Site Title -->
    <title>SunApi Per&uacute;</title>

    <!-- Site Meta Info -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="SunApi Per&uacute; es la API de integraci&oacute;n que permitir&aacute; a sus aplicaciones realizar consultas de contribuyentes e informaci&oacute;n financiera del Per&uacute;.">
    <meta name="keywords"
          content="peru, per&uacute;, api, rest, cloud, sunat, sunapi, contribuyente, ruc, soles, monedas, cambio moneda, facturacion electronica">
    <meta name="author" content="sunapiperu.com">


    <!-- Essential CSS Files -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/simplelightbox.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap-social.css">

    <!-- Color Styles | To Chage the color simply remove all the stylesheet form bellow without the color you want to keep -->
    <link rel="stylesheet" href="css/default-color.css">

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">

</head>
<body>
<section class="hero-section promo-bg" id="top">
    <div class="slider-caption">
        <div class="container">
            <div class="row">
                <!-- Hero Title -->
                <p style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 60px; color: #fff3f4">SunApi <span>Per&uacute;</span></p>
                <!-- Hero Subtitle -->
                <br>
                {{--<p style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 20px; color: #fff3f4">&iquest;Eres nuevo en SunApi?</p>--}}
                <p style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 20px; color: #fff3f4">Resg&iacutestrate aqu&iacute, es gratis ;-)</p>
                <br>

                @if (Auth::guest())
                    <div class="form-group">
                        <a class="btn btn-social btn-google" href="{{url("auth/google?p=hxkbpe31dfkp")}}">
                            <i class="fa fa-google"></i> Inicia con Google
                        </a>&nbsp;

                        <a class="btn btn-social btn-github" href="{{url("auth/github?p=hxkbpe31dfkp")}}">
                            <i class="fa fa-github"></i> Inicia con GitHub
                        </a>
                    </div>

                    {{--&nbsp;<a class="btn btn-social btn-microsoft" href="{{url("microsoft")}}">
                        <i class="fa fa-windows"></i> Iniciar con Microsoft
                    </a>--}}
                @else
                    <a class="btn btn-social btn-github" href="{{url("logout")}}">
                        <i class="fa fa-sign-out "></i> Cerrar sesi&oacute;n&nbsp;
                    </a>
                @endif

            </div>
        </div>
    </div>
</section>

</body>
</html>