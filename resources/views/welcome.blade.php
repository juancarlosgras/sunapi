<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <!-- Site Title -->
    <title>SunApi Per&uacute;</title>

    <!-- Site Meta Info -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SunApi Per&uacute; es la API de integraci&oacute;n que permitir&aacute; a sus aplicaciones realizar consultas de contribuyentes e informaci&oacute;n financiera del Per&uacute;.">
    <meta name="keywords" content="peru, per&uacute;, api, rest, cloud, sunat, sunapi, contribuyente, ruc, soles, monedas, cambio moneda, facturacion electronica">
    <meta name="author" content="sunapiperu.com">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSS Files -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/simplelightbox.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/default-color.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/bootstrap-social.css">
    <link rel="stylesheet" href="css/bootstrap-toggle.min.css">
    <link rel="stylesheet" href="css/component.css">

    <!-- Google Web Fonts =:= Raleway , Montserrat and Roboto -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:700,400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:700,500,400' rel='stylesheet' type='text/css'>

    <!-- JS Files -->
    <script src="js/vendor/jquery-1.11.3.min.js"></script>
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <!-- IE9 Scripts -->
    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.min.js"></script>
    <script src="js/vendor/respond.min.js"></script>
    <![endif]-->

    <!-- Hotjar Tracking Code for https://sunapiperu.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:457032,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>
<body>

<!-- Heaser Area Start -->
<header class="header-area">
    <!-- Navigation start -->
    <nav class="navbar navbar-custom tb-nav" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#tb-nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="#"><h2>SunApi <span>Per&uacute;</span></h2></a>
            </div>

            <div class="collapse navbar-collapse" id="tb-nav-collapse">
                @if (!Auth::guest())
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <img src="{!! Auth::user()->picture !!}" class="img-circle">
                                {{--<span class="caret"></span>--}}
                                {{--<i class="fa fa-user"></i>--}}
                            </a>
                            <ul class="dropdown-menu">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img src="{!! Auth::user()->picture !!}" class="img-thumb">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <b class="media-heading-name">{!! Auth::user()->name !!}</b>

                                        <p class="media-heading-email">{!! Auth::user()->email !!}</p>
                                    </div>

                                    <div class="userlog">
                                        <div id="summary">
                                        @if (Auth::user()->plan)
                                            <span class="label {!! Auth::user()->getPlanLabelStyle() !!}">{!! Auth::user()->availableQueries() !!} / {!! Auth::user()->plan->plan_query_count !!} consultas</span>
                                        @else
                                            <a type="button" class="btn btn-danger btn-xs page-scroll" href="#pricing">
                                                <i class="fa fa-shopping-cart"></i> Comprar plan
                                            </a>
                                        @endif
                                        </div>

                                        <button type="button" class="btn btn-default btn-xs" data-toggle="modal"
                                                data-target="#configModal">
                                            <i class="fa fa-cog"></i> Configuraci&oacute;n
                                        </button>
                                    </div>

                                    <div class="userlogout">
                                        <a class="btn btn-default btn-sm" href="{{url("logout")}}">
                                            <i class="fa fa-sign-out"></i> Cerrar sesi&oacute;n
                                        </a>
                                    </div>

                                </div>
                            </ul>
                        </li>
                    </ul>
                @endif

                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a class="page-scroll" href="#top">Inicio</a></li>
                    <li><a class="page-scroll" href="#services">Servicios</a></li>
                    <li><a class="page-scroll" href="#api">API</a></li>
                    {{--<li><a class="page-scroll" href="#pricing">Planes</a></li>--}}
                    <li><a class="page-scroll" href="#contact">Cont&aacute;ctanos</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<!-- (Modal) Terminar Plan -->
{{--<div class="modal fade" id="terminatePlan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-body">
            @if (!Auth::guest() and Auth::user()->plan and Auth::user()->plan->plan_name != 'Gratis')
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4>Est&aacute; seguro que desea terminar su plan?</h4>
                <p>Si termina su plan no podr&aacute; acceder a los servicios de la API.
                    De igual manera se dejar&aacute; de hacer el cobro recursivo mensual y se eliminar&aacute; el token de su cobro.
                </p>
                <br>
                <p>
                    <button type="button" class="btn btn-danger btn-terminar-plan" data-dismiss="modal">Terminar plan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </p>
            </div>
            @endif
        </div>
    </div>
</div>--}}

<!-- Modal Configuration -->
@if (!Auth::guest())
    <div class="modal fade" id="configModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-cog"></i> Configuraci&oacute;n</h4>
                </div>
                <div class="modal-body">

                    <div class="modal-config">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#config_user" aria-controls="config_user" role="tab" data-toggle="tab">Usuario</a>
                            </li>
                            {{--<li role="presentation">
                                <a href="#config_plan" aria-controls="config_plan" role="tab" data-toggle="tab">Plan</a>
                            </li>
                            <li role="presentation">
                                <a href="#config_pay" aria-controls="config_pay" role="tab" data-toggle="tab">Informaci&oacute;n pago</a>
                            </li>
                            <li role="presentation">
                                <a href="#config_sunat" aria-controls="config_sunat" role="tab" data-toggle="tab">Informaci&oacute;n SUNAT</a>
                            </li>
                            <li role="presentation">
                                <a href="#config_sign" aria-controls="config_sign" role="tab" data-toggle="tab">Firma electr&oacute;nica</a>
                            </li>
                            <li role="presentation">
                                <a href="#config_invoice" aria-controls="config_invoice" role="tab" data-toggle="tab">Facturaci&oacute;n</a>
                            </li>--}}
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="config_user">
                                <br>
                                <div class="media-body media-config">
                                    <p class="media-heading-name"><b>Usuario: </b>{!! Auth::user()->name !!}</p>
                                    <p class="media-heading-email"><b>Email: </b>{!! Auth::user()->email !!}</p>
                                    <p class="media-heading-key"><b>Api key: </b><code>{!! Auth::user()->api_key !!}</code></p>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="config_plan">
                                <br>
                                <div class="media-body media-config" id="planConfig">
                                    @if (Auth::user()->plan)
                                        <p class="media-heading-name"><b>Tipo plan: </b><span class="media-heading-type">{!! Auth::user()->plan->plan_name !!}</span></p>
                                        <p class="media-heading-email"><b>Consultas disponibles: </b>{!! Auth::user()->availableQueries() !!}/{!! Auth::user()->plan->plan_query_count !!}</p>
                                        <p class="media-heading-key"><b>Fecha renovaci&oacute;n: </b>{!! Auth::user()->plan->pay_date !!}</p>
                                        <a type="button" class="btn btn-success btn-sm page-scroll" data-dismiss="modal" href="#pricing">
                                            <i class="fa fa-refresh"></i> Cambiar plan
                                        </a>
                                        @if (Auth::user()->plan->plan_name != 'Gratis')
                                        <a type="button" class="btn btn-danger btn-sm" data-dismiss="modal" data-toggle="modal" data-target="#terminatePlan">
                                            <i class="fa fa-times"></i> Terminar plan
                                        </a>
                                        @endif
                                    @else
                                        <a type="button" class="btn btn-danger btn-sm page-scroll" data-dismiss="modal"
                                           href="#pricing">
                                            <i class="fa fa-shopping-cart"></i> Comprar plan
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="config_pay">
                                <div class="panel-body">
                                    <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Nombre completo</label>
                                                    <input id="cardUserName_uppay" type="text" class="form-control"
                                                           @if(!Auth::guest())
                                                           value="{!! Auth::user()->name !!}"@endif>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Correo electr&oacute;nico</label>
                                                    <input id="cardEmail_uppay" type="text" class="form-control"
                                                           @if(!Auth::guest())
                                                           value="{!! Auth::user()->email !!}"@endif>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Telef&oacute;no</label>
                                                    <input id="phoneNumber_uppay" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label>N&uacute;mero DNI</label>
                                                    <input id="dniNumber_uppay" type="text" maxlength="10" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xs-5">
                                                <div class="form-group">
                                                    <label>N&uacute;mero de tarjeta <i class="fa fa-lock secure-lock" aria-hidden="true" data-toggle="secure_tooltip" data-placement="top" title="Tus pagos se realizan de forma segura con encriptaci&oacute;n SSL (Secure Sockets Layer)"></i></label>
                                                    <div class="input-group">
                                                        <input type="tel" class="form-control" name="cardNumber_uppay" id="ccnum_uppay" maxlength="16" placeholder="Valid Card Number" />
                                                        <div id="iconCredit_uppay" class="input-group-addon"><i class="fa fa-credit-card"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>C&oacute;digo <i class="fa fa-question-circle secure-icon" aria-hidden="true" data-toggle="code_tooltip" data-placement="top" title="C&oacute;digo de seguridad de 3 d&iacute;gitos ubicado en el reverso de tu tarjeta."></i></label>
                                                    <input class="form-control" id="cvcNumber_uppay" maxlength="3" />
                                                </div>
                                            </div>

                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Fecha de vencimiento</label>
                                                    <div class="form-inline">
                                                        <select class="form-control" name="cardExpiryMonth_uppay">
                                                            <option value="Mes">Mes</option>
                                                            <option>01</option>
                                                            <option>02</option>
                                                            <option>03</option>
                                                            <option>04</option>
                                                            <option>05</option>
                                                            <option>06</option>
                                                            <option>07</option>
                                                            <option>08</option>
                                                            <option>09</option>
                                                            <option>10</option>
                                                            <option>11</option>
                                                            <option>12</option>
                                                        </select>
                                                        <select class="form-control" name="cardExpiryYear_uppay">
                                                            <option>A&ntilde;o</option>
                                                            <option>2017</option>
                                                            <option>2018</option>
                                                            <option>2020</option>
                                                            <option>2021</option>
                                                            <option>2022</option>
                                                            <option>2023</option>
                                                            <option>2024</option>
                                                            <option>2025</option>
                                                            <option>2026</option>
                                                            <option>2027</option>
                                                            <option>2028</option>
                                                            <option>2029</option>
                                                            <option>2030</option>
                                                            <option>2031</option>
                                                            <option>2032</option>
                                                            <option>2033</option>
                                                            <option>2034</option>
                                                            <option>2035</option>
                                                            <option>2036</option>
                                                            <option>2037</option>
                                                            <option>2038</option>
                                                            <option>2039</option>
                                                            <option>2040</option>
                                                            <option>2041</option>
                                                            <option>2042</option>
                                                            <option>2043</option>
                                                            <option>2044</option>
                                                            <option>2045</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <p class="bg-warning warning-font">SunApi Per&uacute; no almacena informaci&oacute;n
                                            de sus tarjetas de cr&eacute;dito; por cada informaci&oacute;n de pago se generar&aacute;
                                            un token o c&oacute;digo &uacute;nico para el cobro recursivo del plan.
                                            Puedes consultar nuestros t&eacute;rminos y condiciones <a class="btn-link" target="_blank" href="docs/CondicionesSunApiPeru.pdf">aqu&iacute;</a>.</p>

                                        <div class="text-left">
                                        @if (!Auth::guest())
                                            <button type="button" class="btn btn-success btn-sm btn-actualizar-datos-pag">
                                                <i class="fa fa-refresh"></i> Actualizar informaci&oacute;n de pago</button>
                                        @endif
                                        </div>
                                    </form>
                                </div>
                            </div>

                            {{--<div role="tabpanel" class="tab-pane" id="config_sunat">
                                <div class="panel-body">
                                    <form role="form" id="sunat-form" method="POST" action="javascript:void(0);">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="sol_user">Usuario Sol</label>
                                                    <input id="sol_user" type="text" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="sol_pass">Clave Sol</label>
                                                    <input id="sol_pass" type="password" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    <p class="bg-warning warning-font">Es necesario registrar su usuario y clave Sol (s&oacute;lo el usuario designado para emitir documentos)
                                        debido a que SUNAT requiere esta informaci&oacute;n para enviar un documento. SunApi Per&uacute; almacena
                                        esta informaci&oacute;n de forma segura y encriptada.
                                        Puedes consultar nuestros t&eacute;rminos y condiciones <a class="btn-link" target="_blank" href="docs/CondicionesSunApiPeru.pdf">aqu&iacute;</a>.</p>

                                    <div class="text-left">
                                    <button type="button" class="btn btn-success btn-sm page-scroll">
                                        Guardar datos
                                    </button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="config_sign">
                                <div class="panel-body">
                                    <form role="form" id="sign-form" method="POST" action="javascript:void(0);">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <label for="sol_user">Seleccione el archivo de firma electr&oacute;nica</label><br>
                                                    <input type="file" name="file-7[]" id="file-7" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
                                                    <label for="file-7"><span></span><strong>&nbsp;&nbsp;<i class="fa fa-folder-open"></i> Buscar&nbsp;&nbsp;</strong></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="sign_pass">Clave de certificado</label>
                                                    <input id="sign_pass" type="password" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <div class="text-left">
                                            <button type="button" class="btn btn-success btn-sm page-scroll">
                                                Guardar datos
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="config_invoice">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="checkbox">
                                                <label><input type="checkbox" checked="true" data-toggle="toggle" data-on="S&iacute;" data-off="No" data-onstyle="success" data-offstyle="danger">
                                                    Generar secuencial para documentos <i class="fa fa-question-circle secure-icon" aria-hidden="true" data-toggle="code_tooltip" data-placement="top" title="Seleccione esta opci&oacute;n si desea que SunApi Per&uacute; genere de forma autom&aacute;tica el secuencial (Ej: F001-00000001) para los documentos enviados. Si selecciona 'No' deber&aacute; enviar el secuencial en el documento enviado."></i></label>
                                            </div>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="checkbox">
                                                <label><input type="checkbox" checked="true" data-toggle="toggle" data-on="S&iacute;" data-off="No" data-onstyle="success" data-offstyle="danger">
                                                    Enviar copia de documento a receptor <i class="fa fa-question-circle secure-icon" aria-hidden="true" data-toggle="code_tooltip" data-placement="top" title="Seleccione esta opci&oacute;n si desea que el receptor de un documento reciba un email con una copia del documento al momento de enviarlo. Debe especificar tambi&eacute;n el email de receptor en el documento enviado."></i></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        --}}
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    @endif

    <!-- hero-section -->
    <section class="hero-section static-bg" id="top">
        <div class="slider-caption">
            <div class="container">
                <div class="row">
                    <!-- Hero Title -->
                    <h1>SunApi <span>Per&uacute;</span></h1>
                    <!-- Hero Subtitle -->
                    <h5>Consultas RUC + Cambio Monedas + Calculadora Soles</h5>
                    <br>

                    @if (Auth::guest())
                        <div class="form-group">
                            <a class="btn btn-social btn-google" href="{{url("auth/google")}}">
                                <i class="fa fa-google"></i> Inicia con Google
                            </a>&nbsp;

                            <a class="btn btn-social btn-github" href="{{url("auth/github")}}">
                                <i class="fa fa-github"></i> Inicia con GitHub
                            </a>
                        </div>

                        {{--&nbsp;<a class="btn btn-social btn-microsoft" href="{{url("microsoft")}}">
                            <i class="fa fa-windows"></i> Iniciar con Microsoft
                        </a>--}}
                    @else
                        <a class="btn btn-social btn-github" href="{{url("logout")}}">
                            <i class="fa fa-sign-out "></i> Cerrar sesi&oacute;n&nbsp;
                        </a>
                    @endif

                </div>
            </div>
        </div>
    </section>
    <!-- End Of Hero Section -->

    <!-- Services Section -->
    <section class="services" id="services">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-xs-12 no-padding">
                    <div class="about-us-left">
                        <h2 class="section-title-left">Servicios</h2>
                        <p> SunApi Per&uacute;&reg; es la API de integraci&oacute;n que permitir&aacute; a sus aplicaciones
                            realizar consultas de contribuyentes e informaci&oacute;n financiera del Per&uacute; totalmente <b>gratis</b>.</p>
                        <p> Brindamos informaci&oacute;n actualizada por las instituciones del pa&iacute;s, en tiempo real, encriptada bajo SSL y
                            soportada por las tecnolog&iacute;as de la informaci&oacute;n m&aacute;s actuales.</p>
                        <section class="suported-sunat-by"></section>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 no-padding">
                    <div class="about-us-right">
                        <div class="media">
                            <div class="media-left dotted">
                                <i class="fa fa-users"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">Datos de contribuyentes</h3>

                                <p>Consulte los datos de contribuyentes: RUC, nombres, direcciones, estados,
                                    establecimientos. </p>
                            </div>
                        </div>
                        <!-- Our Features -->
                        <div class="media">
                            <div class="media-left dotted">
                                <i class="fa fa-usd"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">Valor del Sol</h3>

                                <p>Consulte la tasa de cambio actual o de fechas espec&iacute;ficas del Sol. </p>
                            </div>
                        </div>
                        <!-- Our Features -->
                        <div class="media">
                            <div class="media-left dotted">
                                <i class="fa fa-calculator"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">Calculadora monetaria</h3>

                                <p>Calcule sus finanzas convirtiendo a sus monedas preferidas.<br></p>
                            </div>
                        </div>
                        <!-- Our Features -->
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-check-square-o"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">Verificaci&oacute;n de documentos</h3>

                                <p>Verifique la autenticidad de un n&uacute;mero de RUC. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Of Services Section -->

    <!-- API Section -->
    <section class="our-services-section section-padding" id="api">
        <div class="container">
            <!-- Our service Section Title -->
            <h2 class="section-title text-center">API</h2>

            <!-- (Modal) C�mo funciona -->
            <div class="modal fade" id="howItWorks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                        <div class="modal-body">
                            <div align="center" class="embed-responsive embed-responsive-16by9">
                                <video id="videoHowItWorks" controls class="embed-responsive-item">
                                    <source src="video/comoFunciona.mp4" type="video/mp4">
                                </video>
                            </div>
                    </div>
                </div>
            </div>

            <div class="row our-services">
                <!-- Single Service -->
                <a data-toggle="modal" data-target="#howItWorks" onclick="playPause()">
                    <div class="col-md-6 col-lg-5 col-sm-12 col-lg-offset-1 col-xs-12 single-service">
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-cogs"></i>
                            </div>
                            <div class="media-body">
                                <!-- Single service title -->
                                <h3 class="media-heading">C&oacute;mo funciona</h3>
                                <p>Introducci&oacute;n sobre el funcionamiento de nuestra API.</p>
                            </div>
                        </div>
                    </div>
                </a>

                <!-- Single Service -->
                <a href="documentacion" target="_blank">
                    <div class="col-md-6 col-lg-5 col-sm-12 col-xs-12 single-service">
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-file-text-o"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">Documentaci&oacute;n</h3>
                                <p>Revise la documentaci&oacute;n en detalle de nuestra API.</p>
                            </div>
                        </div>
                    </div>
                </a>

                <!-- Single Service -->
                <a href="pruebas" target="_blank">
                    <div class="col-md-6 col-lg-5 col-sm-12 col-xs-12 col-lg-offset-1 single-service">
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-flask"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">Pruebas</h3>
                                <p>Utilice nuestros casos de pruebas para adaptar su sistema.</p>
                            </div>
                        </div>
                    </div>
                </a>

                <!-- Single Service -->
                <a href="https://github.com/SunApiPeru/ConsumoAPI" target="_blank">
                    <div class="col-md-6 col-lg-5 col-sm-12 col-xs-12 single-service">
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-github"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">Ejemplos</h3>
                                <p class="media-heading">Utilice nuestros ejemplos dise&ntilde;ados en varios lenguajes de
                                    programaci&oacute;n.</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <!-- End Of API -->

    <!-- Pricing And Plans Section -->
{{--<section class="pricing-section section-padding" id="pricing">
        <div class="container">
            <!-- Pricing Section Title -->
            <h2 class="section-title text-center">Planes</h2>

            <div class="row text-center">
                <!-- (Tabla) Plan Gratis -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="single-pricing-table table-active" id="freePlan">
                        <!-- Pricing Table Price -->
                        <div class="price">
                            <h1>$0</h1>
                        </div>
                        <!-- Pricing Table Title -->
                        <div class="pricing-title">
                            @if (!Auth::guest() and Auth::user()->plan and Auth::user()->plan->plan_name == 'Gratis')
                                <h2><i class="fa fa-check"></i> Gratis</h2>
                            @else
                                <h2>Gratis</h2>
                            @endif
                        </div>
                        <!-- Pricing Package Items -->
                        <div class="package-item">
                            <ul>
                                <li>Verificaci&oacute;n de documentos RUC</li>
                                <li>Consulta contribuyentes<br>(5 consultas/mes)</li>
                                <li>Calculadora monetaria<br>(Solo USD)</li>
                                <li>Cambio de monedas<br>(Solo USD)</li>
                                <!-- <li>30 days guarentee</li>-->
                            </ul>
                        </div>
                        <!-- Pricing Buy Button -->
                        <div class="pricing-buttons">
                            @if (Auth::guest())
                                <button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getStarted">Obtener</button>
                                <br>
                            @elseif(Auth::user()->plan and Auth::user()->plan->plan_name == 'Gratis')
                            @else
                                <button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getFree">Obtener</button>
                                <br>
                            @endif
                            <button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getFreeReadMore">Leer m&aacute;s</button>
                        </div>
                    </div>
                </div>
                <!-- (Modal) Leer M�s Sobre Plan Gratis -->
                <div class="modal fade" id="getFreeReadMore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h2 class="modal-title">Plan <span>Gratis</span></h2>
                            </div>
                            <div class="modal-body text-left">

                                <p>Plan dise&ntilde;ado para pruebas o para clientes que requieran funcionalidades de cambio de monedas
                                en Soles y D&oacute;lares. Dentro de las funcionalidades admitidas est&aacute;n las siguientes:</p>

                                <div class="package-item">
                                    <ul>
                                        <li><b>Verificaci&oacute;n de documentos RUC</b>: Permite validar la estructura de un n&uacute;mero de RUC.</li>
                                        <li><b>Consulta contribuyentes</b>: Permite realizar la b&uacute;squeda de contribuyentes por su RUC. Este plan s&oacute;lo admite 5 consultas por mes.</li>
                                        <li><b>Calculadora monetaria</b>: Permite convertir valores de una moneda a otra moneda seg&uacute;n la tasa de cambio vigente en la fecha se&ntilde;alada. Este plan s&oacute;lo admite el c&aacute;lculo para las monedas Soles y D&oacute;lares.</li>
                                        <li><b>Cambio de monedas</b>: Permite consultar la tasa de cambio del Sol peruano respecto a una moneda extranjera. Este plan s&oacute;lo admite la consulta para las monedas Soles y D&oacute;lares.</li>
                                        <!-- <li>30 days guarentee</li>-->
                                    </ul>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- (Modal) Obtener Plan Gratis -->
                <div class="modal fade" id="getFree" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h2 class="modal-title">Obtener Plan <span>Gratis</span></h2>
                            </div>
                            <div class="modal-body text-left">

                                <p>Este plan no requiere informaci&oacute;n de su tarjeta de cr&eacute;dito. Para comenzar a utilizarlo
                                de click en el bot&oacute;n de abajo.</p>

                                @if (!Auth::guest() and Auth::user()->plan)
                                <p class="bg-warning warning-font"><i class="fa fa-warning"></i>
                                    Usted ya cuenta con un plan, si obtiene el plan <b>Gratis</b> ser&aacute;
                                    cancelado autom&aacute;ticamente su plan "{!! Auth::user()->plan->plan_name !!}".</p>
                                @endif

                                <p class="bg-warning warning-font"><i class="fa fa-warning"></i>
                                    Tenga presente que m&aacute;s adelante puede cambiar su plan.</p>

                                <br>

                                <div class="text-center">
                                @if (!Auth::guest())
                                        <a type="button" class="btn btn-success page-scroll btn-asignar-plan-gratis" data-dismiss="modal">
                                            <i class="fa fa-heart"></i> Obtener plan gratis</a>
                                @else
                                        <button class="btn btn-success page-scroll" type="button" data-dismiss="modal" data-toggle="modal" data-target="#getStarted">
                                            <i class="fa fa-gratipay"></i> Obtener plan gratis</button>
                                @endif
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal Debe Iniciar Sessi�n -->
                <div class="modal fade" id="getStarted" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-body">
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4>Debe iniciar sesi&oacute;n</h4>
                                <p>Para adquirir un plan debes iniciar sesi&oacute;n con tu usuario de Google o GitHub.</p>
                                <br>
                                <p>
                                    <a type="button" class="btn btn-warning page-scroll" data-dismiss="modal" href="#top">Aceptar</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal Felicidades Plan Gratis -->
                <div class="modal fade" id="getFreePlan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-body">
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4>Felicidades, has adquirido el plan GRATIS!</h4>
                                <p><i class="fa fa-warning"></i> Recuerda que en cualquier momento puedes cambiar tu plan por otro mejor.</p>
                                <br>
                                <p>
                                    <a type="button" class="btn btn-warning page-scroll" data-dismiss="modal">Aceptar</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- (Tabla) Plan B�sico -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="single-pricing-table table-active" id="basicPlan">
                        <!-- Pricing Table Price -->
                        <div class="price">
                           <h1>$10/m</h1>
                        </div>
                        <!-- Pricing Table Title -->
                        <div class="pricing-title">
                            @if (!Auth::guest() and Auth::user()->plan and Auth::user()->plan->plan_name == 'Basico')
                                <h2><i class="fa fa-check"></i> B&aacute;sico</h2>
                            @else
                                <h2>B&aacute;sico</h2>
                            @endif
                        </div>
                        <!-- Pricing Package Items -->
                        <div class="package-item">
                            <ul>
                                <li>Verificaci&oacute;n de documentos RUC</li>
                                <li>Consulta contribuyentes<br>(1000 consultas/mes)</li>
                                <li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li>
                                <li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li>
                            </ul>
                        </div>
                        <!-- Pricing Buy Button -->
                        <div class="pricing-buttons">
                            @if (Auth::guest())
                                <button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getStarted">Comprar</button>
                                <br>
                            @elseif(Auth::user()->plan and Auth::user()->plan->plan_name == 'Basico')
                            @else
                                <button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getBasic">Comprar</button>
                                <br>
                            @endif
                            <button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getBasicReadMore">Leer m&aacute;s</button>
                        </div>
                    </div>
                </div>
                <!-- (Modal) Leer M�s Sobre Plan B�sico -->
                <div class="modal fade" id="getBasicReadMore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h2 class="modal-title">Plan <span>B&aacute;sico</span></h2>
                            </div>
                            <div class="modal-body text-left">

                                <p>Plan dise&ntilde;ado para peque&ntilde;os negocios/empresas que requieran funcionalidades para el
                                    c&aacute;lculo o cambio de monedas, as&iacute; c&oacute;mo la consulta de contribuyentes.
                                    Dentro de las funcionalidades admitidas est&aacute;n las siguientes:</p>

                                <div class="package-item">
                                    <ul>
                                        <li><b>Verificaci&oacute;n de documentos RUC</b>: Permite validar la estructura de un n&uacute;mero de RUC.</li>
                                        <li><b>Consulta contribuyentes</b>: Permite realizar la b&uacute;squeda de contribuyentes por su RUC. Este plan admite hasta 1000 consultas por mes.</li>
                                        <li><b>Calculadora monetaria</b>: Permite convertir valores de una moneda a otra moneda seg&uacute;n la tasa de cambio vigente en la fecha se&ntilde;alada. Este plan admite el c&aacute;lculo para cualquiera de las <a class="btn-link" href="/documentacion#monedas" target="_blank">monedas disponibles</a>.</li>
                                        <li><b>Cambio de monedas</b>: Permite consultar la tasa de cambio del Sol peruano respecto a una moneda extranjera.  Este plan admite la consulta para cualquiera de las <a class="btn-link" href="/documentacion#monedas" target="_blank">monedas disponibles</a>.</li>
                                    </ul>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- (Modal) Comprar Plan B�sico -->
                <div class="modal fade bs-example-modal-lg" id="getBasic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h2 class="modal-title">Comprar Plan <span>B&aacute;sico</span></h2>
                            </div>
                            <div class="modal-body text-left row">
                                <div class="panel-body col-md-5">
                                    @if (!Auth::guest() and Auth::user()->plan)
                                        <p class="bg-warning warning-font"><i class="fa fa-warning"></i>
                                            Usted ya cuenta con un plan, si obtiene el plan <b>B&aacute;sico</b> ser&aacute;
                                            cancelado autom&aacute;ticamente su plan "{!! Auth::user()->plan->plan_name !!}".</p>
                                    @endif
                                    <div class="col-xs-12 form-img">
                                        <a href="http://www.payulatam.com/logos/pol.php?l=137&c=5765709e6f599" target="_blank"><img src="img/PayU_Logo.png" border="0" /></a>
                                    </div>
                                    <div class="single-pricing-table table-active">
                                        <div class="pricing-title price-title">
                                            <h2>$10/m</h2>
                                        </div>
                                        <!-- Pricing Package Items -->
                                        <div class="package-item">
                                            <ul>
                                                <li><i class="fa fa-check"></i> Verificaci&oacute;n de documentos RUC</li>
                                                <li><i class="fa fa-check"></i> Consulta contribuyentes<br>(1000 consultas/mes)</li>
                                                <li><i class="fa fa-check"></i> Calculadora monetaria</li>
                                                <li><i class="fa fa-check"></i> Cambio de monedas</li>
                                            </ul>
                                        </div>
                                        <br>
                                    </div>
                                </div>

                                <div class="panel-body col-md-7">
                                    <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="username">Nombre completo</label>
                                                    <input id="cardUserName" type="text" class="form-control"
                                                           @if(!Auth::guest())
                                                           value="{!! Auth::user()->name !!}"@endif>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="username">Correo electr&oacute;nico</label>
                                                    <input id="cardEmail" type="text" class="form-control"
                                                           @if(!Auth::guest())
                                                           value="{!! Auth::user()->email !!}"@endif>
                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="username">N&uacute;mero telef&oacute;nico</label>
                                                    <input id="phoneNumber" type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="username">N&uacute;mero DNI</label>
                                                    <input id="dniNumber" type="text" maxlength="10" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="cardNumber">N&uacute;mero de tarjeta <i class="fa fa-lock secure-lock" aria-hidden="true" data-toggle="secure_tooltip" data-placement="top" title="Tus pagos se realizan de forma segura con encriptaci&oacute;n SSL (Secure Sockets Layer)"></i></label>
                                                <div class="input-group">
                                                    <input type="tel" class="form-control" name="cardNumber" id="ccnum" maxlength="16" placeholder="Valid Card Number" />
                                                    <div id="iconCredit" class="input-group-addon"><i class="fa fa-credit-card"></i></div>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="cvcNumber">C&oacute;digo <i class="fa fa-question-circle secure-icon" aria-hidden="true" data-toggle="code_tooltip" data-placement="top" title="C&oacute;digo de seguridad de 3 d&iacute;gitos ubicado en el reverso de tu tarjeta."></i></label>
                                                    <input class="form-control" id="cvcNumber" maxlength="3" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="cardExpiryMonth">Fecha de vencimiento</label>
                                                    <div class="form-inline">
                                                        <select class="form-control" name="cardExpiryMonth">
                                                            <option value="Mes">Mes</option>
                                                            <option>01</option>
                                                            <option>02</option>
                                                            <option>03</option>
                                                            <option>04</option>
                                                            <option>05</option>
                                                            <option>06</option>
                                                            <option>07</option>
                                                            <option>08</option>
                                                            <option>09</option>
                                                            <option>10</option>
                                                            <option>11</option>
                                                            <option>12</option>
                                                        </select>
                                                        <select class="form-control" name="cardExpiryYear">
                                                            <option>A&ntilde;o</option>
                                                            <option>2017</option>
                                                            <option>2018</option>
                                                            <option>2020</option>
                                                            <option>2021</option>
                                                            <option>2022</option>
                                                            <option>2023</option>
                                                            <option>2024</option>
                                                            <option>2025</option>
                                                            <option>2026</option>
                                                            <option>2027</option>
                                                            <option>2028</option>
                                                            <option>2029</option>
                                                            <option>2030</option>
                                                            <option>2031</option>
                                                            <option>2032</option>
                                                            <option>2033</option>
                                                            <option>2034</option>
                                                            <option>2035</option>
                                                            <option>2036</option>
                                                            <option>2037</option>
                                                            <option>2038</option>
                                                            <option>2039</option>
                                                            <option>2040</option>
                                                            <option>2041</option>
                                                            <option>2042</option>
                                                            <option>2043</option>
                                                            <option>2044</option>
                                                            <option>2045</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="bg-warning warning-font">Por este medio autorizas a SunApi Per&uacute; para que autom&aacute;ticamente te haga un cargo cada mes, hasta que canceles tu plan.
                                            Puedes consultar nuestros t&eacute;rminos y condiciones <a class="btn-link" target="_blank" href="docs/CondicionesSunApiPeru.pdf">aqu&iacute;</a>.</p>
                                    </form>
                                </div>

                            </div>
                            <div class="modal-footer">
                                    @if (!Auth::guest())
                                    <button type="button" class="btn btn-success btn-comprar-plan-basico" data-loading-text="<i class='fa fa-shopping-cart'></i> Enviando datos...">
                                            <i class="fa fa-shopping-cart"></i> Comprar plan</button>
                                    @else
                                        <button class="btn btn-success page-scroll" type="button" data-dismiss="modal" data-toggle="modal" data-target="#getStarted">
                                            <i class="fa fa-shopping-cart"></i> Comprar plan</button>
                                    @endif
                                <button type="button" class="btn btn-default btn-xs btn-cerrar-plan-basico" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- (Tabla) Plan Premium -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="single-pricing-table table-active" id="premiumPlan">
                        <!-- Pricing Table Price -->
                        <div class="price">
                            <h1>$20/m</h1>
                        </div>
                        <!-- Pricing Table Title -->
                        <div class="pricing-title">
                            @if (!Auth::guest() and Auth::user()->plan and Auth::user()->plan->plan_name == 'Premium')
                                <h2><i class="fa fa-check"></i> Premium</h2>
                            @else
                                <h2>Premium</h2>
                            @endif
                        </div>
                        <!-- Pricing Package Items -->
                        <div class="package-item">
                            <ul>
                                <li>Verificaci&oacute;n de documentos RUC</li>
                                <li>Consulta contribuyentes<br>(5000 consultas/mes)</li>
                                <li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li>
                                <li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li>
                            </ul>
                        </div>
                        <!-- Pricing Buy Button -->
                        <div class="pricing-buttons">
                            @if (Auth::guest())
                                <button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getStarted">Comprar</button>
                                <br>
                            @elseif(Auth::user()->plan and Auth::user()->plan->plan_name == 'Premium')
                            @else
                                <button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getPremium">Comprar</button>
                                <br>
                            @endif
                            <button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getPremiumReadMore">Leer m&aacute;s</button>
                        </div>
                    </div>
                </div>
                <!-- (Modal) Leer M�s Sobre Plan Premium -->
                <div class="modal fade" id="getPremiumReadMore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h2 class="modal-title">Plan <span>Premium</span></h2>
                            </div>
                            <div class="modal-body text-left">

                                <p>Plan dise&ntilde;ado para medianos y grandes negocios/empresas que requieran funcionalidades para el
                                    c&aacute;lculo o cambio de monedas, as&iacute; c&oacute;mo la consulta de contribuyentes.
                                    Dentro de las funcionalidades admitidas est&aacute;n las siguientes:</p>

                                <div class="package-item">
                                    <ul>
                                        <li><b>Verificaci&oacute;n de documentos RUC</b>: Permite validar la estructura de un n&uacute;mero de RUC.</li>
                                        <li><b>Consulta contribuyentes</b>: Permite realizar la b&uacute;squeda de contribuyentes por su RUC. Este plan admite hasta 5000 consultas por mes.<sup>***</sup></li>
                                        <li><b>Calculadora monetaria</b>: Permite convertir valores de una moneda a otra moneda seg&uacute;n la tasa de cambio vigente en la fecha se&ntilde;alada. Este plan admite el c&aacute;lculo para cualquiera de las <a class="btn-link" href="/documentacion#monedas" target="_blank">monedas disponibles</a>.</li>
                                        <li><b>Cambio de monedas</b>: Permite consultar la tasa de cambio del Sol peruano respecto a una moneda extranjera.  Este plan admite la consulta para cualquiera de las <a class="btn-link" href="/documentacion#monedas" target="_blank">monedas disponibles</a>.</li>
                                    </ul>
                                </div>

                                <p class="bg-warning warning-font"><sup>***</sup> Si requiere un mayor n&uacute;mero de consultas en este plan, puede contactarnos desde la sesi&oacute;n "Cont&aacute;ctanos" (formulario de abajo).</p>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- (Modal) Comprar Plan Premium -->
                <div class="modal fade bs-example-modal-lg" id="getPremium" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h2 class="modal-title">Comprar Plan <span>Premium</span></h2>
                            </div>
                            <div class="modal-body text-left row">
                                <div class="panel-body col-md-5">
                                    @if (!Auth::guest() and Auth::user()->plan)
                                        <p class="bg-warning warning-font"><i class="fa fa-warning"></i>
                                            Usted ya cuenta con un plan, si obtiene el plan <b>Premium</b> ser&aacute;
                                            cancelado autom&aacute;ticamente su plan "{!! Auth::user()->plan->plan_name !!}".</p>
                                    @endif
                                    <div class="col-xs-12 form-img">
                                        <a href="http://www.payulatam.com/logos/pol.php?l=137&c=5765709e6f599" target="_blank"><img src="img/PayU_Logo.png" border="0" /></a>
                                    </div>
                                    <div class="single-pricing-table table-active">
                                        <div class="pricing-title price-title">
                                            <h2>$20/m</h2>
                                        </div>
                                        <!-- Pricing Package Items -->
                                        <div class="package-item">
                                            <ul>
                                                <li><i class="fa fa-check"></i> Verificaci&oacute;n de documentos RUC</li>
                                                <li><i class="fa fa-check"></i> Consulta contribuyentes<br>(5000 consultas/mes)</li>
                                                <li><i class="fa fa-check"></i> Calculadora monetaria</li>
                                                <li><i class="fa fa-check"></i> Cambio de monedas</li>
                                            </ul>
                                        </div>
                                        <br>
                                    </div>
                                </div>

                                <div class="panel-body col-md-7">
                                    <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="username">Nombre completo</label>
                                                    <input id="cardUserNameP" type="text" class="form-control"
                                                           @if(!Auth::guest())
                                                           value="{!! Auth::user()->name !!}"@endif>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="username">Correo electr&oacute;nico</label>
                                                    <input id="cardEmailP" type="text" class="form-control"
                                                           @if(!Auth::guest())
                                                           value="{!! Auth::user()->email !!}"@endif>
                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="username">N&uacute;mero telef&oacute;nico</label>
                                                    <input id="phoneNumberP" type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="username">N&uacute;mero DNI</label>
                                                    <input id="dniNumberP" type="text" maxlength="10" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="cardNumber">N&uacute;mero de tarjeta <i class="fa fa-lock secure-lock" aria-hidden="true" data-toggle="secure_tooltip" data-placement="top" title="Tus pagos se realizan de forma segura con encriptaci&oacute;n SSL (Secure Sockets Layer)"></i></label>
                                                    <div class="input-group">
                                                        <input type="tel" class="form-control" name="cardNumberP" id="ccnumP" maxlength="16" placeholder="Valid Card Number" />
                                                        <div id="iconCreditP" class="input-group-addon"><i class="fa fa-credit-card"></i></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="cvcNumber">C&oacute;digo <i class="fa fa-question-circle secure-icon" aria-hidden="true" data-toggle="code_tooltip" data-placement="top" title="C&oacute;digo de seguridad de 3 d&iacute;gitos ubicado en el reverso de tu tarjeta."></i></label>
                                                    <input class="form-control" id="cvcNumberP" maxlength="3" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="cardExpiryMonth">Fecha de vencimiento</label>
                                                    <div class="form-inline">
                                                        <select class="form-control" name="cardExpiryMonthP">
                                                            <option value="Mes">Mes</option>
                                                            <option>01</option>
                                                            <option>02</option>
                                                            <option>03</option>
                                                            <option>04</option>
                                                            <option>05</option>
                                                            <option>06</option>
                                                            <option>07</option>
                                                            <option>08</option>
                                                            <option>09</option>
                                                            <option>10</option>
                                                            <option>11</option>
                                                            <option>12</option>
                                                        </select>
                                                        <select class="form-control" name="cardExpiryYearP">
                                                            <option>A&ntilde;o</option>
                                                            <option>2017</option>
                                                            <option>2018</option>
                                                            <option>2020</option>
                                                            <option>2021</option>
                                                            <option>2022</option>
                                                            <option>2023</option>
                                                            <option>2024</option>
                                                            <option>2025</option>
                                                            <option>2026</option>
                                                            <option>2027</option>
                                                            <option>2028</option>
                                                            <option>2029</option>
                                                            <option>2030</option>
                                                            <option>2031</option>
                                                            <option>2032</option>
                                                            <option>2033</option>
                                                            <option>2034</option>
                                                            <option>2035</option>
                                                            <option>2036</option>
                                                            <option>2037</option>
                                                            <option>2038</option>
                                                            <option>2039</option>
                                                            <option>2040</option>
                                                            <option>2041</option>
                                                            <option>2042</option>
                                                            <option>2043</option>
                                                            <option>2044</option>
                                                            <option>2045</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="bg-warning warning-font">Por este medio autorizas a SunApi Per&uacute; para que autom&aacute;ticamente te haga un cargo cada mes, hasta que canceles tu plan.
                                            Puedes consultar nuestros t&eacute;rminos y condiciones <a class="btn-link" target="_blank" href="docs/CondicionesSunApiPeru.pdf">aqu&iacute;</a>.</p>
                                    </form>
                                </div>

                            </div>
                            <div class="modal-footer">
                                @if (!Auth::guest())
                                    <button type="button" class="btn btn-success btn-comprar-plan-premium" data-loading-text="<i class='fa fa-shopping-cart'></i> Enviando datos...">
                                        <i class="fa fa-shopping-cart"></i> Comprar plan</button>
                                @else
                                    <button class="btn btn-success page-scroll" type="button" data-dismiss="modal" data-toggle="modal" data-target="#getStarted">
                                        <i class="fa fa-shopping-cart"></i> Comprar plan</button>
                                @endif
                                <button type="button" class="btn btn-default btn-xs btn-cerrar-plan-premium" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}

    <!-- End Of Pricing And Plans Section -->

    <!-- Contact Us Section -->
    <section class="contact-us-section section-padding" id="contact">
        <div class="container">
            <!-- Contact Us Section Title -->
            <h2 class="section-title contact-title text-center">Cont&aacute;ctanos</h2>

            <p class="sub-title contact-subtitle text-center">&iquest;Tiene alguna duda o sugerencia? &iquest;Requiere
                un n&uacute;mero
                mayor de consultas?<br> &iquest;Necesita otros datos?</p>

            <p class="sub-title contact-subtitle text-center">Escr&iacute;banos a:</p>
            <p class="sub-title contact-subtitle text-center contact-subtitle-font">soporte@sunapiperu.com</p>

           {{-- <div class="row">
                <div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                    <div class="contact-form-section mt-50">
                        <form method="POST" action="" id="contactForm" name="contact-form" role="form">
                            <div class="row">
                                <div class="col-sm-5 col-md-4">
                                    <!-- Name Field -->
                                    <div class="form-group contact-form-icon">
                                        <label class="sr-only">Name</label>
                                        <i class="fa fa-user"></i>
                                        <input type="text" placeholder="Nombre" id="name" class="form-control"
                                               name="name"
                                               required>
                                    </div>
                                    <!-- Email Field -->
                                    <div class="form-group contact-form-icon">
                                        <label for="email" class="sr-only">Email</label>
                                        <i class="fa fa-envelope"></i>
                                        <input type="email" placeholder="Correo" id="email" class="form-control"
                                               name="email" required>
                                    </div>
                                    <!-- Subject Field -->
                                    <div class="form-group contact-form-icon">
                                        <label for="subject" class="sr-only">Subject</label>
                                        <i class="fa fa-pencil"></i>
                                        <input type="text" placeholder="Asunto" id="subject" class="form-control"
                                               name="subject" required>
                                    </div>
                                </div>
                                <div class="col-sm-7 col-md-8">
                                    <div class="form-group contact-form-icon">
                                        <label for="message" class="sr-only">Message</label>
                                        <i class="fa fa-keyboard-o"></i>
                                    <textarea placeholder="Mensaje" rows="7" id="message" class="form-control"
                                              name="message" required></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-5 col-md-4 text-center contact-button-padding">
                                    <button class="btn-primary btn-contact btn-block" name="submit" type="submit"><i
                                                class="fa fa-paper-plane"></i> Send
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>--}}
        </div>
    </section>

    <!-- End Of Contact Us Section -->

    <!-- Footer Section -->
    <footer class="footer-section">
        <div class="container">
            <div class="row mt-30 mb-30">
                <div class="col-md-12 text-center">
                    <!-- Footer Copy Right Text -->
                    <div class="copyright-info">
                        <span>SunApi Per&uacute;&reg; 2017</span>
                        <h5>soportado por <a href="http://aws.amazon.com/"><i class="fa fa-amazon"></i> AWS</a></h5>
                    </div>

                    <!-- Footer Social Icons -->
                    <div class="social-icons mt-30">
                        <a href="https://www.facebook.com/SunapiPeru" target="_blank"><i class="fa fa-facebook-official"></i></a>
                        <a href="https://twitter.com/SunApiPeru" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UC0CygoFjKWXdIqbpu5AU1IA" target="_blank"><i class="fa fa-youtube"></i></a>
                        <a href="https://github.com/SunApiPeru" target="_blank"><i class="fa fa-github"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Of Footer Section -->

    <!-- JS Files -->
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-toggle.min.js"></script>
    <script src="js/custom-file-input.js"></script>
    <!-- jQuery Easing -->
    <script src="js/jquery.easing.min.js"></script>
    <!-- PreLoader -->
    <script src="js/queryloader2.min.js"></script>
    <!-- WOW JS Animation -->
    <script src="js/wow.min.js"></script>
    <!-- Simple Lightbox -->
    <script src="js/simple-lightbox.min.js"></script>
    <!-- Sticky -->
    <script src="js/jquery.sticky.js"></script>
    <!-- OWL-Carousel -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- jQuery inview -->
    <script src="js/jquery.inview.js"></script>
    <!-- Shuffle jQuery -->
    <script src="js/jquery.shuffle.min.js"></script>
    <!-- Main JS -->
    <script src="js/main.js"></script>
    <script src="js/toastr/toastr.min.js"></script>
    <link href="js/toastr/toastr.css" rel="stylesheet"/>

    <script>

        //Configuraci�n inicial

        $(document).ready(function(){
            $('[data-toggle="secure_tooltip"]').tooltip();
            $('[data-toggle="code_tooltip"]').tooltip();
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //Configuraci�n Videos

        var myVideo = document.getElementById("videoHowItWorks");

        function playPause() {
            if (myVideo.paused)
                myVideo.play();
            else
                myVideo.currentTime = 0;
        }

        //Validaci�n de pagos

        $('#ccnum').keypress(function(e) {
            var a = [];
            var k = e.which;

            for (i = 48; i < 58; i++)
                a.push(i);

            if (!(a.indexOf(k)>=0))
                e.preventDefault();
        });

        $('#ccnumP').keypress(function(e) {
            var a = [];
            var k = e.which;

            for (i = 48; i < 58; i++)
                a.push(i);

            if (!(a.indexOf(k)>=0))
                e.preventDefault();
        });

        $('#ccnum_uppay').keypress(function(e) {
            var a = [];
            var k = e.which;

            for (i = 48; i < 58; i++)
                a.push(i);

            if (!(a.indexOf(k)>=0))
                e.preventDefault();
        });

        $('#cvcNumber').keypress(function(e) {
            var a = [];
            var k = e.which;

            for (i = 48; i < 58; i++)
                a.push(i);

            if (!(a.indexOf(k)>=0))
                e.preventDefault();
        });

        $('#cvcNumberP').keypress(function(e) {
            var a = [];
            var k = e.which;

            for (i = 48; i < 58; i++)
                a.push(i);

            if (!(a.indexOf(k)>=0))
                e.preventDefault();
        });

        $("#ccnum").keyup(function(e){
            var num = $(this).val().toString();
            validateCreditCard(num, num.length);
        });

        $("#ccnumP").keyup(function(e){
            var num = $(this).val().toString();
            validateCreditCard2(num, num.length);
        });

        $("#ccnum_uppay").keyup(function(e){
            var num = $(this).val().toString();
            validateCreditCard3(num, num.length);
        });

        function validateCreditCard(num, charCount){
            /* VALIDACION DE TIPO */
            if(charCount == 0) {
                $('#iconCredit').empty();
                var cardIcon = 'fa fa-credit-card';
                $('#iconCredit').append('<div id="iconCredit"><i class="'+cardIcon+'"></i></div>');
            }

            if(charCount == 1) {
                if(num == "4"){
                    $('#iconCredit').empty();
                    cardIcon = 'fa fa-cc-visa';
                    $('#iconCredit').append('<div id="iconCredit"><i class="'+cardIcon+'"></i></div>');
                } else {
                    $('#iconCredit').empty();
                    cardIcon = 'fa fa-credit-card';
                    $('#iconCredit').append('<div id="iconCredit"><i class="'+cardIcon+'"></i></div>');
                }
            }
            if(charCount == 2){
                if( num >= "51" && num <= "55"){
                    $('#iconCredit').empty();
                    cardIcon = 'fa fa-cc-mastercard';
                    $('#iconCredit').append('<div id="iconCredit"><i class="'+cardIcon+'"></i></div>');
                }
            }

            /* !VALIDACION DE TIPO */

            /* ALGORITMO http://ricardogeek.com/como-validar-tarjetas-de-credito/ */
            if(charCount == 13 || charCount == 16){
                if (num.substring (0, 2) >= "51" && num.substring (0, 2) <= "55")
                    cardIcon = 'fa fa-cc-mastercard';
                else if(num.substring (0, 1) == "4")
                    cardIcon = 'fa fa-cc-visa';

                var valid = isValid(num, charCount);
                if(valid){
                    $('#iconCredit').empty();
                    $('#iconCredit').append('<div id="iconCredit">'+'<i class="'+cardIcon+' secure-icon"></i>&nbsp;'+'<i class="fa fa-check secure-icon"></i></div>');
                } else {
                    $('#iconCredit').empty();
                    $('#iconCredit').append('<div id="iconCredit">'+'<i class="'+cardIcon+' error-input"></i>&nbsp;'+'<i class="fa fa-times error-input"></i></div>');
                }
            }
        }

        function validateCreditCard2(num, charCount){
            /* VALIDACION DE TIPO */
            if(charCount == 0) {
                $('#iconCreditP').empty();
                var cardIcon = 'fa fa-credit-card';
                $('#iconCreditP').append('<div id="iconCreditP"><i class="'+cardIcon+'"></i></div>');
            }

            if(charCount == 1) {
                if(num == "4"){
                    $('#iconCreditP').empty();
                    cardIcon = 'fa fa-cc-visa';
                    $('#iconCreditP').append('<div id="iconCreditP"><i class="'+cardIcon+'"></i></div>');
                } else {
                    $('#iconCreditP').empty();
                    cardIcon = 'fa fa-credit-card';
                    $('#iconCreditP').append('<div id="iconCreditP"><i class="'+cardIcon+'"></i></div>');
                }
            }
            if(charCount == 2){
                if( num >= "51" && num <= "55"){
                    $('#iconCreditP').empty();
                    cardIcon = 'fa fa-cc-mastercard';
                    $('#iconCreditP').append('<div id="iconCreditP"><i class="'+cardIcon+'"></i></div>');
                }
            }

            /* !VALIDACION DE TIPO */

            /* ALGORITMO http://ricardogeek.com/como-validar-tarjetas-de-credito/ */
            if(charCount == 13 || charCount == 16){
                if (num.substring (0, 2) >= "51" && num.substring (0, 2) <= "55")
                    cardIcon = 'fa fa-cc-mastercard';
                else if(num.substring (0, 1) == "4")
                    cardIcon = 'fa fa-cc-visa';

                var valid = isValid(num, charCount);
                if(valid){
                    $('#iconCreditP').empty();
                    $('#iconCreditP').append('<div id="iconCreditP">'+'<i class="'+cardIcon+' secure-icon"></i>&nbsp;'+'<i class="fa fa-check secure-icon"></i></div>');
                } else {
                    $('#iconCreditP').empty();
                    $('#iconCreditP').append('<div id="iconCreditP">'+'<i class="'+cardIcon+' error-input"></i>&nbsp;'+'<i class="fa fa-times error-input"></i></div>');
                }
            }
        }

        function validateCreditCard3(num, charCount){
            /* VALIDACION DE TIPO */
            if(charCount == 0) {
                $('#iconCredit_uppay').empty();
                var cardIcon = 'fa fa-credit-card';
                $('#iconCredit_uppay').append('<div id="iconCredit_uppay"><i class="'+cardIcon+'"></i></div>');
            }

            if(charCount == 1) {
                if(num == "4"){
                    $('#iconCredit_uppay').empty();
                    cardIcon = 'fa fa-cc-visa';
                    $('#iconCredit_uppay').append('<div id="iconCredit_uppay"><i class="'+cardIcon+'"></i></div>');
                } else {
                    $('#iconCredit_uppay').empty();
                    cardIcon = 'fa fa-credit-card';
                    $('#iconCredit_uppay').append('<div id="iconCredit_uppay"><i class="'+cardIcon+'"></i></div>');
                }
            }
            if(charCount == 2){
                if( num >= "51" && num <= "55"){
                    $('#iconCredit_uppay').empty();
                    cardIcon = 'fa fa-cc-mastercard';
                    $('#iconCredit_uppay').append('<div id="iconCredit_uppay"><i class="'+cardIcon+'"></i></div>');
                }
            }

            /* !VALIDACION DE TIPO */

            /* ALGORITMO http://ricardogeek.com/como-validar-tarjetas-de-credito/ */
            if(charCount == 13 || charCount == 16){
                if (num.substring (0, 2) >= "51" && num.substring (0, 2) <= "55")
                    cardIcon = 'fa fa-cc-mastercard';
                else if(num.substring (0, 1) == "4")
                    cardIcon = 'fa fa-cc-visa';

                var valid = isValid(num, charCount);
                if(valid){
                    $('#iconCredit_uppay').empty();
                    $('#iconCredit_uppay').append('<div id="iconCredit_uppay">'+'<i class="'+cardIcon+' secure-icon"></i>&nbsp;'+'<i class="fa fa-check secure-icon"></i></div>');
                } else {
                    $('#iconCredit_uppay').empty();
                    $('#iconCredit_uppay').append('<div id="iconCredit_uppay">'+'<i class="'+cardIcon+' error-input"></i>&nbsp;'+'<i class="fa fa-times error-input"></i></div>');
                }
            }
        }

        function isValid(ccNum, charCount){
            if(ccNum == "")
                return false;

            if ((ccNum.substring (0, 1) == "4") || (ccNum.substring (0, 2) >= "51" && ccNum.substring (0, 2) <= "55")) {
                var double = true;
                var numArr = [];
                var sumTotal = 0;
                for (i = 0; i < charCount; i++) {
                    var digit = parseInt(ccNum.charAt(i));

                    if (double) {
                        digit = digit * 2;
                        digit = toSingle(digit);
                        double = false;
                    } else {
                        double = true;
                    }
                    numArr.push(digit);
                }

                for (i = 0; i < numArr.length; i++) {
                    sumTotal += numArr[i];
                }
                var diff = eval(sumTotal % 10);
                return (diff == "0");
            } else
                return false;
        }

        function toSingle(digit){
            if(digit > 9){
                var tmp = digit.toString();
                var d1 = parseInt(tmp.charAt(0));
                var d2 = parseInt(tmp.charAt(1));
                return (d1 + d2);
            } else {
                return digit;
            }
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function getCardType(ccNum){
            if (ccNum.substring (0, 1) == "4")
                return "VISA";
            else if (ccNum.substring (0, 2) >= "51" && ccNum.substring (0, 2) <= "55")
                return "MASTERCARD";
            return "";
        }

        //Gestion de Planes

        $(".btn-asignar-plan-gratis").click(function() {
            $.ajax("/asignarplan",
                    {
                        data: {
                            "codigo": "FRE01"
                        },
                        type: "POST",
                        async: true,
                        beforeSend: function (data) {
                        },
                        error: function (data) {
                        },
                        success: function (data) {
                            //Actualiza la informaci�n del usuario en el men�
                            $('#summary').empty();
                            var div_user = '<span class="label ' + data["planLabelStyle"] + '">' + data["availableQueries"] + ' / ' + data["planQueryCount"] + ' consultas</span>';
                            $('#summary').append(div_user);

                            //Actualiza la informaci�n de la configuraci�n
                            $('#planConfig').empty();
                            var div_plan_config = '<p class="media-heading-name"><b>Tipo plan: </b><span class="media-heading-type">' + data["planName"] + '</span></p><p class="media-heading-email"><b>Consultas disponibles: </b>' + data["availableQueries"] + '/' + data["planQueryCount"] + '</p><p class="media-heading-key"><b>Fecha renovaci&oacute;n: </b>' + data["payDate"] + '</p>';
                            div_plan_config += '<a type="button" class="btn btn-success btn-sm page-scroll" data-dismiss="modal" href="#pricing"><i class="fa fa-refresh"></i> Cambiar plan</a>';
                            $('#planConfig').append(div_plan_config);

                            //Actualiza la informaci�n del Plan
                            $('#freePlan').empty();
                            var div_plan = '<div class="price"><h1>$0</h1></div><div class="pricing-title"><h2><i class="fa fa-check"></i> Gratis</h2></div>';
                            div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(5 consultas/mes)</li><li>Calculadora monetaria<br>(Solo USD)</li><li>Cambio de monedas<br>(Solo USD)</li></ul></div>';
                            div_plan += '<div class="pricing-buttons"><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getFreeReadMore">Leer m&aacute;s</button></div></div>';
                            $('#freePlan').append(div_plan);

                            //Desmarca los otros planes
                            $('#basicPlan').empty();
                            var div_plan = '<div class="price"><h1>$10/m</h1></div><div class="pricing-title"><h2>B&aacute;sico</h2></div>';
                            div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(1000 consultas/mes)</li><li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li><li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li></ul></div>';
                            div_plan += '<div class="pricing-buttons"><button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getBasic">Comprar</button><br><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getBasicReadMore">Leer m&aacute;s</button></div>';
                            $('#basicPlan').append(div_plan);


                            $('#premiumPlan').empty();
                            var div_plan = '<div class="price"><h1>$20/m</h1></div><div class="pricing-title"><h2>Premium</h2></div>';
                            div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(5000 consultas/mes)</li><li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li><li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li></ul></div>';
                            div_plan += '<div class="pricing-buttons"><button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getPremium">Comprar</button><br><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getPremiumReadMore">Leer m&aacute;s</button></div>';
                            $('#premiumPlan').append(div_plan);

                            //Muestra el dialogo de confirmaci�n
                            $('#getFreePlan').modal('show');
                        }
                    });
        });

        $(".btn-comprar-plan-basico").click(function() {
            var valid = true;

            if ($("#dniNumber").val().length < 6) {
                valid = false;
                $("#dniNumber").css("border", "1px solid #D01F1F");
            } else {
                $("#dniNumber").css("border", "1px solid #bfbfbf");
            }

            if ($("#phoneNumber").val().length < 6) {
                valid = false;
                $("#phoneNumber").css("border", "1px solid #D01F1F");
            } else {
                $("#phoneNumber").css("border", "1px solid #bfbfbf");
            }

            if ($("#cardUserName").val().length == 0) {
                valid = false;
                $("#cardUserName").css("border", "1px solid #D01F1F");
            } else {
                $("#cardUserName").css("border", "1px solid #bfbfbf");
            }

            var validEmail = validateEmail($("#cardEmail").val());
            if (!validEmail) {
                valid = false;
                $("#cardEmail").css("border", "1px solid #D01F1F");
            } else {
                $("#cardEmail").css("border", "1px solid #bfbfbf");
            }

            var num = $("#ccnum").val().toString();
            if (!isValid(num, num.length)) {
                valid = false;
                $("#ccnum").css("border", "1px solid #D01F1F");
            } else {
                $("#ccnum").css("border", "1px solid #bfbfbf");
            }

            var cvc = $("#cvcNumber").val().toString();
            if (cvc.length < 3) {
                valid = false;
                $("#cvcNumber").css("border", "1px solid #D01F1F");
            } else {
                $("#cvcNumber").css("border", "1px solid #bfbfbf");
            }

            if ($('select[name="cardExpiryMonth"] option:selected').val() == "Mes") {
                valid = false;
                $('select[name="cardExpiryMonth"]').css("border", "1px solid #D01F1F");
            } else {
                $('select[name="cardExpiryMonth"]').css("border", "1px solid #bfbfbf");
            }

            if (isNaN($('select[name="cardExpiryYear"] option:selected').val())) {
                valid = false;
                $('select[name="cardExpiryYear"]').css("border", "1px solid #D01F1F");
            } else {
                $('select[name="cardExpiryYear"]').css("border", "1px solid #bfbfbf");
            }

            var $btn = $(this).button('loading');
            if (valid) {
                $.ajax("/comprarplan",
                        {
                            data: {
                                "plan": "BAS01",
                                "nombre": $("#cardUserName").val(),
                                "correo": $("#cardEmail").val(),
                                "telefono": $("#phoneNumber").val(),
                                "dni": $("#dniNumber").val(),
                                "numero": $('#ccnum').val(),
                                "cvc": $("#cvcNumber").val(),
                                "fecha": $('select[name="cardExpiryYear"] option:selected').val() + "/" + $('select[name="cardExpiryMonth"] option:selected').val(),
                                "tipo": getCardType($('#ccnum').val())
                            },
                            type: "POST",
                            async: true,
                            beforeSend: function (data) {},
                            error: function (data) {
                                toastr.error('Intente m&aacute;s adelante.', 'Error durante la transacci&oacute;n');
                                $btn.button('reset');
                            },
                            success: function (data) {
                                if (data["responseType"] == "ok") {
                                    toastr.success('', 'Pago satisfactorio');
                                    //Actualiza la informaci�n del usuario en el men�
                                    $('#summary').empty();
                                    var div_user = '<span class="label ' + data["planLabelStyle"] + '">' + data["availableQueries"] + ' / ' + data["planQueryCount"] + ' consultas</span>';
                                    $('#summary').append(div_user);

                                    //Actualiza la informaci�n de la configuraci�n
                                    $('#planConfig').empty();
                                    var div_plan_config = '<p class="media-heading-name"><b>Tipo plan: </b><span class="media-heading-type">' + data["planName"] + '</span></p><p class="media-heading-email"><b>Consultas disponibles: </b>' + data["availableQueries"] + '/' + data["planQueryCount"] + '</p><p class="media-heading-key"><b>Fecha renovaci&oacute;n: </b>' + data["payDate"] + '</p>';
                                    div_plan_config += '<a type="button" class="btn btn-success btn-sm page-scroll" data-dismiss="modal" href="#pricing"><i class="fa fa-refresh"></i> Cambiar plan</a>  <a type="button" class="btn btn-danger btn-sm page-scroll" data-dismiss="modal" data-toggle="modal" data-target="#terminatePlan"><i class="fa fa-times"></i> Terminar plan</a>';
                                    $('#planConfig').append(div_plan_config);

                                    //Actualiza la informaci�n del Plan
                                    $('#basicPlan').empty();
                                    var div_plan = '<div class="price"><h1>$10/m</h1></div><div class="pricing-title"><h2><i class="fa fa-check"></i> B&aacute;sico</h2></div>';
                                    div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(1000 consultas/mes)</li><li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li><li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li></ul></div>';
                                    div_plan += '<div class="pricing-buttons"><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getBasicReadMore">Leer m&aacute;s</button></div></div>';
                                    $('#basicPlan').append(div_plan);

                                    //Desmarca los otros planes
                                    $('#premiumPlan').empty();
                                    var div_plan = '<div class="price"><h1>$20/m</h1></div><div class="pricing-title"><h2>Premium</h2></div>';
                                    div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(5000 consultas/mes)</li><li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li><li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li></ul></div>';
                                    div_plan += '<div class="pricing-buttons"><button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getPremium">Comprar</button><br><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getPremiumReadMore">Leer m&aacute;s</button></div>';
                                    $('#premiumPlan').append(div_plan);

                                    $('#freePlan').empty();
                                    var div_plan = '<div class="price"><h1>$0</h1></div><div class="pricing-title"><h2>Gratis</h2></div>';
                                    div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(5 consultas/mes)</li><li>Calculadora monetaria<br>(Solo USD)</li><li>Cambio de monedas<br>(Solo USD)</li></ul></div>';
                                    div_plan += '<div class="pricing-buttons"><button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getFree">Obtener</button><br><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getFreeReadMore">Leer m&aacute;s</button></div>';
                                    $('#freePlan').append(div_plan);

                                    $('#getBasic').modal('hide');
                                    cleanPayData();
                                }
                                else if (data["responseType"] == "warn") {
                                    toastr.warning('Una vez verificado se notificar&aacute; al correo: ' + data["userEmail"], 'Pago pendiente de aprobaci&oacute;n');
                                    $('#getBasic').modal('hide');
                                    cleanPayData();
                                }
                                else if (data["responseType"] == "error") {
                                    toastr.error('Su tarjeta de cr&eacute;dito fue rechazada.', 'Error en el pago');
                                }
                                $btn.button('reset');
                            }
                        });
            }
            else {
                $btn.button('reset');
            }
        });

        $(".btn-comprar-plan-premium").click(function(){
            var valid = true;

            if ($("#dniNumberP").val().length < 6) {
                valid = false;
                $("#dniNumberP").css("border", "1px solid #D01F1F");
            } else {
                $("#dniNumberP").css("border", "1px solid #bfbfbf");
            }

            if ($("#phoneNumberP").val().length < 6) {
                valid = false;
                $("#phoneNumberP").css("border", "1px solid #D01F1F");
            } else {
                $("#phoneNumberP").css("border", "1px solid #bfbfbf");
            }

            if ($("#cardUserNameP").val().length == 0) {
                valid = false;
                $("#cardUserNameP").css("border", "1px solid #D01F1F");
            } else {
                $("#cardUserNameP").css("border", "1px solid #bfbfbf");
            }

            var validEmail = validateEmail($("#cardEmailP").val());
            if (!validEmail) {
                valid = false;
                $("#cardEmailP").css("border", "1px solid #D01F1F");
            } else {
                $("#cardEmailP").css("border", "1px solid #bfbfbf");
            }

            var num = $("#ccnumP").val().toString();
            if (!isValid(num, num.length)) {
                valid = false;
                $("#ccnumP").css("border", "1px solid #D01F1F");
            } else {
                $("#ccnumP").css("border", "1px solid #bfbfbf");
            }

            var cvc = $("#cvcNumberP").val().toString();
            if (cvc.length < 3) {
                valid = false;
                $("#cvcNumberP").css("border", "1px solid #D01F1F");
            } else {
                $("#cvcNumberP").css("border", "1px solid #bfbfbf");
            }

            if ($('select[name="cardExpiryMonthP"] option:selected').val() == "Mes") {
                valid = false;
                $('select[name="cardExpiryMonthP"]').css("border", "1px solid #D01F1F");
            } else {
                $('select[name="cardExpiryMonthP"]').css("border", "1px solid #bfbfbf");
            }

            if (isNaN($('select[name="cardExpiryYearP"] option:selected').val())) {
                valid = false;
                $('select[name="cardExpiryYearP"]').css("border", "1px solid #D01F1F");
            } else {
                $('select[name="cardExpiryYearP"]').css("border", "1px solid #bfbfbf");
            }

            var $btn = $(this).button('loading');
            if (valid) {
                $.ajax("/comprarplan",
                        {
                            data: {
                                "plan": "PRE01",
                                "nombre": $("#cardUserNameP").val(),
                                "correo": $("#cardEmailP").val(),
                                "telefono": $("#phoneNumberP").val(),
                                "dni": $("#dniNumberP").val(),
                                "numero": $('#ccnumP').val(),
                                "cvc": $("#cvcNumberP").val(),
                                "fecha": $('select[name="cardExpiryYearP"] option:selected').val() + "/" + $('select[name="cardExpiryMonthP"] option:selected').val(),
                                "tipo": getCardType($('#ccnumP').val())
                            },
                            type: "POST",
                            async: true,
                            beforeSend: function (data) {},
                            error: function (data) {
                                toastr.error('Intente m&aacute;s adelante.', 'Error durante la transacci&oacute;n');
                                $btn.button('reset');
                            },
                            success: function (data) {
                                if (data["responseType"] == "ok") {
                                    toastr.success('', 'Pago satisfactorio');
                                    //Actualiza la informaci�n del usuario en el men�
                                    $('#summary').empty();
                                    var div_user = '<span class="label ' + data["planLabelStyle"] + '">' + data["availableQueries"] + ' / ' + data["planQueryCount"] + ' consultas</span>';
                                    $('#summary').append(div_user);

                                    //Actualiza la informaci�n de la configuraci�n
                                    $('#planConfig').empty();
                                    var div_plan_config = '<p class="media-heading-name"><b>Tipo plan: </b><span class="media-heading-type">' + data["planName"] + '</span></p><p class="media-heading-email"><b>Consultas disponibles: </b>' + data["availableQueries"] + '/' + data["planQueryCount"] + '</p><p class="media-heading-key"><b>Fecha renovaci&oacute;n: </b>' + data["payDate"] + '</p>';
                                    div_plan_config += '<a type="button" class="btn btn-success btn-sm page-scroll" data-dismiss="modal" href="#pricing"><i class="fa fa-refresh"></i> Cambiar plan</a>  <a type="button" class="btn btn-danger btn-sm page-scroll" data-dismiss="modal" data-toggle="modal" data-target="#terminatePlan"><i class="fa fa-times"></i> Terminar plan</a>';
                                    $('#planConfig').append(div_plan_config);

                                    //Actualiza la informaci�n del Plan
                                    $('#premiumPlan').empty();
                                    var div_plan = '<div class="price"><h1>$20/m</h1></div><div class="pricing-title"><h2><i class="fa fa-check"></i> Premium</h2></div>';
                                    div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(5000 consultas/mes)</li><li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li><li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li></ul></div>';
                                    div_plan += '<div class="pricing-buttons"><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getPremiumReadMore">Leer m&aacute;s</button></div></div>';
                                    $('#premiumPlan').append(div_plan);

                                    //Desmarca los otros planes
                                    $('#freePlan').empty();
                                    var div_plan = '<div class="price"><h1>$0</h1></div><div class="pricing-title"><h2>Gratis</h2></div>';
                                    div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(5 consultas/mes)</li><li>Calculadora monetaria<br>(Solo USD)</li><li>Cambio de monedas<br>(Solo USD)</li></ul></div>';
                                    div_plan += '<div class="pricing-buttons"><button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getFree">Obtener</button><br><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getFreeReadMore">Leer m&aacute;s</button></div>';
                                    $('#freePlan').append(div_plan);

                                    $('#basicPlan').empty();
                                    var div_plan = '<div class="price"><h1>$10/m</h1></div><div class="pricing-title"><h2>B&aacute;sico</h2></div>';
                                    div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(1000 consultas/mes)</li><li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li><li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li></ul></div>';
                                    div_plan += '<div class="pricing-buttons"><button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getBasic">Comprar</button><br><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getBasicReadMore">Leer m&aacute;s</button></div>';
                                    $('#basicPlan').append(div_plan);

                                    $('#getPremium').modal('hide');
                                    cleanPayData2();
                                }
                                else if (data["responseType"] == "warn") {
                                    toastr.warning('Una vez verificado se notificar&aacute; al correo: ' + data["userEmail"], 'Pago pendiente de aprobaci&oacute;n');
                                    $('#getBasic').modal('hide');
                                    cleanPayData2();
                                }
                                else if (data["responseType"] == "error") {
                                    toastr.error('Su tarjeta de cr&eacute;dito fue rechazada.', 'Error en el pago');
                                }
                                $btn.button('reset');
                            }
                        });
            }
            else {
                $btn.button('reset');
            }
        });

        $(".btn-actualizar-datos-pag").click(function() {
            var valid = true;

            if ($("#dniNumber_uppay").val().length < 6) {
                valid = false;
                $("#dniNumber_uppay").css("border", "1px solid #D01F1F");
            } else {
                $("#dniNumber_uppay").css("border", "1px solid #bfbfbf");
            }

            if ($("#phoneNumber_uppay").val().length < 6) {
                valid = false;
                $("#phoneNumber_uppay").css("border", "1px solid #D01F1F");
            } else {
                $("#phoneNumber_uppay").css("border", "1px solid #bfbfbf");
            }

            if ($("#cardUserName_uppay").val().length == 0) {
                valid = false;
                $("#cardUserName_uppay").css("border", "1px solid #D01F1F");
            } else {
                $("#cardUserName_uppay").css("border", "1px solid #bfbfbf");
            }

            var validEmail = validateEmail($("#cardEmail_uppay").val());
            if (!validEmail) {
                valid = false;
                $("#cardEmail_uppay").css("border", "1px solid #D01F1F");
            } else {
                $("#cardEmail_uppay").css("border", "1px solid #bfbfbf");
            }

            var num = $("#ccnum_uppay").val().toString();
            if (!isValid(num, num.length)) {
                valid = false;
                $("#ccnum_uppay").css("border", "1px solid #D01F1F");
            } else {
                $("#ccnum_uppay").css("border", "1px solid #bfbfbf");
            }

            var cvc = $("#cvcNumber_uppay").val().toString();
            if (cvc.length < 3) {
                valid = false;
                $("#cvcNumber_uppay").css("border", "1px solid #D01F1F");
            } else {
                $("#cvcNumber_uppay").css("border", "1px solid #bfbfbf");
            }

            if ($('select[name="cardExpiryMonth_uppay"] option:selected').val() == "Mes") {
                valid = false;
                $('select[name="cardExpiryMonth_uppay"]').css("border", "1px solid #D01F1F");
            } else {
                $('select[name="cardExpiryMonth_uppay"]').css("border", "1px solid #bfbfbf");
            }

            if (isNaN($('select[name="cardExpiryYear_uppay"] option:selected').val())) {
                valid = false;
                $('select[name="cardExpiryYear_uppay"]').css("border", "1px solid #D01F1F");
            } else {
                $('select[name="cardExpiryYear_uppay"]').css("border", "1px solid #bfbfbf");
            }

            var $btn = $(this).button('loading');
            if (valid) {
                $.ajax("/actualizarpago",
                        {
                            data: {
                                "nombre": $("#cardUserName_uppay").val(),
                                "correo": $("#cardEmail_uppay").val(),
                                "telefono": $("#phoneNumber_uppay").val(),
                                "dni": $("#dniNumber_uppay").val(),
                                "numero": $('#ccnum_uppay').val(),
                                "cvc": $("#cvcNumber_uppay").val(),
                                "fecha": $('select[name="cardExpiryYear_uppay"] option:selected').val() + "/" + $('select[name="cardExpiryMonth_uppay"] option:selected').val(),
                                "tipo": getCardType($('#ccnum_uppay').val())
                            },
                            type: "POST",
                            async: true,
                            beforeSend: function (data) {},
                            error: function (data) {
                                toastr.error('Intente m&aacute;s adelante.', 'Error durante la transacci&oacute;n');
                                $btn.button('reset');
                            },
                            success: function (data) {
                                toastr.success('', 'Actualizaci&oacute;n de datos de pago satisfactoria');
                                cleanPayData3();
                                $btn.button('reset');
                            }
                        });
            }
            else {
                $btn.button('reset');
            }
        });

        $(".btn-terminar-plan").click(function() {
            $.ajax("/teminarplan",
                    {
                        data: {},
                        type: "POST",
                        async: true,
                        beforeSend: function (data) {},
                        error: function (data) {
                            toastr.error('Intente m&aacute;s adelante.', 'Error durante la transacci&oacute;n');
                        },
                        success: function (data) {
                            if (data["removed"] == true) {
                                toastr.success('', 'Su plan fue finalizado');

                                //Actualiza la informaci�n del usuario en el men�
                                $('#summary').empty();
                                var div_user = '<a type="button" class="btn btn-danger btn-xs page-scroll" href="#pricing"><i class="fa fa-shopping-cart"></i> Comprar plan</a>';
                                $('#summary').append(div_user);

                                //Actualiza la informaci�n de la configuraci�n
                                $('#planConfig').empty();
                                var div_plan_config = '<a type="button" class="btn btn-danger btn-sm page-scroll" data-dismiss="modal" href="#pricing"><i class="fa fa-shopping-cart"></i> Comprar plan</a>';
                                $('#planConfig').append(div_plan_config);

                                //Desmarca los planes
                                $('#freePlan').empty();
                                var div_plan = '<div class="price"><h1>$0</h1></div><div class="pricing-title"><h2>Gratis</h2></div>';
                                div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(5 consultas/mes)</li><li>Calculadora monetaria<br>(Solo USD)</li><li>Cambio de monedas<br>(Solo USD)</li></ul></div>';
                                div_plan += '<div class="pricing-buttons"><button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getFree">Obtener</button><br><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getFreeReadMore">Leer m&aacute;s</button></div>';
                                $('#freePlan').append(div_plan);

                                $('#basicPlan').empty();
                                var div_plan = '<div class="price"><h1>$10/m</h1></div><div class="pricing-title"><h2>B&aacute;sico</h2></div>';
                                div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(1000 consultas/mes)</li><li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li><li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li></ul></div>';
                                div_plan += '<div class="pricing-buttons"><button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getBasic">Comprar</button><br><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getBasicReadMore">Leer m&aacute;s</button></div>';
                                $('#basicPlan').append(div_plan);


                                $('#premiumPlan').empty();
                                var div_plan = '<div class="price"><h1>$20/m</h1></div><div class="pricing-title"><h2>Premium</h2></div>';
                                div_plan += '<div class="package-item"><ul><li>Verificaci&oacute;n de documentos RUC</li><li>Consulta contribuyentes<br>(5000 consultas/mes)</li><li>Calculadora monetaria<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li><li>Cambio de monedas<br>(todas las monedas <a class="btn-link" href="/documentacion#monedas" target="_blank">disponibles</a>)</li></ul></div>';
                                div_plan += '<div class="pricing-buttons"><button class="btn-primary buy-btn" type="button" data-toggle="modal" data-target="#getPremium">Comprar</button><br><button type="button" class="btn btn-link spacing" data-toggle="modal" data-target="#getPremiumReadMore">Leer m&aacute;s</button></div>';
                                $('#premiumPlan').append(div_plan);
                            }
                        }
                    })
        });

        function cleanPayData(){
            $('input[id="cardUserName"]').val('');
            $('input[id="cardEmail"]').val('');
            $('input[id="phoneNumber"]').val('');
            $('input[id="dniNumber"]').val('');
            $('input[name="cardNumber"]').val('');
            $('input[id="cvcNumber"]').val('');

            //$("select.cardExpiryMonth select").val("Mes");
            //$('select[id="cardExpiryMonth"]').val('Mes');
            //$('select[id="cardExpiryYear"]').val('2020');
            validateCreditCard('', 0);
        }

        function cleanPayData2(){
            $('input[id="cardUserNameP"]').val('');
            $('input[id="cardEmailP"]').val('');
            $('input[id="phoneNumberP"]').val('');
            $('input[id="dniNumberP"]').val('');
            $('input[name="cardNumberP"]').val('');
            $('input[id="cvcNumberP"]').val('');

            //$("select.cardExpiryMonthP select").val("Mes");
            //$('select[id="cardExpiryMonthP"]').val('Mes');
            //$('select[id="cardExpiryYearP"]').val('2020');
            validateCreditCard2('', 0);
        }

        function cleanPayData3(){
            $('input[id="cardUserName_uppay"]').val('');
            $('input[id="cardEmail_uppay"]').val('');
            $('input[id="phoneNumber_uppay"]').val('');
            $('input[id="dniNumber_uppay"]').val('');
            $('input[name="cardNumber_uppay"]').val('');
            $('input[id="cvcNumber_uppay"]').val('');

            //$("select.cardExpiryMonth_uppay select").val("Mes");
            //$('select[id="cardExpiryMonth_uppay"]').val('Mes');
            //$('select[id="cardExpiryYear_uppay"]').val('2020');
            validateCreditCard3('', 0);
        }

    </script>
</body>
</html>