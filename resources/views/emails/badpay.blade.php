@extends('emails.base')
@section('content')
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Hola {{$name}},</p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">No hemos podido procesar tu pago correctamente,
        por tanto no hemos podido activar tu plan Premium a&uacute;n.
        Por favor verifica tus datos para el pago y vuelve a intentarlo nuevamente.</p>
@stop