@extends('emails.base')
@section('content')
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">&iexcl;Felicidades {{$name}}!</p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Tu pago ha sido verificado y has optenido el plan {{$plan}}.
        Ya puedes comenzar a usar SunApi. Te recordamos que este plan se cobrar&aacute; mensualmente hasta que lo canceles, para m&aacute;s
        informaci&oacute;n te recomendamos que leeas detenidamente nuestros
        <a href="https://sunapiperu.com/docs/CondicionesSunApiPeru.pdf" target="black">t&eacute;rminos y condiciones</a>.</p></p>
@stop