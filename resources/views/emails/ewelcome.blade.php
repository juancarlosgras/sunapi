@extends('emails.basewelcome')
@section('content')
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Hola {{$name}},</p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Bienvenido a SunApi Per&uacute;, la plataforma donde podr&aacute;s integrar tus aplicaciones con
        informaci&oacute;n financiera y de contribuyentes del Per&uacute; totalmente gratis.</p>

    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        �Ya puedes empezar a usar SunApi!</p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Accede con tu clave Api: <span style="font-family: 'Consolas'; color: #d45500; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">{{$apikey}}</span>
    </p>

    {{--<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Para comenzar a utilizar nuestra API debes obtener uno de nuestros planes y utilizar tu
    Clave API: <span style="font-family: 'Consolas'; color: #d45500; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">{{$apikey}}</span>
    </p>

    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Si no has obtenido un plan a&uacute;n, puedes hacerlo desde este bot&oacute;n.</p>

    <!-- button -->
    <table class="btn-primary" cellpadding="0" cellspacing="0" border="0"
           style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; width: auto !important; Margin: 0 0 10px; padding: 0;">
        <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
            <td style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; line-height: 1.6em; border-radius: 25px; text-align: center; vertical-align: top; background: #ff5959; margin: 0; padding: 0;"
                align="center" bgcolor="#348eda" valign="top">
                <a href="https://sunapiperu.com/#pricing" target="_blank"
                   style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 2; color: #ffffff; border-radius: 5px; display: inline-block; cursor: pointer; font-weight: bold; text-decoration: none; background: #ff5959; margin: 0; padding: 0; border-color: #ff5959; border-style: solid; border-width: 10px 20px;">Obtener un plan</a>
            </td>
        </tr>
    </table>
    <!-- /button -->--}}

    <h3 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 18px; line-height: 1.2em; color: #111111; font-weight: 200; margin: 40px 0 10px; padding: 0;">&iquest;C&oacute;mo empezar?</h3>

    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Te recomendamos que leas detenidamente nuestra <a target="_blanck" href="https://sunapiperu.com/documentacion">documentaci&oacute;n</a>
        online y te mantengas al tanto de nuestras promociones y anuncios a trav&eacute;s de nuestros
        canales:</p>

    <p align="center">
        <a href="https://www.facebook.com/SunapiPeru" target="_blank"
           style="margin: 10px 10px 10px; color: #ff5959;"><img border="0" src="https://sunapiperu.com/img/facebook_icon.png"/></a>
        <a href="https://twitter.com/SunApiPeru" target="_blank"
           style="margin: 10px 10px 10px; color: #ff5959;"><img border="0" src="https://sunapiperu.com/img/twitter_icon.png"/></a>
        <a href="https://www.youtube.com/channel/UC0CygoFjKWXdIqbpu5AU1IA" target="_blank"
           style="margin: 10px 10px 10px; color: #ff5959;"><img border="0" src="https://sunapiperu.com/img/youtube_icon.png"/></a>
        <a href="https://github.com/SunApiPeru" target="_blank"
           style="margin: 10px 10px 10px; color: #ff5959;"><img border="0" src="https://sunapiperu.com/img/github_icon.png"/></a>
    </p>
@stop