@extends('emails.basewelcome')
@section('content')
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Hola {{$name}},</p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Bienvenido a SunApi Per&uacute;, la plataforma donde podr&aacute;s integrar tus aplicaciones con
        informaci&oacute;n financiera y de contribuyentes del Per&uacute;.</p>

    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        &iexcl;Felicidades! Has optenido un Plan B&aacute;sico de promoci&oacute;n por un mes, v&aacute;lido
        hasta {{$promodate}}.
        Puedes comenzar a utilizar nuestra API con tu
        Clave API: <span
                style="font-family: 'Consolas'; color: #d45500; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">{{$apikey}}</span>
    </p>

    <h3 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 18px; line-height: 1.2em; color: #111111; font-weight: 200; margin: 40px 0 10px; padding: 0;">
        &iquest;C&oacute;mo empezar?</h3>

    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Te recomendamos que leas detenidamente nuestra <a target="_blanck"
                                                          href="https://sunapiperu.com/documentacion">documentaci&oacute;n</a>
        online y te mantengas al tanto de nuestras promociones y anuncios a trav&eacute;s de nuestros
        canales:</p>

    <p align="center">
        <a href="https://www.facebook.com/SunapiPeru" target="_blank"
           style="margin: 10px 10px 10px; color: #ff5959;"><img border="0"
                                                                src="https://sunapiperu.com/img/facebook_icon.png"/></a>
        <a href="https://twitter.com/SunApiPeru" target="_blank"
           style="margin: 10px 10px 10px; color: #ff5959;"><img border="0"
                                                                src="https://sunapiperu.com/img/twitter_icon.png"/></a>
        <a href="https://www.youtube.com/channel/UC0CygoFjKWXdIqbpu5AU1IA" target="_blank"
           style="margin: 10px 10px 10px; color: #ff5959;"><img border="0"
                                                                src="https://sunapiperu.com/img/youtube_icon.png"/></a>
        <a href="https://github.com/SunApiPeru" target="_blank"
           style="margin: 10px 10px 10px; color: #ff5959;"><img border="0"
                                                                src="https://sunapiperu.com/img/github_icon.png"/></a>
    </p>
@stop