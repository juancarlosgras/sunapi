@extends('emails.base')
@section('content')
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Hola {{$name}},</p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Estamos verificando tu pago para que puedas adquirir el plan {{$plan}}.
        Una vez que hagamos la verificaci&oacute;n se activar&aacute; tu plan y te enviaremos una notificaci&oacute;n por este email.</p>

    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
        Mientras verificamos tu pago te recomendamos que leas nuestra
        <a target="_blanck" href="https://sunapiperu.com/documentacion">documentaci&oacute;n</a>
        online y te mantengas al tanto de nuestras promociones y anuncios a trav&eacute;s de nuestros
        canales.</p>
@stop